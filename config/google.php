<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Google Analytics
    |--------------------------------------------------------------------------
    |
    | Found in views/partials/ga.blade.php
    */
    
    'maps_key' => env('GOOGLE_MAPS_KEY', 'AIzaSyBDBYOlNSA5ofbvMRe6I-MHxdio2TgdXQU'),
    'analytics' => 'UA-XXXXX-X',
    'location' => [
      'lat' => -4.048629,
      'lon' => 39.704943,
    ],
];
