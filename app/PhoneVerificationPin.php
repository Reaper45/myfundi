<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User\User;

class PhoneVerificationPin extends Model
{
    // protected $fillable = ['phone_number', 'pin'];

    protected $primaryKey = 'phone_number';

    public $timestamps = false;

    public function user()
    {
      return $this->belongsTo(User::class);
    }
}
