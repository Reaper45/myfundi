<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User\User;
use App\Category;
use App\Comment;
use App\Models\Traits\Model as ModelTrait;

class Job extends Model
{
    use ModelTrait;

    /**
     *
     *
     * @var array
     */
    public $hidden = ['created_at', 'updated_at', 'deleted_at', 'cancelled_at', 'accepted_at', 'created_by', 'cancelled_by', 'completed', 'location_name'];

    /**
     * Make all attr mass assignnable
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Date Mutator
     *
     * @var array
     */
    public $dates = ['created_at', 'updated_at', 'deleted_at', 'cancelled_at', 'accepted_at'];

    /**
     * Get user relation
     *
     * @param null
     * @return User
     */
    public function user()
    {
      return $this->belongsTo(User::class);
    }

    /**
     * Get category relation
     *
     * @param null
     * @return Category
     */
    public function category()
    {
      return $this->belongsTo(Category::class);
    }

    /**
     * Define Job Comment relshp
     *
     * @param null
     * @return Comment
     */
    public function comment()
    {
        return $this->hasOne(Comment::class);
    }

    /**
     *
     * @param
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function service()
    {
        return $this->belongsTo(Service::class);
    }
}
