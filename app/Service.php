<?php

namespace App;

use App\Category;
use App\Job;
use App\Models\Auth\User\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Model as ModelTrait;

class Service extends Model
{
    use ModelTrait;
    
    protected $table = 'services';

    protected $fillable = ['name', 'slug', 'description'];

    public $hidden = [];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'technician_services', 'service_id', 'user_id');
    }

    /**
     * Jobs belonging to a Service
     *
     * @params
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobs()
    {
        return $this->hasMany(Job::class);
    }
}
