<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User\Traits\Ables\Rolable;
use App\Models\Auth\User\User;
use App\Models\Traits\Model as ModelTrait;

class Client extends Model
{
    use Rolable, ModelTrait;

    protected $table = 'users';

    public static function getAll()
    {
        $clients = User::all();

        $clients = $clients->reject(function($client, $key){
            if (!$client->hasRole('customer')) {
                return $client;
            }
        });
        return $clients;
    }
}
