<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User\User;

class Device extends Model
{
    protected $table = 'devices';

    public $timestamps = false;

    public $guarded = [];
    
    public function user()
    {
      return $this->belongsTo(User::class);
    }
}
