<?php

namespace App\Http\Controllers;

use App\Category;
use App\Job;
use App\Service;
use App\Technician;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Auth\User\User;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    /**
     * Reports page
     *
     * @params Request
     * @return view
     */
    public function index(Request $request)
    {
        $categories = Service::all('id', 'category_id', 'name', 'slug')->groupBy('category_id');

        $categories_group = array();
        foreach ($categories as $key => $group) {
            $name = Category::find($key)->name;
            $categories_group[$name] = $group;
        }

        $technicians = User::select('id','first_name','last_name')->where('user_type', '>', 0)->get();
//        dd($technicians);
        $data = [
            'categories'=>$categories_group,
            'technicians'=>$technicians
        ];
        return view('admin.reports', $data);
    }

    /**
     * All technicians under service
     */
    public function technicians(Request $request)
    {

        $service = Service::find($request->get('service_id'));

        $technicians = User::where('user_type', $service->id)->get();
        $data = [
            'technicians'=>$technicians
        ];

        return response($data, 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function printJobReports(Request $request)
    {
        $from = $request->get('from') ? explode('/', $request->get('from')): null;
        $to = $request->get('to') ? explode('/', $request->get('to')): null ;

        $from = $from ? $from[2]."-".$from[0]."-".$from[1]: null ;
        $to = $to ? $to[2]."-".$to[0]."-".$to[1]: null ;

        $service_id = $request->get('category_id');
        $status = $request->get('status');

        $query = DB::table('jobs');
        if ($status != 'all'){
            $query = $query->where('status', $status);
        }

        if ($service_id != 'all'){
            $query = $query->where('service_id', $service_id);
        }

        if($from != null && $to == null){
            $today = Carbon::today();
            $to = $today->toDateString();
        }

        if($from){
            $query = $query->whereBetween('created_at', [$from, $to]);
        }
        $jobs = $query->get();

        $payload = array();
        foreach ($jobs as $job){
            $user = User::find($job->user_id);
            $fullname = $user->first_name.' '.$user->last_name;

            $job->fullname = $fullname;
            array_push($payload, $job);
        }

        return view('admin.jobs.report-print',['jobs'=>$payload]);
    }

    public function printTechReports(Request $request)
    {
        $from = $request->get('from') ? explode('/', $request->get('from')): null;
        $to = $request->get('to') ? explode('/', $request->get('to')): null ;

        $from = $from ? $from[2]."-".$from[0]."-".$from[1]: null ;
        $to = $to ? $to[2]."-".$to[0]."-".$to[1]: null ;

        $category = $request->get('category');
        $technician = $request->get('technician_id');

        $query = User::with('technician')->where('user_type', '>', 0);
        if ($technician != 'all'){
            $query = $query->where('id', $technician);
        }

        if ($category != 'all'){
            $query = $query->where('user_type', $category);
        }

        if($from != null && $to == null){
            $today = Carbon::today();
            $to = $today->toDateString();
        }

        if($from){
            $query = $query->whereBetween('created_at', [$from, $to]);
        }
        $technicians = $query->get();

//        dd($technicians);
//        $payload = array();
//        foreach ($technicians as $job){
//            $user = User::find($job->user_id);
//            $fullname = $user->first_name.' '.$user->last_name;
//
//            $job->fullname = $fullname;
//            array_push($payload, $job);
//        }

        return view('admin.technician.report-print',['users'=>$technicians]);
    }
}
