<?php

namespace App\Http\Controllers;

use App\Models\Auth\Role\Role;
use App\Models\Auth\User\User;
use App\PhoneVerificationPin;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Traits\ControllerHelper;
use App\Rating;
use Carbon\Carbon;
use App\Jobs\SendSMS;
use App\Http\Controllers\Traits\CreatesClient;
use App\Http\Controllers\Traits\CreatesTechnician;

class ApiController extends Controller
{
    use SendsPasswordResetEmails, ControllerHelper, CreatesClient;

    // Register user
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return response($this->api_response(false, null, $validator->errors()), 200);
        }

        $data = $request->all();

        $user = array(
            'first_name'=>$data['first_name'],
            'last_name'=>$data['last_name'],
            'email'=>$data['email'],
            'phone_number'=>$data['phone_number'],
            'password'=> $data['password'],
            'confirmation_code' => Uuid::uuid4(),
            'confirmed' => false
        );

        if ($data['user_type'] == 0) {
            $users_exists = User::where([['email', '=', $data['email']],['user_type', '=', 0]])
                ->orWhere([['phone_number', '=', $data['phone_number']],['user_type', '=', 0]])
                ->first();

            if ($users_exists) {
                $error = 'Email/Phone Number already taken';

                return response($this->api_response(false, null, $error), 200);
            }
            $user['user_type'] = 0;

            $user = $this->createClient($user);

        }else{
            $user['user_type'] = $data['user_type'];

            $users_exists = User::where([['email', '=', $data['email']],['user_type', '>=', 1]])
                ->orWhere([['phone_number', '=', $data['phone_number']],['user_type', '>=', 1]])
                ->first();

            if ($users_exists) {
                $error = 'Email/Phone Number already taken';

                return response($this->api_response(false, null, $error), 200);
            }

            $technician = $this->makeTechnician($data);
            $user = $this->createTechnician($request, $user, $technician);
        }

        if ($user instanceof User){
            event(new Registered($user));

            $this->registered($request, $user);
            $user = User::find($user->id);
            // TODO: Create response here
            return response($this->api_response(true, $user, "Registration Successful!"), 200);
        }
        return $user;
    }

    // Log in
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'=>'required',
            'password'=>'required',
            'user_type'=>'required|numeric'
        ]);

        if ($validator->fails()) {
            return response($this->api_response(false, null, $validator->errors()), 200);
        }
        $password = $request->input('password');
        $user_type = $request->input('user_type');

        if(Auth::guard('web')->attempt(['email'=>$request->input('username'),'password'=>$password, 'user_type'=> $user_type])){
            $user = Auth::guard('web')->user();

            // $user = $this->reduce($user, ['created_at', 'deleted_at', 'updated_at']);

            return response($this->api_response(true, $user, null));
        }
        $error = ['These credentials do not match our records'];
        return response($this->api_response(false, null, $error), 200);
    }

    //Get all users
    public function getAllUsers()
    {
        $users = ['users' => User::all()];

        return response($this->api_response(true, $users, null), 200);
    }

    // Get user by Id
    public function getUser($id)
    {
        if ($id && is_numeric($id)) {
            if (User::find($id)) {
                $user = User::where(['id',$id])->first();
                return response($this->api_response(true, $user, null), 200);
            }
        }
        $error = ['Not Found!'];
        return response($this->api_response(false, null, $error), 200);
    }

    // Forgot password
    public function forgotPassword(Request $request)
    {
        $email = $request->input('email');
        $user_type = $request->input('user_type');

        $user = User::where([
            ['email', '=', $email],
            ['user_type', '=', $user_type]
        ])->first();

        if ($user) {
            /* To use the ResetsPasswords trait we must Insert email into Request
            -Object. Laravel pluck the email and send the link to it
            */
            $request['email'] = $user->email;
            $this->sendResetLinkEmail($request);

            return response($this->api_response(true, null, null), 200);
        }
        $error = 'Details do not match our records!';
        return response($this->api_response(false, null, $error), 200);
    }

    //Get Services
    public function getServices()
    {
        $services = Service::all('id', 'name');

        return response($this->api_response(true, $services, null), 200);
    }

    // Get Providers
    public function getProviders($service_id)
    {
        if (isset($service_id) && is_numeric($service_id)) {
            $service = Service::find($service_id);

            $providers = $service->users()->get();

            $to_remove  = [
                'description', 'email', 'confirmation_code', 'confirmed', 'pivot', 'active', 'created_at', 'deleted_at', 'updated_at'
            ];
            foreach ($providers as $provider) {
                $provider = $this->reduce($provider, $to_remove);
            }

            return response($this->api_response(true, $providers, null), 200);
        }
        $error = ['Not Found!'];
        return response($this->api_response(false, null, $error), 200);
    }

    // Get user details
    public function getUserDetails($user_id)
    {
        $hidden = [1];
        if (isset($user_id) && is_numeric($user_id) && !in_array($user_id, $hidden)) {
            $user = User::find($user_id);

            $user = $this->reduce($user, ['email']);

            if ($user) {
                return response($this->api_response(true, $user, null), 200);
            }
        }
        $error = 'Not Found!';
        return response($this->api_response(false, null, $error), 200);
    }

    // Check Phone Number
    public function checkPhoneNumber(Request $request)
    {
        $phone_number = $request->input('phone_number');
        $user_type = $request->input('user_type');

        $user_type = ($user_type > 0) ? 1: 0;

        $user = User::where([
            ['phone_number', '=', $phone_number],
            ['user_type', '=', $user_type]])->first();

        if (!$user) {
            $phone_pin = PhoneVerificationPin::where('phone_number', $phone_number)->first();

            $verification_code = rand(10000, 99999);

            if ($phone_pin) {
                if($phone_pin->timestamp - time() < 300){
                    $verification_code = $phone_pin->pin;
                }else{
                    $phone_pin->pin = $verification_code;
                }
                $this->send_pin($phone_number, $verification_code);

                $phone_pin->timestamp = time();
                $phone_pin->update();

                return response($this->api_response(false, null, null), 200);
            }

            $this->send_pin($phone_number, $verification_code);

            $phone_pin = new PhoneVerificationPin;

            $phone_pin->pin = $verification_code;
            $phone_pin->phone_number = $phone_number;
            // $phone_pin->user_type = $user_type;
            $phone_pin->timestamp = time();

            $phone_pin->save();

            return response($this->api_response(false, null, null), 200);

        }
        return response($this->api_response(true, null, null), 200);
    }

    // verify Phone Number wiht SMS
    public function verifyNumber(Request $request)
    {
        $phone_number = $request->input('phone_number');
        $verification_code = rand(10000, 99999);

        $sms = [
            'phone_number'=> $phone_number
        ];

        $phone_pin = PhoneVerificationPin::where('phone_number',$phone_number)->first();

        if ($phone_pin) {
            if($phone_pin->timestamp - time() < 300){
                $sms['message'] = 'patafundi: '.$phone_pin->pin;
                // $phone_pin->pin =>$verification_code;
            }else{
                $sms['message'] = 'patafundi: '.$verification_code;
                $phone_pin->pin = $verification_code;
            }
            dispatch(new SendSMS($sms));

            $phone_pin->timestamp = time();
            $phone_pin->update();

            return response($this->api_response(true, null, null), 200);
        }
        $phone_pin = new PhoneVerificationPin;

        $sms['message'] = 'patafundi: '.$verification_code;
        $phone_pin->pin = $verification_code;
        $phone_pin->phone_number = $phone_number;

        dispatch(new SendSMS($sms));

        $phone_pin->timestamp = time();
        $phone_pin->save();

        return response($this->api_response(true, null, null), 200);
    }

    // Verrify pin
    public function verifyPin(Request $request)
    {
        $pin = $request->input('pin');
        $phone_number = $request->input('phone_number');

        $failled_error = 'Oops! Verification failled';
        $phone_pin = PhoneVerificationPin::find($phone_number);
        if (!$phone_pin) {
            return response($this->api_response(false, null, $failled_error), 200);
        }

        if ($phone_pin->pin == $pin) {
            $phone_pin->delete();

            return response($this->api_response(true, null, null), 200);
        }
        return response($this->api_response(false, null, $failled_error), 200);
    }

    // Resend Verification SMS
    public function resendVerificationPin()
    {

    }

    // Update Location
    public function updateLocation(Request $request)
    {
        $user = Auth::user();

        $user->lat = $request>input('lat');
        $user->lon = $request>input('lon');

        if ($user->update()) {
            return response($this->api_response(true, null, null), 200);
        }
        $error = 'Error updating location';
        return response($this->api_response(false, null, $error), 200);
    }

    public function getRatings($user_id)
    {
        $user = User::find($user_id);
        if($user){
            return response($this->api_response(true, $user->stars(), null), 200);
        }
        $error = 'Not Found!';
        return response($this->api_response(false, null, $error), 200);
    }

    public function rateFundi(Request $request)
    {
        $rating = new Rating();

        $rating->stars = $request->input('star');
        $rating->rated_user = $request->input('rated_user');
        $user= User::find(2);

        if ($user) {
            $user->ratings()->save($rating);
            return response($this->api_response(true, null, null), 200);
        }

        return response($this->api_response(false, null, "Error Processing Request!"), 200);
    }

    private function createTechnician(Request $request, $user, $technician)
    {
        if(!$user){
            return response($this->api_response(true, null, "Unknow Internal Error Occured. Please try Again!"), 200);
        }

        DB::beginTransaction();
        try {
            $user_exists = User::where([['email', '=' ,$request->get('email')], ['user_type', '>', 0]])->first();

            if($user_exists){
                DB::rollback();

                return response($this->api_response(false, null, 'Email/Phone Already Taken!'), 200);
            }
            $user = User::create((array)$user);
            $technician = $user->technician()->save($technician);

            $service = Service::find($user->user_type);
            $user->service()->attach($service);

            $user->roles()->attach(Role::firstOrCreate(['name' => 'technician']));

            if (!$technician) {
                DB::rollback();
                throw new Exception("Error Processing Request", 1);

            }else {
                DB::commit();

                return $user;
            }

        } catch (Exception $e) {
            Log::error('Error: '.$e->getMessage());
            return response($this->api_response(false, null, "Error Processing Request"), 200);
        }
    }
}
