<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\UploadsFile;
use App\Technician;
use App\Service;
use App\Company;
use App\Job;
use App\Client;
use App\Models\Auth\User\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\CompanyCategory;

class DashboardController extends Controller
{
    use UploadsFile;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin')->except('charts');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.home');
    }

    /**
     * @get_percentage
     * @param int $dividend
     * @param int $divider
     * @return float|int
     */
    public function get_percentage(int $dividend, int $divider)
    {
        $percentage = ($divider == 0 ) ? 0  : $dividend/$divider *100 ;

        return $percentage;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        $technicians = Technician::count();
        $male_technicians = Technician::where('gender', 'male')->count();
        $female_technicians = Technician::where('gender', 'female')->count();
        $companies = Company::count();
        $clients = Client::getAll()->count();
        $jobs = Job::all()->count();
        $categories = Category::count();

        $today = Carbon::today();
        $last_week = $today->diffInDays($today->copy()->subWeek());
        $jobs_diff_count = Job::whereBetween('created_at', [$today, $last_week])->count();

        // Today Stats
        $yesterday = $today->subDay();

        $technicians_today = Technician::where('created_at', '>', $yesterday)->count();
        $clients_today = Client::getAll()->where('created_at', '>', $yesterday)->count();
        $jobs_today = Job::where('created_at', '>', $yesterday)->count();

        $technicians_increase = $this->get_percentage(0,0);
        $male_technician_increases =  $this->get_percentage(0,0);
        $female_technicians_increase =  $this->get_percentage(0,0);
        $companies_increase =  $this->get_percentage(0,0);
        $clients_increase =  $this->get_percentage(0,0);
        $jobs_increase =  $this->get_percentage($jobs ,$jobs_diff_count );

        $companies_per_category = CompanyCategory::all()->groupBy('category_id');

        $res = [
            'male_technicians'=>$male_technicians,
            'female_technicians'=>$female_technicians,
            'technicians'=>$technicians,
            'companies'=>$companies,
            'clients'=>$clients,
            'jobs'=>$jobs,
            'categories'=>$categories,
            'technicians_increase' => $technicians_increase,
            'male_technicians_increase' => $male_technician_increases,
            'female_technicians_increase' => $female_technicians_increase,
            'companies_increase' => $companies_increase,
            'clients_increase' => $clients_increase,
            'jobs_increase' => $jobs_increase,
            'companies_per_category' => $companies_per_category,
            'technicians_today' => $technicians_today,
            'clients_today' => $clients_today,
            'jobs_today' => $jobs_today,
        ];

        return view('admin.dashboard', $res);
    }

    /**
     * Charts data
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @internal param $
     */
    public function charts(Request $request)
    {

        $job_count = array();
        $categories_count = array();
        $jobs_per_cat = array();


        // Jobs Per status
        $jobs = Job::all()->groupBy('status');

        // Cartegories
        $categories = Category::all();

        // Jobs Per service
        $jobs_percat = Job::all()->groupBy('service_id');

        $services = Service::all();
        foreach ($services as $service_id => $service){
            foreach ($jobs_percat as $key => $group){
                if($key == $service_id){-
                $jobs_per_cat[$service->name] = count($group);
                }else{
                    $jobs_per_cat[$service->name] = 0;
                }
            }
        }

        // User growth
        $user_growth = DB::table('users')
            ->select(DB::raw('count(id) as count, MONTH(created_at) as month'))
            ->whereDate('created_at', '>=' ,Carbon::now()->subYear())
            ->groupBy('month')
            ->get();

        $sup_data = array();
        $months_bf = array();
        foreach ($user_growth as $user){
            array_push($months_bf, $user->month);
        }

        $mnths = range(1, 12);
        for ($i=1; $i<=count($mnths); $i++){

            if (!in_array($i, $months_bf)){
                $cur_month = array(
                    'count'=>0,
                    'month'=>$i
                );
                array_push($sup_data, $cur_month);
            }
        }

        $user_growth = $user_growth->toArray();
        foreach ($sup_data as $data){
            array_unshift($user_growth, $data);
        }

        foreach ($categories as $category){
            $categories_count[$category->name] = $category->services->count();
        }

        foreach ($jobs as $status => $job){
            $job_count[$status] = $job->count();
        }

        $jobs_per_cat = collect($jobs_per_cat)->sort()->toArray();
        $payload = array(
            'categories'=>$categories_count,
            'jobs'=>$job_count,
            'user_growth'=>$user_growth,
            'jobs_per_cat'=>$jobs_per_cat
        );

        return response($payload, 200);
    }
}
