<?php

namespace App\Http\Controllers;

use App\Category;
use App\Client;
use App\Company;
use App\Job;
use App\Service;
use App\Technician;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'about']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with('services')->select('id', 'name')->get();

        $category_jobs = [];
        foreach ($categories as $key => $category){
            $service_jobs = [];
            foreach ($category->services as $service){
                 array_push($service_jobs, $service->jobs->count());
            }
            $category_jobs[$key] = array(
                'name' => Category::find($category->id)->name,
                'jobs'=> array_sum($service_jobs)
            );
        }
        $category_jobs = array_random($category_jobs, 4);

        $total_jobs = Job::all()->count();
        $total_fundis = Technician::all()->count();
        $total_clients = Client::all()->count();
        $total_companies = Company::all()->count();

        $data = array(
            'category_jobs'=> $category_jobs,
            'total_fundis'=>$total_fundis,
            'total_clients'=>$total_clients,
            'total_companies'=>$total_companies,
            'total_jobs'=>$total_jobs
        );
        return view('welcome', $data);
    }

    public function about()
    {
      return view('about');
    }
}
