<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\UploadsFile;
use App\Job;
use App\Rating;
use Illuminate\Http\Request;
use App\Service;
use App\Models\Auth\User\User;
use App\Http\Controllers\Traits\CreatesTechnician;
use App\Models\Auth\Role\Role;
use App\Technician;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class TechnicianController extends UserController
{
    use CreatesTechnician, UploadsFile;


	public $model = Technician::class;

	/**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $user)
    {
        $this->middleware('admin');

        $accepted_jobs = Job::where('accepted_by', $user->id)->count();
        $done_jobs = Job::where([['accepted_by', '=', $user->id],['completed', '=', true]])->count();
        $user_clients = Job::where('accepted_by', $user->id)->groupBy('user_id')->count();
        $data = array(
            'user' => $user,
            'accepted_jobs'=>$accepted_jobs,
            'done_jobs'=>$done_jobs,
            'user_clients'=>$user_clients
        );
        return view('admin.technician.profile', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.technician.edit', ['user' => $user, 'roles' => Role::get()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     * @internal param User $user
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'location_name' => 'sometimes|required|max:255',
            'country' => 'sometimes|required|max:255',
            'id_card_no' => 'sometimes|required|numeric',
            'dob' => 'sometimes|required|date|max:255',
            'kra_pin_no' => 'sometimes|required|alpha_num|max:255',
        ]);

        if ($validator->fails()) return redirect()->back()->withErrors($validator->errors());

        $technician = auth()->user()->technician;

        $technician->location_name = $request->input('location_name');
        $technician->country = $request->input('country');
        $technician->id_card_no = $request->input('id_card_no');
        $technician->dob = $request->input('dob');
        $technician->kra_pin_no = $request->input('kra_pin_no');
        $technician->lat = $request->input('lat');
        $technician->lon = $request->input('lon');

        $technician->update();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        if($user->delete()){
            return response('User Account Deleted Successful',200);
        }
        return response('Error Processing request',500);

    }

    /**
     * @param $filename
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     */
	public function avatar($filename)
	{
		return response($this->getFile('profile', $filename), 200);
	}

	/**
	* @map
	* @return view
	*/
	public function map(User $user)
	{
		return view('admin.technician.map');
	}

    /**
     *
     * @param Request $request
     * @internal param $
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fundiDashboard(Request $request)
    {
        $feedback = Rating::all()
            ->where('rated_user', auth()->user()->id)
            ->sortBy('created_at')
            ->take(5);

        return view('fundi.dashboard', ['feedback'=>$feedback]);
    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param $
     */
    public function fundiDocuments(Request $request)
    {
        return view('fundi.documents');
    }

    /**
     *
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param $
     */
    public function signupSuccess(Request $request, $uuid)
    {
        return view('success')->with(['uuid'=>$uuid]);
    }

    /**
     * upload certificate
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    function uploadCertificate(Request $request)
    {
        $this->validateFile('certificate', $request)->validate();

        $technician = auth()->user()->technician;

        if ($request->hasFile('certificate')) {
            $inputFile = Input::file('certificate');

            if ($inputFile->isValid()) {
                $extention = $inputFile->extension();

                $technician->certificate = Uuid::uuid4().'.'.$extention;
                $technician->certificate_valid = false;

                $content = File::get($inputFile);

                if(!$this->upload($content, $technician->certificate, 'certificate')){

                    return redirect()->back()->withErrors(["message", "Failled to upload certificate"]);
                }

                $technician->update();
            }else{
                return redirect()->back()->withErrors(["message", "Failled to upload certificate"]);
            }
        }
        return redirect()->back();
    }

    /**
     * Upload certificate of good conduct
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function uploadCertificateOfGoodConduct(Request $request)
    {
        $this->validateFile('gc_file', $request)->validate();

        $technician = auth()->user()->technician;

        if ($request->hasFile('gc_file')) {
            $inputFile = Input::file('gc_file');

            if ($inputFile->isValid()) {
                $extention = $inputFile->extension();

                $technician->good_conduct = Uuid::uuid4().'.'.$extention;
                $technician->good_conduct_valid = false;

                $content = File::get($inputFile);

                if(!$this->upload($content, $technician->good_conduct, 'good_conduct')){

                    return redirect()->back()->withErrors(["message", "Failled to upload certificate"]);
                }

                $technician->update();
            }else{
                return redirect()->back()->withErrors(["message", "Failled to upload certificate"]);
            }
        }
        return redirect()->back();
    }

    /**
     *
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function uploadNationaIdCard(Request $request)
    {
        $this->validateFile('national_id', $request)->validate();

        $technician = auth()->user()->technician;

        if ($request->hasFile('national_id')) {
            $inputFile = Input::file('national_id');

            if ($inputFile->isValid()) {
                $extention = $inputFile->extension();

                $technician->id_card = Uuid::uuid4().'.'.$extention;
                $technician->id_card_valid = false;

                $content = File::get($inputFile);

                if(!$this->upload($content, $technician->id_card, 'national_ids')){

                    return redirect()->back()->withErrors(["message", "Failled to upload certificate"]);
                }

                $technician->update();
            }else{
                return redirect()->back()->withErrors(["message", "Failled to upload certificate"]);
            }
        }
        return redirect()->back();
    }

    /**
     * @param $name
     * @param $request
     * @return mixed
     */
    public function validateFile($name, $request)
    {
        $rules = [
            $name => 'bail|required|file|filled|mimes:pdf,jpg,png,jpeg,doc'
        ];

        return Validator::make($request->only($name), $rules);
    }


    /**
     * Upload account profile picture
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     *
     */
    public function uploadProfilePic(Request $request)
    {
        $this->validateFile('image', $request)->validate();

        $technician = auth()->user()->technician;

        if ($request->hasFile('image')) {
            $inputImage = Input::file('image');

            if ($inputImage->isValid()) {
                $extention = $inputImage->extension();
                $technician->avartar = Uuid::uuid4().'.'.$extention;

                $content = File::get($inputImage);
                $this->upload($content, $technician->avartar, 'profile');
            }else{
                Session::flash('errors', ['failled', 'Failled to upload Image']);
            }
        }
        $technician->update();

        return redirect()->back();
    }

    /**
     *
     */
    public function certificate(Request $request, $filename)
    {
        return response(Storage::get('certificates/'.$filename));
    }
}
