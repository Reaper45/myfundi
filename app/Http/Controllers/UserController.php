<?php

namespace App\Http\Controllers;

use App\Models\Auth\User\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Activate user account
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function activate(Request $request, $id)
    {
        $id = (int)$id;
        $user = User::find($id);

        if ($user->active){
            return redirect()->back()->withErrors('Error Processing Request');
        }
        $user->active = true;
        $user->update();

        return redirect()->back()->with(['message'=>'Account Activated Successfully']);
    }

    /**
     * Deactivate user account
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function deactivate(Request $request, $id)
    {
        $id = (int)$id;
        $user = User::find($id);

        if (!$user->active){
            return redirect()->back()->withErrors('Error Processing Request');
        }
        $user->active = false;
        $user->update();

        return redirect()->back()->with(['message'=>'Account Deactivated Successfully']);
    }
}
