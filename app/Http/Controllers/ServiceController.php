<?php

namespace App\Http\Controllers;

use App\Category;
use App\Service;
use App\Job;
use App\Models\Auth\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    /**
     * Create new service
     *
     *
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'service' => 'required',
        ]);

        if ($validate->fails()) {

            return response(['error'=>'Error Processing request. Try Again!'], 500);
        }

        $cat_id = $request->input('category_id');
        $name = $request->input('service');
        $desc = $request->input('desc');

        $service = new Service;
        $service->name = $name;
        $service->slug = str_slug($name);
        $service->description = $desc;

        $categoty = Category::find($cat_id);
        if (!$categoty) {
            return response(['success'=>'Service created successfully!'], 200);
        }

        if ($categoty->services()->save($service)) {
            return response(['success'=>'Service created successfully!'], 200);
        }
        return response(['error'=>'Error Processing request. Try Again!'], 500);
    }

    /**
     * View particular service and fundis unser it
     *
     * @params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewService()
    {
        return view('admin.categories.service');
    }

    /**
     * Get service per category
     */
    public function fetchServices($slug)
    {
        $category = Category::where('slug', $slug)->first();

        if (!$category){
            return response(['message'=>'Error Processing request!'], 500);
        }
        $category->services = $category->services()->get();
        $services = $category->services;

        $data = array();

        foreach($services as $service){
            $id = $service->id;

            $service->technicians = User::where('user_type', $id)->count();
            $service->jobs = Job::where('service_id', $id)->count();
            $service->action = "<button class='btn btn-sm btn-default edit-service'><i class='fa fa-edit'></i></button>
                                <input type='hidden' value=$id >
                                <button class='btn btn-sm btn-default delete-service'><i class='fa fa-trash'></i></button>";

            array_push($data, $service);
        }
        return response(['data'=>$data]);
    }

    /**
     * Delete service
     *
     */
    public function delete(Request $request)
    {
        $id = $request->get('id');
        $service = Service::find($id);

        if($service){
            $service->delete();
            return response('', 200);
        }
        return response('', 500);
    }

    /**
     * Get service
     */
    public function get(Request $request)
    {
        $id = $request->get('id');
        $service = Service::find($id);

        return response(['service'=>$service], 200);
    }

    /**
     * Edit service
     */
    public function edit(Request $request)
    {
        $service_id = $request->input('service_id');
        $service_desc = $request->input('service_desc');

        $service = Service::find($service_id);
        if ($service){
            $service->description = $service_desc;
            $service->update();

            return response('', 200);
        }
        return response('', 500);
    }
}