<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Auth\User\User;

class ClientController extends UserController
{
    function __construct()
    {
      $this->middleware('auth');
    }

    public function clients(Request $request)
    {
      $users = User::with('roles')->sortable(['email' => 'asc'])->paginate();

      return view('admin.client.all', ['users' => $users]);
    }
}
