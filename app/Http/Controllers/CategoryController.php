<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    function __construct()
    {
    	$this->middleware('admin')->except('categoryServices');
    }

    /**
    * @getAll
    * @return view()
    */
    public function index(Request $request)
    {
        $categories = Category::all();

        foreach ($categories as $category){
            $services = $category->services->count();
            $category->services = $services;
        }

    	return view('admin.categories.all', ['categories'=>$categories]);
    }

    /**
    * @create
    * @return view()
    */
    public function create(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'title'=>'required',
        ]);

        if($validate->fails()){
            return response([$validate->errors()], 500);
//            return redirect()->back()->withErrors(['errors'=>$validate->errors()]);
        }
        $title = $request->input('title');
        $desc = $request->input('desc');

        $category = new Category;
        $category->name = $title;
        $category->slug = str_slug($title);
        $category->description = $desc;

        if ($category->save()){
            return response(['success'=>'Category created successfully!'], 200);
        }

    	return response(['errors'=>'Error Processing request. Try Again!'], 500);
    }

    /**
     * Delete catetgory
     *
     * @param Request $request
     * @internal param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function delete(Request $request)
    {
        if ($request->input('id')){
            $id = $request->input('id');

            $category = Category::find($id);
            if ($category){
                $category->delete();
                return response(['message'=>'Category deleted successfully!'], 200);
            }
        }
        return response(['message'=>'Error Processing request!'], 500);
    }

    /**
     * View Category Deteils
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @internal param $
     */
    public function services(Request $request, $slug)
    {
        $category = Category::where('slug', $slug)->first();

        return view('admin.categories.view', ['category'=>$category]);
    }
}
