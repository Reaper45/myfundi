<?php
namespace App\Http\Controllers\Traits;

use App\Models\Auth\Role\Role;
use App\Models\Auth\User\User;
use Illuminate\Support\Facades\Validator;

trait CreatesClient{

	/**
	 * @createClient
	 *
	 *
	 * @param $user
	 * @return $client
	 */
	public function createClient($user)
	{
		$user = User::create($user);
        $user->roles()->attach(Role::firstOrCreate(['name' => 'client']));

        return $user;
	}


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'first_name'=>'required|max:255',
            'last_name'=>'required|max:255',
            'email'=>'bail|required|email|max:255',
            'phone_number'=>'bail|required|max:255',
            'password' => 'required|min:6',
        ];

        return Validator::make($data, $rules);
    }

}