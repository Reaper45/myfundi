<?php

namespace App\Http\Controllers\Traits;

use App\Jobs\SendSMS;
use App\Models\Auth\User\User;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use App\Models\Auth\Role\Role;
use Illuminate\Auth\Events\Registered;
use App\Technician;
use Exception;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

trait CreatesTechnician
{

    /**
     * @var string
     */
    protected $app_link = 'https://drive.google.com/open?id=0ByZ2e3LqYDalZzlod3hCUG1RVlE';

    /**
     * Registration technician
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register(Request $request)
    {
        if ($request->isMethod('get')){
            return view('become-fundi');
        }

        $this->validator($request->all())->validate();

        $user = User::where([['email', '=' ,$request->input('email')], ['user_type', '>', 0]])->first();
        if($user){
            Session::flash('_old_input', $request->all());

            return redirect()->back()->withErrors(['email'=>'Email Already Taken!']);
        }
        $uid = Uuid::uuid4();

        $user = $this->makeFundiObj($request);

        $hidden = array_diff($user->hidden, ['password']);
        $user->hidden = $hidden;

        Redis::set($uid, $user);
        Redis::expire($uid, 3600);

        return redirect()->route('fundi.state', ['uid'=>$uid]);
    }


    /**
     * Registering as Company or Individual
     *
     * @param Request $request
     * @return string
     */
    public function selectState(Request $request, $uid)
    {
        $account_type = $request->get('state');

        if ($request->isMethod('get') && !$account_type){
            return view('state')->with(['uid'=>$uid]);
        }

        $user = Redis::get($uid);

        $user = json_decode($user);

        $user = (object)$user;
        $user->user_type = $account_type;

        Redis::set($uid, json_encode($user));
        Redis::expire($uid, 3600);

        if ($user->user_type == 'fundi'){

            return $this->selectService($uid);

        }elseif ($user->user_type == 'company'){

            return redirect()->action('CompanyController@register', ['uid', $uid]);
        }
    }

    /**
     * Select service individual fundi registering for
     *
     */
    public function  selectService($uid)
    {
        $services = Service::all('id','category_id', 'name')->groupBy('category_id');

        return view('select-service', ['services'=>$services, 'uid'=>$uid]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param Request $request
     * @return User|\Illuminate\Database\Eloquent\Model
     * @internal param array $data
     */
    protected function makeFundiObj(Request $request)
    {
        $user = new User;

        $user->email = $request->input('email');
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->phone_number = $request->input('phone_number');
        $user->plain_pass = $request->input('password');
        $user->gender = $request->input('gender');
        $user->location_name = $request->input('location_name');
        $user->confirmation_code = Uuid::uuid4();
        $user->confirmed = false;

        return $user;
    }


    /**
     * Commit technician to DB
     *
     * @param Request $request
     * @param $uid
     * @return $this|\Illuminate\Database\Eloquent\Model
     * @internal param $user
     * @internal param $technician
     */
    public function create(Request $request, $uid)
    {
        $service_id = $request->get('service_id');
        $user = Redis::get($uid);

        if(!$user){
            return redirect()
                ->back()
                ->withErrors(['message'=>'Unknow Internal Error Occured. Please try Again!']);
        }

        $user = json_decode($user);

        $user->user_type = (int)$service_id;
        $location_name = $user->location_name;

        DB::beginTransaction();
        try {
            $user_exists = User::where([['email', '=' ,$user->email], ['user_type', '>', 0]])->first();
            if($user_exists){
                DB::rollback();
                Session::flash('_old_input', $request->all());

                return redirect()->route('become.fundi')->withErrors(['email'=>'Email Already Taken!']);
            }
            $user->password = $user->plain_pass;
            $user = User::create((array)$user);

            $technician = new Technician;

            $technician->gender = $user->gender;
            $technician->location_name = $location_name;

            $technician = $user->technician()->save($technician);

            $service = Service::find($service_id);
            $user->service()->attach($service);

            $user->roles()->attach(Role::firstOrCreate(['name' => 'technician']));

            if (!$technician) {
                DB::rollback();
                throw new Exception("Error Processing Request", 1);

            }else {
                DB::commit();

//                Auth::login($user);
                Redis::del($uid);

                $this->registered($request, $user);

                return redirect()->route('fundi.success', ['uuid'=>$user->confirmation_code]);
            }

        } catch (Exception $e) {
            Log::error('Error: '.$e->getMessage());
            return redirect()->back()->withErrors(['message'=>$e->getMessage()]);
        }
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'first_name'=>'required|max:255',
            'last_name'=>'required|max:255',
            'email'=>'bail|required|email|max:255',
            'phone_number'=>'bail|required|max:255',
            'password' => 'required|confirmed|min:6',
        ];

        // use captcha
        if (config('auth.captcha.registration')) {
            $rules['g-recaptcha-response'] = 'required|captcha';
        }

        return Validator::make($data, $rules);
    }

    /**
     * Send fundi app link
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     * @internal param Request $request
     */
    public function sendLink($uuid)
    {
        $user = User::where('confirmation_code', $uuid)->get()->first();
        if ($user){
            $sms = [
                'phone_number' => $user->phone_number,
                'message' => $this->app_link
            ];
            dispatch(new SendSMS($sms));
        }

        return redirect()->intended('/');
    }
}
