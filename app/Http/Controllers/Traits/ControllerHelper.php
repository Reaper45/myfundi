<?php

namespace App\Http\Controllers\Traits;

use App\Device;
use App\Geolocation;
use App\Job;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Notifications\Auth\ConfirmEmail;
use App\Jobs\SendSMS;
use App\Technician;
use App\Models\Auth\User\User;
use App\Jobs\SendPushNotification;
use Illuminate\Support\Facades\DB;

trait ControllerHelper
{
    /**
     * Create Technicain details for user_type 1
     *
     * @param array $data
     * @return Technician
     */

    public function makeTechnician(array $data)
    {
      // $data = $request->all();
      $technician = new Technician;

      $technician->gender        = isset($data['gender']) ?$data['gender']: null;
      $technician->dob           = isset($data['dob']) ?$data['dob']: null;
      $technician->lat           = isset($data['lat']) ?$data['lat']: config('google.location.lat');
      $technician->lon           = isset($data['lon']) ?$data['lon']: config('google.location.lon');
      $technician->id_card_no    = isset($data['id_card_no']) ?$data['id_card_no']: null;
      $technician->location_name = isset($data['location_name']) ?$data['location_name']: null;

      return $technician;
    }

    /**
     * Formart the api responce
     *
     *
     * @param bool $success, $data, $message
     * @return array
     */
    protected function api_response(bool $success, $data, $message)
    {
        return [
            'success' => $success,
            'data'    => $data,
            'message' => $message
        ];
    }

    /** 
     * Dynamically set $hidden models attr
     *
     *
     * @param $model, array $to_remove
     * @return stdClass
     */
    public function reduce($model, array $to_remove)
    {
        // $data = [$model->hidden, $to_remove];
        foreach ($to_remove as $value) {
            array_push($model->hidden, $value);
        }

        return $model;
    }

    /**
     * Add new attr to model
     *
     *
     * @param $model, array $attributes
     * @return stdClass
     */
    protected function grow($model, array $attributes)
    {
        foreach ($attributes as $key => $value) {
            $model->$key = $value;
        }
        return $model;
    }

    /**
     * User registered notifier
     *
     *
     * @param Request $request, $user
     * @return bool
     */
    protected function registered(Request $request, $user)
    {
        if (config('auth.users.confirm_email') && !$user->confirmed) {

            $user->notify(new ConfirmEmail());

            return true;
        }
    }


    // Rename Attr in a Models
    protected function rename_attr($model, array $names)
    {
        return $model;
    }

    /**
     * @send_pin
     *
     * @param $phone_number, $pin
     * @return $pin
     */
    public function send_pin($phone_number, $pin)
    {
        $sms = [
            'phone_number'=> $phone_number,
            'message' => 'patafundi: '.$pin,
        ];

        dispatch(new SendSMS($sms));
    }

    /**
     * @format_phone
     *
     * @param string($phone)
     * @return void
     */
    public function format_phone($phone) 
    {

        if(!isset($phone{3})) return '';

        $phone = preg_replace("/[^0-9]/", "", $phone);

        $length = strlen($phone);

        if ($length == 10 && preg_match("/([0]){1}/", $phone)) {
            return preg_replace("/([0-9]{1})([0-9]{9})/", "+254$2", $phone);
        }
        return $phone;
    }

    /**
     * Get closest technicians
     * @return User $users
     * @internal param array $starting_point
     */
    public function nearTechnicians(Request $request)
    {
        $coords = DB::table('technicians')->select(['user_id', 'lat', 'lon'])->get();

        $origin = array(
            'lat'=> $request->get('lat'),
            'lon'=> $request->get('lon')
            );

        $user_distance = array();
        foreach ($coords as $value) {
            if($value->lat != null || $value->lon != null){
                $destination = array(
                    'lat'=> $value->lat,
                    'lon'=> $value->lon
                    );
                // Set locations
                $origin_geo = GeoLocation::fromDegrees($origin['lat'], $origin['lon']);
                $destination_geo = GeoLocation::fromDegrees($destination['lat'], $destination['lon']);

                $dist_in_km = $origin_geo->distanceTo($destination_geo, 'kilometers');

                $user_distance[$value->user_id] = $dist_in_km;
            }
        }

        $max_radius = 10;
        for ($radius = 1; $radius < $max_radius; $radius++){
            $users = $this->filter_distance($user_distance, $radius);
            if (!empty($users)){
                break;
            }
        }

        $users = User::find($users);

        $data = [];
        foreach ($users as $key => $user){
            $user = array(
                'first_name'=>$user->first_name,
                'last_name'=>$user->last_name,
                'service'=>$user->service->first()?$user->service->first()->name:null,
                'lat'=>$user->technician->lat,
                'lon'=>$user->technician->lon
            );
            array_push($data, $user);
        }
        $payload = array('users'=>$data, 'radius'=>$radius);

        return response($this->api_response(true, $payload, null), 200);
    }

    /**
     * @param array $users
     * @param float $radius
     * @return array
     */
    public function  filter_distance(array $users, float $radius)
    {
        $user_ids = [];
        foreach ($users as $id => $distance){
            if ($distance < $radius){
                array_push($user_ids, $id);
            }
        }
        return $user_ids;
    }

    /**
     * Create data payload to push
     *
     *
     * @param string $action, array $data
     * @return mixed
     */
    public function createPayload(string $action, array $data)
    {
        switch ($action) {
            case "available_job":
               return array("action"=>"available_job", "data" => $data);

            case "accepted_job":
                return array("action"=>"accepted_job", "data" => $data);

            case "cancelled_job":
                return  array("action"=>"cancelled_job", "data" => $data);

            case "no_tech":
                return array("action"=>"no_tech", "data" => $data);
        }
    }

    /**
     * @return array
     */
    public function technicianDevices(): array
    {
        $devices = Device::all(); // Get all devices

        // Filter out devices 'untechnician'
        $devices = $devices->reject(function ($value, $key) {
            $user = $value->user()->first();
            if ($user != null && !$user->hasRoles('technician')) {
                return $value;
            }
        });

        // Only get tokens
        return $devices->pluck('token')->toArray();
    }


    public function prepareNotificationPayload(Job $job): array
    {
        return array(
            'job_id' => $job->id,
            'description' => $job->description,
            'user' => $job->user->first_name . " " . $job->user->last_name,
            'lat' => $job->lat,
            'lon' => $job->lon
        );
    }

    /**
     * Send a Push Notification
     *
     *
     * @param  array $devices , array $data_payload
     * @param array $data_payload
     * @return void
     */
    private function sendNotification(array $devices, array $data_payload)
    {
        $notification = array(
            'to'=> $devices,
            'data'=>$data_payload,
        );

        // Send notification to client
        $job_notification = (new SendPushNotification($notification))->onConnection("database");
        dispatch($job_notification);

        return;
    }


}
