<?php

namespace App\Http\Controllers\Traits;

use App\AfricasTalkingGateway;
use App\Jobs\SendSMS;

trait SendsSMS
{

  public function send(array $sms)
  {
    $qued = (new SendSMS($sms))->onConnection('database');

    dispatch($qued);
  }
}
