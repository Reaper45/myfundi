<?php

namespace App\Http\Controllers\Auth;

use App\Models\Auth\Role\Role;
use App\Notifications\Auth\ConfirmEmail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Models\Auth\User\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\Traits\UploadsFile;
use App\Http\Controllers\Traits\CreatesTechnician;
use App\Http\Controllers\Traits\CreatesClient;
use Ramsey\Uuid\Uuid;
use App\Technician;
use App\Http\Controllers\Traits\ControllerHelper;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, CreatesTechnician, CreatesClient, UploadsFile, ControllerHelper;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param Request $request
     * @return User|\Illuminate\Database\Eloquent\Model
     * @internal param array $data
     */
    protected function create(Request $request)
    {
        $data = $request->all();

        $user = new User;

        $user->email = $data['email'];
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->phone_number = $data['phone_number'];
        $user->password = $data['password'];
        $user->confirmation_code = Uuid::uuid4();
        $user->confirmed = false;

        if ($data['service_id']) {
            $user->user_type = 1;
            $technician = $this->makeTechnician($data);

            return $this->createTechnician($request, $user, $technician);
        }else{
            $user->user_type = 0;

            $user = $this->createClient($user);
        }
        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        // dd($request->all());
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request)));
        
        $this->registered($request, $user);

        return redirect()->back();
    }

    /**
     *
     * @param Request $request
     * @internal param $
     */
     public function signUpFundi(Request $request)
     {

     }
}
