<?php

namespace App\Http\Controllers;

use App\Jobs\CheckJobState;
use App\Technician;
use Illuminate\Http\Request;
use App\Job;
use App\Http\Controllers\Traits\ControllerHelper;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Auth\User\User;
use Exception;
use App\Device;
use App\Service;
use Carbon\Carbon;
use App\Comment;

class JobController extends Controller
{
    use ControllerHelper;

    function __construct()
    {
        // $this->middleware('admin');
    }

    /**
     * @create
     *
     * @param $request
     * @return response;
     */
    public function create(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'user_id' => 'required|numeric|max:100',
            'service_id' => 'required|numeric|max:100',
            'description' => 'required|max:255',
            'lat' => 'required',
            'lon' => 'required',
            'location_name' => 'sometimes|required'
        ]);

        if ($validate->fails()) {
            return response($this->api_response(false, null, $validate->errors()));
        }

        $job = new Job();
        $job->lat = $request->input('lat');
        $job->lon = $request->input('lon');
        $job->location_name = $request->input('location_name');
        $job->description = $request->input('description');
        $job->service_id = $request->input('service_id');
        $job->created_by = $request->input('user_id');

        $user = User::find($request->input('user_id'));
        if ($user) {
            try {
                $job = $user->jobs()->save($job);
                if ($job) {
                    $job = $this->reduce($job, ['service_id', 'user_id']);

                    // Only get device tokens
                    $devices = $this->technicianDevices();
                    $data_payload = $this->createPayload('available_job', $this->prepareNotificationPayload($job));

                    $this->sendNotification($devices, $data_payload);
                    $this->trackJobStatus($job->id);

                    return response($this->api_response(true, $job, null));
                }
                throw new Exception("Error Processing Request", 1);
            } catch (Exception $e) {
                Log::error('Error :' . $e->getMessage());

                return response($this->api_response(false, null, "Error Processing Request. Try Again!"));
            }
        }
        return response($this->api_response(false, null, "Error Processing Request. Try Again!"));
    }

    /**
     * @getAll
     * @return view
     * @internal param $request
     */
    public function getAll()
    {
        $this->middleware('admin');

        $jobs = Job::all();

        foreach ($jobs as $job) {
            $client = $job->user;
            $job->client = $client->first_name . " " . $client->last_name;

            $job->service = Service::find($job->service_id)->name;
            $assigned_to = User::find($job->assigned_to);
            if ($assigned_to) {
                $job->assigned_to = $assigned_to->first_name . " " . $assigned_to->last_name;
            }
        }
        return view('admin.jobs.all', ['jobs' => $jobs]);
    }

    /**
     * @jobDetails
     *
     * @param \Illuminate\Http\Request $request , $job_id
     * @return Response
     */
    public function jobDetails(Request $request, $job_id)
    {
        $jb = Job::find($job_id);
        if ($jb == null) {
            return response($this->api_response(false, null, "Job does not exist"));
        }
        $comment = ($jb->comment) ? $jb->comment->content : '';

        $jb = collect($jb);
        $jb = $jb->reject(function ($value, $key) {
            if ($key == "comment") {
                return $value;
            }
        });

        $job = new Job;
        foreach ($jb as $key => $value) {
            $job->$key = $value;
        }

        $job->comments = $comment;
        $user_phone = $this->format_phone(User::find($job->user_id)->phone_number);

        if ($job->accepted_by) {
            $tech_phone = $this->format_phone(User::find($job->accepted_by)->phone_number);
        } else {
            $tech_phone = null;
        }

        $job = $job->addAttr(array('tech_phone' => $tech_phone, 'user_phone' => $user_phone));
        $job = $job->setHidden(array('id', 'status', 'accepted_by', 'deleted_at', 'created_at', 'completed', 'updated_at', 'assigned_to', 'start_date', 'service_id', 'location_name', 'user_id'));

        return response($this->api_response(true, $job, null));
    }

    /**
     * @recentJobs
     *
     * @param \Illuminate\Http\Request $request , $user_id
     * @return Response
     */
    public function recentJobs(Request $request, $user_id)
    {
        $jobs = DB::table('jobs')
            ->where('created_by', '=', $user_id)
            ->orWhere('cancelled_by', '=', $user_id)
            ->orWhere('accepted_by', '=', $user_id)
            ->orderBy('created_at', 'desc')
            ->get();

        $result = array();
        if ($jobs) {
            foreach ($jobs as $job) {
                array_push($result, $this->createJobObj($job));
            }
        }
        return response($this->api_response(true, $result, null));
    }

    /**
     * @cancelJob
     *
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function cancelJob(Request $request)
    {
        $job_id = $request->input('job_id');
        $user_id = $request->input('user_id');
        $user_type = $request->input('user_type');
        $comment_val = $request->input('comment');

        $job = Job::find($job_id);
        $user = User::find($user_id);
        $comment = new Comment;

        if ($job == null) {
            return response($this->api_response(false, null, "Job does not exist"));
        }

        if ($job->status == 'cancelled') {
            return response($this->api_response(false, null, "Job has already beed cancelled"));
        }

        if ($user_type > 0 && $job->status == 'pending') {
            return response($this->api_response(false, null, null));
        }

        if ($job->status == "pending" ) {
            $job->cancelled_by = (int)$user_id;
            $job->cancelled_at = Carbon::now();
            $job->status = 'cancelled';

            $comment->content = $comment_val;
            $comment->user_id = $user_id;

            DB::beginTransaction();
            try {
                if (!$job->update() || !$job->comment()->save($comment)) {
                    DB::rollback();
                    throw new Exception("Error Processing Request", 1);
                } else {
                    DB::commit();
                }
            } catch (Exception $e) {
                Log::error('Cancel Job Error: ' . $e->getMessage()); // Log Error

                return response($this->api_response(false, null, "Couldn't cancel job. Try again!"));
            }
        }

        if ($job->status == 'accepted') {
            $accepted_by = $job->accepted_by;

            $job->cancelled_by = (int)$user_id;
            $job->cancelled_at = Carbon::now();
            $job->accepted_by = null;
            $job->accepted_at = null;

            $job->status = 'cancelled';

            $comment->content = $comment_val;
            $comment->user_id = $user_id;

            DB::beginTransaction();
            try {
                if (!$job->update() || !$job->comment()->save($comment)) {
                    DB::rollback();
                    throw new Exception("Error Processing Request", 1);
                } else {
                    DB::commit();
                }
            } catch (Exception $e) {
                Log::error('Cancel Job Error: ' . $e->getMessage()); // Log Error

                return response($this->api_response(false, null, "Couldn't cancel job. Try again!"));
            }

            if ($user_id == $accepted_by) {
                // Send notification to technician
                $user_to_notify = User::find($job->created_by);
            }else {
                // Send notification to user
                $user_to_notify = User::find($accepted_by);
            }

            if ($user_to_notify) {
                $devices = $user_to_notify->devices->pluck('token')->toArray();
            }

            $data_payload = array(
                'job_id' => $job_id
            );

            $data_payload = $this->createPayload('cancelled_job', $data_payload);
            $this->sendNotification($devices, $data_payload);
        }

        return response($this->api_response(true, null, null));
    }

    /**
     * @jobDetails
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function acceptJob(Request $request)
    {
        $job_id = $request->input('job_id');
        $user_id = $request->input('user_id');

        // Technicians location
        $lat = $request->input('lat');
        $lon = $request->input('lon');

        $job = Job::find($job_id);

        if ($job == null) {
            return response($this->api_response(false, null, "Job does not exist"));
        }

        if ($job->status == 'cancelled') {
            return response($this->api_response(false, null, "Job has been cancelled"));
        }

        if ($job->status == 'accepted') {
            return response($this->api_response(false, null, null));
        }

        if ($job && ($job->status == 'pending')) {

            $job->accepted_by = $user_id;
            $job->accepted_at = Carbon::now();
            $job->status = 'accepted';

            $job->update();

            // Update Technicians Location
            $technician = Technician::where('user_id', $job->accepted_by)->first();
            $technician->lat = $lat;
            $technician->lon = $lon;
            $technician->update();

            $data_payload = array(
                'job_id' => $job_id
            );

            $devices = $job->user->devices->pluck('token');
            $data_payload = $this->createPayload('accepted_job', $data_payload);

//            info($devices->toArray());
            $this->sendNotification($devices->toArray(), $data_payload);

            return response($this->api_response(true, null, null));
        }

        return response($this->api_response(false, null, null));
    }

    /**
     * Create a job Object
     *
     *
     * @param $job
     * @return  array
     */
    private function createJobObj($job)
    {
        $service = Service::find($job->service_id);

        $tech_name = '';
        $user_name = '';

        if ($job->accepted_by != null) {
            $user = User::find($job->accepted_by);

            $tech_name = $user->first_name . " " . $user->last_name;
        }

        if ($job->created_by != null) {
            $user = User::find($job->created_by);

            $user_name = $user->first_name . " " . $user->last_name;
        }

        return array(
            "job_id" => $job->id,
            "job_name" => $service->name,
            "job_status" => $job->status,
            "tech_name" => $tech_name,
            "user_name" => $user_name,
            "timestamp" => $job->created_at,
            "user_id"  => $job->created_by,
            "tech_id" => $job->accepted_by
        );   

    }

    /**
     * All jobs that are pending for that particular type for now
     *
     *
     * @param \Illuminate\Http\Request $request , $user_id
     * @return mixed
     */
    public function availableJobs(Request $request, $user_id)
    {
        $user = User::find($user_id);
        if (!$user) {
            return response($this->api_response(false, null, 'User does not exist!'));
        }
        if (!$user->hasRole('technician')) {
            return response($this->api_response(false, null, 'Error Processing Request. Try Again!'));
        }
        // dd($user->service->count() );
        if ($user->service->count() == 0) {
            Log::error('Error: Unknown Error: ' . __class__ . '@' . __function__ . '(), line:  ' . __line__);

            return response($this->api_response(false, null, 'Error Processing Request. Try Again!'));
        }

        $service_id = $user->service->first()->id;
        $user_name = $user->first_name . ' ' . $user->last_name;

        $jobs = Job::where([['status', '=', 'pending'], ['service_id', '=', $service_id]])->get();

        $result = array();
        foreach ($jobs as $job) {
            $data = array(
                'job_id' => $job->id,
                'user_name' => $user_name,
                'timestamp' => $job->created_at,
                'lat' => $job->lat,
                'lon' => $job->lon
            );
            array_push($result, $data);
        }

        return response($this->api_response(true, $result, null));
    }

    /**
     * @param $job_id
     */
    private function trackJobStatus($job_id): void
    {
        $notification = (new CheckJobState($job_id))
            ->onConnection("database")
            ->delay(Carbon::now()->addSeconds(40));

        dispatch($notification);
    }
}
