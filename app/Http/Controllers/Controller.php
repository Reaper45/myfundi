<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\Breadcrumbs;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Notifications\Auth\ConfirmEmail;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * User registered notifier
     *
     *
     * @param Request $request, $user
     * @return bool
     */
    protected function registered(Request $request, $user)
    {
        if (config('auth.users.confirm_email') && !$user->confirmed) {

            $user->notify(new ConfirmEmail());

            return true;
        }
    }

    public function getFile($disk, $filename)
    {
        return Storage::disk($disk)->get($filename);
    }

    /**
	* @getCoordinates
	* @return response
	*/
    public function getCoordinates()
	{
		$coordinates = $this->model::all(['lat', 'lon']);
		return response()->json($coordinates);
	}
}
