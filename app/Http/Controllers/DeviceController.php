<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\Http\Controllers\Traits\ControllerHelper;

class DeviceController extends Controller
{
    use ControllerHelper;

    public function addDevice(Request $request){
        $device = Device::updateOrCreate(
          ['user_id' => $request->input('user_id')],
          ['token' => $request->input('token')]
        );

        if ($device) {
           return response($this->api_response(true, null, null));
        }
        return response(false, null, null);
    }
}
