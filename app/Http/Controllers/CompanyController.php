<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;

class CompanyController extends Controller
{
	//model to fetch
	public $model = Company::class;

    function __construct()
    {
    	$this->middleware('admin');
    }

    function getAll(Request $request)
    {
    	$companies = Company::all();
    	// dd($companies);
    	return view('admin.company.all', ['companies' =>$companies] );
    }

    // Mapping companies
    function map(Request $request)
    {
    	return view('admin.company.map');
    }

    /**
     * Create a company user type
     *
     */
    function register($uid)
    {

    }
}
