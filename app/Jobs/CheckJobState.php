<?php

namespace App\Jobs;

use App\Device;
use App\Job;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Controllers\Traits\ControllerHelper;

class CheckJobState implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ControllerHelper;

    /**
     * @var int $job
     */
    private $job_id;

    /**
     * @var bool $checked
     */
    private $checked = false;
    /**
     * Create a new job instance.
     * @param int $job_id
     */
    public function __construct(int $job_id)
    {
        $this->job_id = (int)$job_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $job = Job::find($this->job_id);

        while(!$this->checked){
            if ($job->status == "pending"){
                // Resend notifications to nearest technicians
                $data_payload = $this->createPayload('available_job', $this->prepareNotificationPayload($job));
                $devices = $this->technicianDevices();

                $this->sendNotification($devices, $data_payload);

                $this->checked = true;
            }
            elseif ($job->status == "accepted"){
                $this->checked = true;
            }
        }
    }
}
