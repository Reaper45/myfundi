<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
// use App\Http\Controllers\Traits\SendsSMS;
use App\AfricasTalkingGateway;
use Illuminate\Support\Facades\Log;

class SendSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $gateway;

    // africastalking credentials
    protected $username = 'pata_fundi';

    protected $key = 'b71dd3fcd2c3e0001e0f494e8b47f77c98cc217293479499f4b38d2247580e7a';

    // Sends to
    protected $recipients;

    // Message
    protected $message;

    // Results
    protected $sms_results;

    // Errors
    protected $sms_error;

    /**
     * Create a new job instance.
     *
     * @param $sms
     */
    public function __construct($sms)
    {
      $this->recipients = $sms['phone_number'];
      $this->message = $sms['message'];

    }

    /**
     * Execute the job.
     *
     * @return void
     */
     protected function getInstance()
     {
       if (!$this->gateway) {
         $this->gateway = new AfricasTalkingGateway($this->username, $this->key);
       }
       return $this->gateway;
     }

    protected function sendMessage()
    {
      $gateway = $this->getInstance();

      try
        {
          $this->sms_results = $gateway->sendMessage($this->recipients, $this->message);

          return true;
        }
        catch ( AfricasTalkingGatewayException $e )
        {
          $this->sms_error = "Encountered an error while sending: ".$e->getMessage();

          return false;
        }
    }

    public function handle()
    {
        return $this->sendMessage();
    }
}
