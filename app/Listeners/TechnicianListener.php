<?php

namespace App\Listeners;

use App\Events\TechnicianCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Auth\Events\Registered;

class TechnicianListener implements ShouldQueue
{
    // public $connection = 'database';
    //
    // public $queue = 'listeners';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TechnicianCreated  $event
     * @return void
     */
    public function handle(TechnicianCreated $event)
    {
        $user = $event->user;
        $user->notify(new ConfirmEmail());

        return false;
    }
}
