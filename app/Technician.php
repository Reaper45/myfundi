<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User\User;
use App\Company;
use Carbon\Carbon;
use App\Models\Traits\Model as ModelTrait;

class Technician extends Model
{
    use ModelTrait;
    
    protected $hidden = [];

    protected $guarded = [];

    protected $table = 'technicians';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_employees', 'user_id', 'company_id');
    }

    public function isValidated()
    {
        if ($this->id_card_no == null || !$this->certificate_valid || !$this->$good_conduct_valid) {
            return false;
        }
        return true;
    }

    /**
     * Format createdate (m-d-Y: h:i:s)
     *
     * @param string
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        $date = new Carbon($value);
        return $date->format('m-d-Y: h:i:s');
    }

    /**
     * Format updatedate (m-d-Y: h:i:s)
     *
     * @param string
     * @return string
     */
    public function getUpdatedAtAttribute($value)
    {
        $date = new Carbon($value);

        return $date->format('m-d-Y: h:i:s');
    }
}
