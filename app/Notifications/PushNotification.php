<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 10/24/17
 * Time: 3:06 PM
 */

namespace App\Notifications;


class PushNotification
{
    /**
     * FCM Authentication key
     *
     * @var string
     */
    private $server_key = "AAAAyYHXKNc:APA91bGGTzXQ_caIJli9Kswd_YOkcmYs2gCD1ztQvkMgUf4mQapPcBB7FVxC3ro66G_2L0nomvlhrcgTQ5vpRPOXnZR44Bx5xSP3yE32VOYv6sqSGwDiZtoznde359SdlkwFU3fkLi93";

    /**
     * Header for FCM
     *
     * @var array
     */
    private $headers = array();

    /**
     * Datapayload to send
     *
     * @var array
     */
    public $data = array();

    /**
     * Devices to send to
     *
     * @var array
     */
    public $devices;

    /**
     * @var mixed $results
     */
    public $results;
    /**
     * Create a new job instance.
     *
     * @param array $notification
     */
    public function __construct(array $notification)
    {
        $this->devices = $notification['to'];

        $this->headers = array (
            'Authorization: key='.$this->server_key,
            'Content-Type: application/json'
        );

        $this->data = $notification['data'];
    }

    /**
     * Send data
     *
     *
     * @internal param $fields
     * @internal param $
     */
    public function send(): void
    {
        $data_payload = array(
            'registration_ids' =>$this->devices,
            'data' => $this->data
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $this->headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $data_payload ) );

        $this->results = curl_exec($ch );
        curl_close( $ch );

        return;
    }
}