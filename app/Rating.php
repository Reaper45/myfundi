<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User\User;
use App\Models\Traits\Model as ModelTrait;

class Rating extends Model
{
	use ModelTrait;
	
    protected $hidden = ['updated_at'];

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
