<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User\User;
use App\Technician;
use App\Category;
use App\Models\Traits\Model as ModelTrait;

class Company extends Model
{
    use ModelTrait;
    
    protected $hidden = [];

    protected $guarded = [];

    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function technicians()
    {
      return $this->hasMany(Technician::class,'company_employees' , 'company_id', 'user_id');
    }

    /**
    * @categories
    * @return User::class
    */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'company_categories', 'company_id', 'category_id');
    }
}
