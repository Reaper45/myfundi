<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 10/18/17
 * Time: 12:30 PM
 */

namespace App\Observers;


use App\Models\Auth\User\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserObserver
{
    /**
     * Listen to the User deleting event.
     *
     * @param  User  $user
     * @return void
     */
    public function deleting(User $user): void
    {
        Log::info(Auth::user()->first_name.' ('.Auth::user()->id.')'.' Deleted user: '.$user);
    }

    public function updated(): void
    {

    }
}