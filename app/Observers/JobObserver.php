<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 10/18/17
 * Time: 12:30 PM
 */

namespace App\Observers;


use App\Job;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class JobObserver
{
    /**
     * Listen to the User deleting event.
     *
     * @param  Job  $job
     * @return void
     */
    public function  deleting(Job $job): void
    {
        Log::info(Auth::user()->first_name.' ('.Auth::user()->id.')'.' Deleted user: '.$job);
    }
}