<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Job;
use App\Models\Traits\Model as ModelTrait;

class Comment extends Model
{
    use ModelTrait;
    
	/**
	 * Hidden attr of the model
	 *
	 * @var array
	 */
    protected $hidden = [];

    /**
     * Database table associated with the Model
     *
     * @var string
     */
    protected $table = 'comments';

    /**
     * Define timestamps required for this model
     *
     * @var array
     */
    public $timestamps = ['created_at'];

    /**
     * The job a comment is associated with
     *
     * @param null
     * @return \App\Job
     */
    public function job()
    {
    	$this->belongsTo(Job::class);
    }

}
