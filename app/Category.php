<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Job;
use App\Service;
use App\Company;
use App\Models\Traits\Model as ModelTrait;

class Category extends Model
{
    use ModelTrait;
    
    protected $fillable = ['name', 'slug', 'description'];

    public $hidden = array();

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function services()
    {
      return $this->hasMany(Service::class);
    }

    public function jobs()
    {
      return $this->hasMany(Job::class);
    }

    /**
    * @companies
    */
    public function companies()
    {
        return $this->belongsToMany(Company::class, 'company_categories', 'category_id', 'company_id');
    }
}
