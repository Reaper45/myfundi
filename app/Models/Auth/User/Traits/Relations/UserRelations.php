<?php

namespace App\Models\Auth\User\Traits\Relations;

use App\Service;
use App\Company;
use App\Technician;
use App\Models\Auth\Role\Role;
use App\Models\Auth\User\SocialAccount;
use App\Models\Protection\ProtectionShopToken;
use App\Models\Protection\ProtectionValidation;
use App\Job;
use App\Rating;
use App\Device;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait UserRelations
{
    /**
     * Relation with role
     *
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'users_roles', 'user_id', 'role_id');
    }

    /**
     * Relation with social provider
     *
     * @return HasMany
     */
    public function providers()
    {
        return $this->hasMany(SocialAccount::class);
    }

    /**
     * Relation with protection validation
     *
     * @return mixed
     */
    public function protectionValidation()
    {
        return $this->hasOne(ProtectionValidation::class);
    }

    /**
     * Relation with protection shop tokens
     *
     * @return mixed
     */
    public function protectionShopTokens()
    {
        return $this->hasMany(ProtectionShopToken::class);
    }

    public function service()
    {
        return $this->belongsToMany(Service::class, 'technician_services', 'user_id', 'service_id');
    }

    public function technician()
    {
        return $this->hasOne(Technician::class);
    }

    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

    public function company()
    {
        return $this->hasOne(Company::class);
    }

    public function stars()
    {
        $ratings = Rating::where('rated_user', $this->id)->get();
        return $ratings;
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function devices()
    {
        return $this->hasMany(Device::class);
    }
}
