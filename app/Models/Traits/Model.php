<?php

namespace App\Models\Traits;

use Carbon\Carbon;

/**
* Defines Mutators and Accessors for models
*/
trait Model
{
    /**
    * Set atrributes of a model hidden on the fly.
    *
    * @param array $attr
    * @return void
    */
    public function setHidden(array $attr)
    {
        foreach ($attr as $value) {
            array_push($this->hidden, $value);
        }

        return $this;
    }

    /**
    * Add new properties to model
    * 
    * @param array $props
    * @return void
    */
    public function addAttr(array $props)
    {
    	foreach ($props as $key => $value) {
    		$this->$key = $value;
    	}
    	return $this;
    }

    /**
     * Format createdate (m-d-Y: h:i:s)
     *
     * @param string
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        $date = new Carbon($value);
        return $date->format('m-d-Y: h:i:s');
    }

    /**
     * Format updatedate (m-d-Y: h:i:s)
     *
     * @param string
     * @return string
     */
    public function getUpdatedAtAttribute($value)
    {
        $date = new Carbon($value);

        return $date->format('m-d-Y: h:i:s');
    }
}