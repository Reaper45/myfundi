		function initMap() {
			map = new google.maps.Map(document.getElementById(map_id), {
		    zoom: 12,
		    mapTypeControl: false,
		    streetViewControl: false,
		  	mapTypeId: google.maps.MapTypeId.ROADMAP,
		  	styles: [{
		      	stylers: [
		          	{ invert_lightness: true },
		          	{ hue: "#0091ff" }
		      	]
		  	}],
		  	center: new google.maps.LatLng(-4.048629, 39.704943),
		  	mapTypeId: 'terrain'
		});

		var script = document.createElement('script');
		
		script.src = "https://developers.google.com/maps/documentation/javascript/examples/json/earthquake_GeoJSONP.js";

		document.getElementsByTagName('head')[0].appendChild(script);
		}
		
		$.get(coordsurl, function(results) {
			
			for (var i = 0; i < results.length; i++) {
				var coords = results[i]
				var latLng = new google.maps.LatLng(coords.lat, coords.lon);
				var marker = new google.maps.Marker({
					position: latLng,
					map: map
				});
			}
		});