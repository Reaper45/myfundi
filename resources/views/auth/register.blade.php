<div class="animate form">
    <section class="login_content">

        {{ Form::open(['route' => 'become.fundi']) }}
        {{-- <hr> --}}

        {{ Form::token() }}
        <div class="row">
            <div class="col-md-6  col-sm-12">
                <div class="form-group {{$errors->has('first_name') ? ' has-error' : '' }}">
                    {{ Form::text('first_name', old('first_name'), ['class' => 'form-control', 'required' => 'true', 'placeholder'=>'First Name']) }}
                    @if ($errors->has('first_name'))
                        <p class="text-danger" role="alert">
                            {!! $errors->first('first_name') !!}
                        </p>
                    @endif
                </div>
            </div>
            <div class="col-md-6  col-sm-12">
                <div class="form-group {{ $errors->has('last_name') ? ' has-error' : ''  }}">
                    {{ Form::text('last_name', old('last_name'), ['class' => 'form-control', 'required' => 'true', 'placeholder'=>'Last Name']) }}
                    @if ($errors->has('last_name'))
                        <p class="text-danger" role="alert">
                            {!! $errors->first('last_name') !!}
                        </p>
                    @endif
                </div>
            </div>
        </div>

        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            {{ Form::email('email', old('email'), ['class' => 'form-control', 'required' => 'true', 'placeholder'=>'Email Address']) }}
            @if ($errors->has('email'))
                <p class="text-danger" role="alert">
                    {!! $errors->first('email') !!}
                </p>
            @endif
        </div>

        <div class="form-group {{ $errors->has('phone_number') ? ' has-error' : ''  }}">
            {{ Form::text('phone_number', old('phone_number'), ['class' => 'form-control', 'required' => 'true', 'placeholder'=>'Phone Number']) }}
            @if ($errors->has('phone_number'))
                <p class="text-danger" role="alert">
                    {!! $errors->first('phone_number') !!}
                </p>
            @endif
        </div>

        <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }}">
            <div id="gender" class="btn-group" data-toggle="buttons">
                <p class="btn btn-common gender" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                    {{ Form::radio('gender', 'male', ['id'=>'male']) }}&nbsp; Male
                </p>
                <p class="btn btn-common gender"
                       data-toggle-class="btn-primary"
                       data-toggle-passive-class="btn-default"
                       data-date-format="yyyy/mm/dd">
                    {{ Form::radio('gender', 'female', ['id'=>'male']) }}&nbsp;Female
                </p>
            </div>
            @if ($errors->has('gender'))
                <p class="text-danger" role="alert">
                    {!! $errors->first('gender') !!}
                </p>
            @endif
        </div>

        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            {{ Form::password('password', ['class' => 'form-control', 'required' => 'true', 'placeholder'=>'Password']) }}
            @if ($errors->has('password'))
                <p class="text-danger" role="alert">
                    {!! $errors->first('password') !!}
                </p>
            @endif
        </div>

        {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'true', 'placeholder'=>'Confirm Password']) }}

        {{ Form::text('location_name', old('location_name'), ['class' => 'form-control', 'required' => 'true', 'placeholder'=>'City']) }}

        {{ Form::submit('Submit', ['class' => 'btn btn-common btn-register', 'style'=>'width:100%; border-radius: 0px;']) }}
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        {{ Form::close() }}
    </section>
</div>
