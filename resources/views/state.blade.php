<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 9/22/17
 * Time: 6:04 AM
 */
?>

@extends('layouts.patafundi')

@section('title')
    Sign Up - Almost Done
@endsection

@section('styles')
    @parent
    <style>
        .form-control{
            padding: 10px 10px !important;
            height: 45px !important;
        }
        .logo-menu{
            display: none !important;
        }
        footer{
            display: none !important;
        }
        .job-list{
            width: 100%;
        }
        .job-list:hover {
            cursor: pointer;
            color: #FF9D02;
        }
        .job-list.active{
            border: 1px solid #FF9D02;
        }
        .job-list.active .title{
            color: #FF9D02 !important;
        }
        .hide{
            display: none;
        }
        a.btn-common:hover .ti-arrow-right{
            transition: margin 200ms;
            margin-right: -10px;
        }
        .ti-info{
            color: #FF9D02;
        }
    </style>
@endsection

@section('page-header')
    <!-- Page Header Start -->
    <div class="page-header" style="background: url(assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Become a Fundi</h2>
                        <ol class="breadcrumb">
                            <li><a href="/"><i class="ti-home"></i>&nbsp;Home</a></li>
                            <li class="current">Sign Up - Almost Done</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2" style="padding-top: 15px;">
                <h2 class="title headline">Please choose how you would like to register into Patafundi. <i class="fa fa-exclamation-circle pull-right" aria-hidden="true"></i></h2>
                <div class="job-list card active" id="fundi">
                    <h3 class="title">Technician (Fundi)<i class="ti-check pull-right" aria-hidden="true"></i></h3>
                    <p>You are a certified technician or skilled personnel. You are looking to be hired directly for jobs posts in Patafundi.</p>
                </div>
                <div class="job-list" id="company">
                    <h3 class="title">Company<i class="ti-check pull-right icon hide" aria-hidden="true"></i></h3>
                    <p>Register as a company with not less than 5 technicians. Pick jobs as an organisation adn assign to your technicians.</p>
                </div>

                <button type="submit" form="setstate" class="btn btn-common" style="border-radius: 0px; width: 100%;">Continue <i class="ti-arrow-right" style="color: #fff; right: 30px; position: absolute;" aria-hidden="true"></i></button>
                <form id="setstate" method="get" action="{{ route('fundi.state', ['uid'=>$uid]) }}">
{{--                    <input type="hidden" name="uuid" value="{{  }}">--}}
                    <input id="state" type="hidden" name="state" value="fundi">
                </form>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>

        $('.job-list').click(function (e) {
            e.preventDefault()
//            console.log('hit')
            $('.job-list').removeClass('active')
            $('.ti-check').addClass('hide')

            $(this).children('h3').children('i').removeClass('hide')
            $(this).addClass('active')
            $('#state').val($(this).attr('id'))

        })
    </script>
    @endsection
