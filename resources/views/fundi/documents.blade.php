<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 9/23/17
 * Time: 1:11 AM
 */
?>

<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 9/22/17
 * Time: 3:38 PM
 */
?>

@extends('layouts.fundi-account')

@section('title')
    Dashboard - {{ Auth::guest()?'':Auth::user()->first_name.' '.Auth::user()->last_name }}
@endsection

@section('content')
    @parent

    <div class="container push-bottom-xx">
        <div class="row">
            <h2 class="title">Your Documents</h2>
            @if(!auth()->user()->technician->certificate || !auth()->user()->technician->good_conduct || !auth()->user()->technician->id_card)
                <p class="alert alert-info">Make sure you have uploaded all the highlighted document below. The documents will be used reviewed and if verified your account.<i class="fa fa-exclamation-circle pull-right" aria-hidden="true"></i></p>
            @endif
        </div>
        <div class="row">
            <div class="container">
                <div class="row document">
                    <div class="col-md-6 col-sm-4 col-xs-12"><p>Technical certificate</p></div>
                    <div class="col-md-3 col-sm-4 col-xs-6">
                        @if(!auth()->user()->technician->certificate)
                            <span class="label label-danger" style="border-radius: 0px">Missing &nbsp;<i class="ti-close"></i></span>
                        @else
                            @if(auth()->user()->technician->certificate_valid)
                                <span class="label label-success" style="border-radius: 0px">Valid &nbsp;<i class="ti-check"></i></span>
                            @else
                                <span class="label label-warning" style="border-radius: 0px">Pending Validation &nbsp;<i class="ti-info"></i></span>
                            @endif
                        @endif
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-6"><a data-toggle="modal" data-target="#uploadAcdCert" class="btn btn-default btn-upload">Upload</a></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <div class="row document">
                    <div class="col-md-6 col-sm-4 col-xs-12"><p>Certificate of Good Conduct</p></div>
                    <div class="col-md-3 col-sm-4 col-xs-6">
                        @if(!auth()->user()->technician->good_conduct)
                            <span class="label label-danger" style="border-radius: 0px">Missing &nbsp;<i class="ti-close"></i></span>
                        @else
                            @if(auth()->user()->technician->good_conduct_valid)
                                <span class="label label-success" style="border-radius: 0px">Valid &nbsp;<i class="ti-check"></i></span>
                            @else
                                <span class="label label-warning" style="border-radius: 0px">Pending Validation &nbsp;<i class="ti-info"></i></span>
                            @endif
                        @endif
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-6"><a data-toggle="modal" data-target="#uploadGoodCndt"  class="btn btn-default btn-upload">Upload</a></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <div class="row document">
                    <div class="col-md-6 col-sm-4 col-xs-12"><p>Nationa ID card</p></div>
                    <div class="col-md-3 col-sm-4 col-xs-6">
                        @if(!auth()->user()->technician->id_card)
                            <span class="label label-danger" style="border-radius: 0px">Missing &nbsp;<i class="ti-close"></i></span>
                        @else
                            @if(auth()->user()->technician->id_card_valid)
                                <span class="label label-success" style="border-radius: 0px">Valid &nbsp;<i class="ti-check"></i></span>
                            @else
                                <span class="label label-warning" style="border-radius: 0px">Pending Validation &nbsp;<i class="ti-info"></i></span>
                            @endif
                        @endif
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-6"><a data-toggle="modal" data-target="#uploadNatId"  class="btn btn-default btn-upload">Upload</a></div>
                </div>
            </div>
        </div>
    </div>

    {{--Upload Modals--}}
    <div class="modal fade" id="uploadGoodCndt" tabindex="-1" role="dialog" aria-labelledby="uploadGoodCndtLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Upload Good Conduct</h4>
                </div>
                <div class="modal-body">
                    <form id="uploadGoodCndtForm" action="{{ action('TechnicianController@uploadCertificateOfGoodConduct') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="gc_file" class="control-label">Good Conduct:</label>
                            <input type="file" class="form-control" id="gc_file" name="gc_file">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" form="uploadGoodCndtForm" class="btn btn-common" style="padding: 8px 20px;">Upload File</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="modal fade" id="uploadAcdCert" tabindex="-1" role="dialog" aria-labelledby="uploadAcdCertLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Upload Academic Certificate</h4>
                </div>
                <div class="modal-body">
                    <form id="uploadAcdCertForm" action="{{ action('TechnicianController@uploadCertificate') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="certificate" class="control-label">Academic Certificate:</label>
                            <input type="file" class="form-control" id="certificate" name="certificate">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" form="uploadAcdCertForm" class="btn btn-common" style="padding: 8px 20px;">Upload File</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="modal fade" id="uploadNatId" tabindex="-1" role="dialog" aria-labelledby="uploadNatIdLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Upload National ID. card</h4>
                </div>
                <div class="modal-body">
                    <form id="uploadNatIdForm" action="{{ action('TechnicianController@uploadNationaIdCard') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="national_id" class="control-label">National ID:</label>
                            <input type="file" class="form-control" id="national_id" name="national_id">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" form="uploadNatIdForm" class="btn btn-common" style="padding: 8px 20px;">Upload File</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('scripts')
    @parent
    <script>
        $('#documents-tab').addClass('active')
    </script>
@endsection



