<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 9/22/17
 * Time: 3:38 PM
 */
?>

@extends('layouts.fundi-account')

@section('title')
    Dashboard - {{ Auth::guest()?'':Auth::user()->first_name.' '.Auth::user()->last_name }}
@endsection

@section('styles')
    @parent
    <style>
        .feedback img{
            width: 40px;
            height: 40px;
        }
         #message{
             display: none;
         }
        #picklocationform{
            margin-bottom: 0px !important;
            padding: 8px 20px 8px 45px !important;
        }
        #picklocationform.alert{
            border-radius: 3px !important;
        }
        #picklocationform .ti-info-alt{
            margin-left: 5px !important;
            color: #fff !important;
        }
        .modal .modal-dialog {
            margin-top: 70px !important;
        }
        @media screen and (max-width: 990px){
            .col-md-6{
                padding-left: 0px;
                padding-right: 0px;
            }
            .user-content{
                text-align: center;
            }
            .first-name{
                float: left;
                margin-right: 10px;
                width: 50%;
                text-align: right;
            }
            .last-name{
                line-height: 2.4;
            }
        }
    </style>
@endsection

@section('content')
    @parent
        <div class="container push-bottom-2x">
            <div class="row">
                <div class="col-md-8">
                    <a href="{{ route('fundi.documents') }}" class="btn btn-common">Manage Documents <i class="ti-arrow-right"></i></a>
                    <div class="divider"></div>
                    <div>
                        {{ Form::open(['route'=>'fundi.update']) }}

                        {{ Form::label('email', 'Email', ['class' => 'control-label', 'for'=>'email']) }}
                        {{ Form::email('email', auth()->user()->email, ['class' => 'form-control', 'disabled'=>'true']) }}

                        {{ Form::label('phone_number', 'Phone Name', ['class' => 'control-label', 'for'=>'phone_number']) }}
                        {{ Form::text('phone_number', auth()->user()->phone_number, ['class' => 'form-control', 'required' => 'true', 'disabled'=>'true']) }}


                        {{ Form::label('dob', 'Date Of Birth', ['class' => 'control-label']) }}
                        <span class="pull-right" style="color: #0ab1fc;">( Format: YYYY-MM-DD )</span>
                        {{ Form::text('dob',  auth()->user()->technician->dob ? auth()->user()->technician->dob : \Carbon\Carbon::today()->toDateString(), ['class' => 'form-control', 'required' => 'true']) }}

                        <div class="divider"></div>

                        <div class="col-md-6" style="padding-left: 0px">
                            {{ Form::label('id_card_no', 'ID No.',['class' => 'control-label', 'for'=>'id_card_no']) }}
                            <span class="pull-right" style="color: #0ab1fc;">
                                <i class="ti-info-alt"></i>
                                Don't forget to upload your ID card &nbsp;
                                <a href="{{ route('fundi.documents') }}"  style="color: #FF9D02;">Here</a>
                            </span>
                            {{ Form::text('id_card_no', auth()->user()->technician->id_card_no, ['class' => 'form-control', 'required'=>'true']) }}

                        </div>
                        <div class="col-md-6" style="padding-right: 0px">
                            {{ Form::label('kra_pin_no', 'KRA Pin', ['class' => 'control-label']) }}
                            {{ Form::text('kra_pin_no', auth()->user()->technician->kra_pin_no, ['class' => 'form-control','placeholder'=>'Enter KRA pin number...', 'required'=>'true']) }}
                        </div>

                        <div class="text-left">
                            {{ Form::label('location', 'Location', ['class' => 'control-label', 'style'=>'width: 100%']) }}

                            <a data-toggle="modal" data-target="#locationModal"  class="btn btn-common">Pick Location</a>

                            <div class="text-center">
                                <input id="lon" type="hidden" name="lon" value="">
                                <input id="lat" type="hidden" name="lat" value="">

                                <div id="message">
                                    <div id='picklocationform' class="alert alert-info"><i class="ti-info-alt"></i>Geo Location Added</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6" style="padding-left: 0px">
                            {{ Form::label('country', 'Country',['class' => 'control-label', 'for'=>'country']) }}
                            {{ Form::select('country', ['Kenya'=>'Kenya', 'Tanzana'=>'Tanzana'], auth()->user()->technician->country, ['class' => 'form-control', 'required'=>'true']) }}

                        </div>
                        <div class="col-md-6" style="padding-right: 0px">
                            {{ Form::label('location_name', 'City', ['class' => 'control-label']) }}
                            {{ Form::text('location_name', auth()->user()->technician->location_name, ['class' => 'form-control','placeholder'=>'Enter City', 'required'=>'true']) }}
                        </div>

                        {{ Form::submit('Update', ['class' => 'btn btn-common btn-register', 'style'=>'border-radius: 0px;']) }}

                        {{ Form::close() }}
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="push-bottom">
                        <h3 class="title">Feedback</h3>
                        <div class="divider"></div>
                    </div>

                    <div class="feedback push-bottom">
                        <div class="row">
                            @if(count($feedback) > 0)
                                @foreach($feedback as $item)
                                <div class="col-md-3">
                                    @if(Storage::disk('profile')->has(App\Models\Auth\User\User::find($item->user_id)->technician->avartar))
                                        <img src="{{ route('avatar',['filename'=>App\Models\Auth\User\User::find($item->user_id)->technician->avartar]) }}" alt="" class="img-circle">
                                    @else
                                        <img src="{{ asset('assets/img/default.png') }}" alt="" class="img-circle">
                                    @endif
                                </div>
                                <div class="col-md-9">
                                    <h5>{{$item->user->first_name.' '.$item->user->last_name }}</h5>
                                    {{--<p>{{ $item->content }}</p>--}}
                                    <p>
                                        @for($i=1; $i <= $item->stars; $i++)
                                        {!! '<i class="ti-star" style="color: #FF9D02;"></i>' !!}
                                            @endfor
                                    </p>
                                </div>
                                    @endforeach
                            @else
                                <div class="col-md-12 alert alert-info">
                                    <i class="ti-info-alt"></i>
                                    <span class="title"> Don't forget to ask your clients to rate your services!</span>
                                </div>
                            @endif
                        </div>
                        <div class="divider"></div>
                    </div>
                </div>
            </div>
        </div>

        {{--Upload Modals--}}
        <div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="locationModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update Location</h4>
                        <p class="title">Click on the map, then click the macker to save location.</p>
                    </div>
                    <div class="modal-body" style="width: 100%; height: 420px;" id="picklocation"></div>
                    <div class="modal-footer">
                        <br>
                        <button class="btn btn-common" data-dismiss="modal" style="padding: 8px 20px;">Save Location</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        $('#profile-tab').addClass('active')

        var MARKER_COLORS = [ 'blue', 'red', 'green', 'orange', 'pink', 'purple', 'yellow' ];

        var log = document.getElementById('lon');
        var lat = document.getElementById('lat');
        var map;
        var marker;
        var infowindow;
        var messagewindow;

        $('#locationModal').on('shown.bs.modal', function () {
            initMap()
        })
        initMap = function() {
            var mapOptions = {
                center: new google.maps.LatLng(-4.0435, 39.6682),
                zoom: 15,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL
                },
                mapTypeControl: false,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    stylers: [
                        { invert_lightness: true },
                        { hue: "#0091ff" }
                    ]
                }]
            };

            infowindow = new google.maps.InfoWindow({
                content: document.getElementById('picklocationform')
            });

            map = new google.maps.Map(document.getElementById('picklocation'), mapOptions);

            messagewindow = new google.maps.InfoWindow({
                content: document.getElementById('message')
            });

            google.maps.event.addListener(map, 'click', function(event) {

                marker = new google.maps.Marker({
                    position: event.latLng,
                    map: map
                });

                google.maps.event.addListener(marker, 'click', function(){
                    infowindow.open(map, marker);
                    var latlng = marker.getPosition();

                    lat.value = latlng.lat();
                    lon.value = latlng.lng();
                });
            });

        };
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBegRx25srxedzqkSyfs-glxltcyMRRNAQ&callback=initMap"> </script>

@endsection