@if(Session::has('success'))
  <div class="alert-container">
    <li class="alert alert-success alert-dismissible message" role="alert" style="list-style: none;">
        <strong><i style="font-size: 1.5em; padding-right: 5px;"  class="fa fa-check-circle" aria-hidden="true"></i>Done!</strong>
        {{ Session::get('success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button></li>
          </div>
@endif


@if(Session::has('info'))
  <div class="alert-container">
    <li class="alert alert-info alert-dismissible message" role="alert" style="list-style: none;">
      <strong>
        <i style="font-size: 1.5em; padding-right: 5px;"  class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
        Heads Up!
      </strong>
      {{ Session::get('info')}}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </li>
  </div>
@endif


@if(Session::has('errors'))
  <div class="alert-container">
    @foreach(Session::get('errors') as $error)
    <li class="alert alert-error alert-dismissible message" role="alert" style="list-style: none;">
      <strong>
        <i style="font-size: 1.5em; padding-right: 5px;"  class="fa fa-minus-circle fa-2x" aria-hidden="true"></i>
        Heads Up!
      </strong>
      {{ $error }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </li>
  @endforeach
  </div>
@endif

{{-- validation Errors --}}
@if($errors->count() > 0)
  <div class="alert-container">
    @foreach($errors->all() as $error)
      <li class="alert alert-danger alert-dismissible message" role="alert" style="list-style: none;">
        <strong><i style="font-size: 1.5em; padding-right: 5px;" class="fa fa-minus-circle" aria-hidden="true"></i>Failed!</strong>
        {{ $error }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button></li>
        @endforeach
  </div>
    @endif
