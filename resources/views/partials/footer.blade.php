<!-- Footer up Here -->
<!-- Footer Section Start -->
<footer>
  <!-- Footer Area Start -->
  <section class="footer-Content">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="widget">

            <a href="{{ route('home') }}">
              <h2 class="title" style="color: #FF9D02">PataFundi</h2>
            </a>
            <div class="textwidget">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis tincidunt est, et euismod purus suscipit quis. Etiam euismod ornare elementum. Sed ex est, consectetur eget facilisis sed, auctor ut purus.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-md-offset-1 col-sm-6 col-xs-12">
          <div class="widget">
            <h3 class="block-title">Quick Links</h3>
            <ul class="menu">
              <li><a href="{{ route('about') }}">About Us</a></li>
              <li><a href="#">Support</a></li>
              <li><a href="#">License</a></li>
              <li><a href="#">Terms & Conditions</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div>
        </div>
        {{--<div class="col-md-3 col-sm-6 col-xs-12">--}}
          {{--<div class="widget">--}}
            {{--<h3 class="block-title">Trending Jobs</h3>--}}
            {{--<ul class="menu">--}}
              {{--<li><a href="#">Android Developer</a></li>--}}
              {{--<li><a href="#">Senior Teamleader</a></li>--}}
              {{--<li><a href="#">iOS Developer</a></li>--}}
              {{--<li><a href="#">Junior Tester</a></li>--}}
              {{--<li><a href="#">Full Stack Developer</a></li>--}}
            {{--</ul>--}}
          {{--</div>--}}
        {{--</div>--}}
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="widget">
            <h3 class="block-title">Follow Us</h3>
            <div class="bottom-social-icons social-icon">
              <a class="twitter" href="https://twitter.com/{{ config('app.name')}}"><i class="ti-twitter-alt"></i></a>
              <a class="facebook" href="https://web.facebook.com/{{ config('app.name')}}"><i class="ti-facebook"></i></a>
              {{-- <a class="youtube" href="https://youtube.com"><i class="ti-youtube"></i></a>
              <a class="dribble" href="https://dribbble.com/"><i class="ti-dribbble"></i></a> --}}
              <a class="linkedin" href="https://www.linkedin.com/{{ config('app.name')}}"><i class="ti-linkedin"></i></a>
            </div>
            <p>Join our mailing list to stay up to date and get notices about our new releases!</p>
            <form class="subscribe-box">
              <input type="text" placeholder="Your email">
              <input type="submit" class="btn-system" value="Send">
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Footer area End -->
  <!-- Copyright Start  -->
  <div id="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p>All Rights reserved &copy; 2017 -
            <a rel="nofollow" href="{{config('app.url')}}"> {{ config('app.name')}}</a>
          </p>
        </div>
      </div>
    </div>
  </div>
    <!-- Copyright End -->

</footer>
<!-- Footer Section End -->

<!-- Go To Top Link -->
<a href="#" class="back-to-top">
  <i class="ti-arrow-up"></i>
</a>

<div id="loading">
  <div id="loading-center">
    <div id="loading-center-absolute">
      <div class="object" id="object_one"></div>
      <div class="object" id="object_two"></div>
      <div class="object" id="object_three"></div>
      <div class="object" id="object_four"></div>
      <div class="object" id="object_five"></div>
      <div class="object" id="object_six"></div>
      <div class="object" id="object_seven"></div>
      <div class="object" id="object_eight"></div>
    </div>
  </div>
</div>
