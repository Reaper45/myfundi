    <!-- Bootstrap CSS -->
    {{ Html::style(asset('assets/theme/css/bootstrap.min.css')) }}
    {{ Html::style(asset('assets/theme/css/jasny-bootstrap.min.css')) }}
    {{ Html::style(asset('assets/theme/css/bootstrap-select.min.css')) }}
    <!-- Material CSS -->
    {{ Html::style(asset('assets/theme/css/material-kit.css')) }}
    <!-- Font Awesome CSS -->
    {{ Html::style(asset('assets/theme/fonts/font-awesome.min.css')) }}
    {{ Html::style(asset('assets/theme/fonts/themify-icons.css')) }}
    <!-- Animate CSS -->
    {{ Html::style(asset('assets/theme/css/animate.css')) }}
    <!-- Owl Carousel -->
    {{ Html::style(asset('assets/theme/css/owl.carousel.css')) }}
    {{ Html::style(asset('assets/theme/css/owl.theme.css')) }}
    <!-- Rev Slider CSS -->
    {{ Html::style(asset('assets/theme/css/settings.css')) }}
    <!-- Slicknav js -->
    {{ Html::style(asset('assets/theme/css/slicknav.css')) }}
    <!-- Main Styles -->
    {{ Html::style(asset('assets/theme/css/main.css')) }}
    <!-- Responsive CSS Styles -->
    {{ Html::style(asset('assets/theme/css/responsive.css')) }}
    <!-- Color CSS Styles  -->
    {{ Html::style(asset('assets/theme/css/colors/yellow.css')) }}
