<!-- Main JS  -->
<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-messaging.js"></script>
{{-- <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-messaging-sw.js"></script> --}}

<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAX3_-6IXE3ir5bYzrs0ycrBrXUdW6yevc",
    authDomain: "pata-fundi-1501419823853.firebaseapp.com",
    databaseURL: "https://pata-fundi-1501419823853.firebaseio.com",
    projectId: "pata-fundi-1501419823853",
    storageBucket: "pata-fundi-1501419823853.appspot.com",
    messagingSenderId: "475184609755"
  };
  firebase.initializeApp(config);
  const messaging = firebase.messaging();

  messaging.requestPermission()
  .then(function() {
    console.log('Notification permission granted.');
    messaging.getToken()
    .then(function(currentToken) {
      if (currentToken) {
        console.log(currentToken);
        sendTokenToServer(currentToken);
        // updateUIForPushEnabled(currentToken);
      } else {
        // Show permission request.
        console.log('No Instance ID token available. Request permission to generate one.');
        // Show permission UI.
        // updateUIForPushPermissionRequired();
        setTokenSentToServer(false);
      }
    })
    .catch(function(err) {
      console.log('An error occurred while retrieving token. ', err);
      // showToken('Error retrieving Instance ID token. ', err);
      setTokenSentToServer(false);
    });

  })
  .catch(function(err) {
    console.log('Unable to get permission to notify.', err);
  });

  function setTokenSentToServer(token, sent) {
    window.localStorage.setItem('sentToServer', sent ? 1 : 0);
    window.localStorage.setItem('token', token);
  }

  function sendTokenToServer(currentToken) {
    if (isTokenSentToServer()) {
      console.log('Sending token to server...');
      // TODO(developer): Send the current token to your server.

      setTokenSentToServer(currentToken,true);
    } else {
      console.log('Token already sent to server so won\'t send it again ' +
          'unless it changes');
    }
  }
  function isTokenSentToServer() {
   return window.localStorage.getItem('sentToServer') == 1;
 }
 var key = 'AAAAbqM1uds:APA91bFjaWkeqRjejiYF4IUrvqJNEIrlvjLuB2mFtCBXQ54LreAScFB7SKPSPFs8s6qDgWZgR4V3zAvyvgHqAkRcqg_aBMdlVNHGbb_ePSSH20bGW2lI2WdQK-VDFG1WiE2wUMOkumet';
 var to = window.localStorage.getItem('token');
 var notification = {
   'title': 'Portugal vs. Denmark',
   'body': '5 to 1',
   'icon': 'firebase-logo.png',
   'click_action': 'http://localhost:8000'
 };

 fetch('https://fcm.googleapis.com/fcm/send', {
   'method': 'POST',
   'headers': {
     'Authorization': 'key=' + key,
     'Content-Type': 'application/json'
   },
   'body': JSON.stringify({
     'notification': notification,
     'to': to
   })
 }).then(function(response) {
   console.log(response);
 }).catch(function(error) {
   console.error(error);
 })
</script>

  {{ Html::script(asset('assets/theme/js/jquery-min.js')) }}
  {{ Html::script(asset('assets/theme/js/bootstrap.min.js')) }}
  {{ Html::script(asset('assets/theme/js/material.min.js')) }}
  {{ Html::script(asset('assets/theme/js/material-kit.js')) }}
  {{ Html::script(asset('assets/theme/js/jquery.parallax.js')) }}
  {{ Html::script(asset('assets/theme/js/owl.carousel.min.js')) }}
  {{ Html::script(asset('assets/theme/js/jquery.slicknav.js')) }}
  {{ Html::script(asset('assets/theme/js/main.js')) }}
  {{ Html::script(asset('assets/theme/js/jquery.counterup.min.js')) }}
  {{ Html::script(asset('assets/theme/js/waypoints.min.js')) }}
  {{ Html::script(asset('assets/theme/js/jasny-bootstrap.min.js')) }}
  {{ Html::script(asset('assets/theme/js/bootstrap-select.min.js')) }}
  {{ Html::script(asset('assets/theme/js/form-validator.min.js')) }}
  {{ Html::script(asset('assets/theme/js/contact-form-script.js')) }}
  {{ Html::script(asset('assets/theme/js/jquery.themepunch.revolution.min.js')) }}
  {{ Html::script(asset('assets/theme/js/jquery.themepunch.tools.min.js')) }}
