<nav class="navbar navbar-default" role="navigation" data-spy="affix" data-offset-top="50">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      {{--<a class="navbar-brand" href="/"><img src="{{url('assets/theme/img/logo.png')}}" alt=""></a>--}}
      <a class="navbar-brand" href="{{ route('home') }}"><h3 class="title" style="color: #FF9D02">PataFundi</h3></a>
    </div>

    <div class="collapse navbar-collapse" id="navbar">
      <!-- Start Navigation List -->
      <ul class="nav navbar-nav">
        <li>
          <a class="" href="{{ route('home') }}">
            <i class="ti-home" style="margin-right: 8px"></i>
            Home
          </a>
        </li>
        <li>
          <a href="/about">
            <i class="ti-direction-alt" style="margin-right: 8px"></i>
            About
          </a>
        </li>

      </ul>
      <ul class="nav navbar-nav navbar-right float-right">
        @if (Auth::guest())
          <li class="right"><a href="{{ route('login')}}"><i class="ti-lock"></i> SIGN IN</a></li>

          <li class="left"><a href="{{ route('become.fundi') }}">BECOME A FUNDI</a></li>
        @else
          <li class="right">
            <a clas="auth-name" style="border: 0px">
              {{ auth()->user()->first_name.' '.auth()->user()->last_name }}
              <i class="ti-angle-down"></i>
            </a>
            <ul class="dropdown" role="menu" style="border-radius: 0px;">
              @if(auth()->user()->hasRole('administrator'))
                <li>
                  <a href="{{ url('/admin') }}">{{ __('views.welcome.admin') }}</a>
                </li>
                @elseif(auth()->user()->hasRole('technician'))
                <li>
                  <a href="{{ url('fundi/dashboard') }}">Your Profile</a>
                </li>
              @endif
              <li>
                <a href="{{ url('/logout') }}">
                  <i class="fa fa-sign-out float-right"></i>
                  {{ __('views.welcome.logout') }}
                </a>
              </li>
            </ul>
          </li>
        @endif

      </ul>
    </div>
  </div>
  <!-- Mobile Menu Start -->
  <ul class="wpb-mobile-menu">
    <li>
      <a class="active" href="/about">About</a>

    @if (Auth::guest())
      <li><a href="{{ route('login')}}"><i class="ti-unlock"></i>SIGN IN</a></li>

      <li class="btn-m"><a href="#">BECOME A FUNDI</a></li>
    @else
      <li class="right">
        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
          {{ auth()->user()->first_name.' '.Auth::user()->last_name }}
          <i class="ti-angle-down"></i>
        </a>
        <ul class="" role="menu">
          @if(auth()->user()->hasRole('administrator'))
            <li>
              <a href="{{ url('/admin') }}">{{ __('views.welcome.admin') }}</a>
            </li>
          @elseif(auth()->user()->hasRole('technician'))
            <li>
              <a href="{{ url('fundi/dashboard') }}">Your Profile</a>
            </li>
          @endif
          <li>
            <a href="{{ url('/logout') }}">
              <i class="fa fa-sign-out float-right"></i>
              {{ __('views.welcome.logout') }}
            </a>
          </li>
        </ul>
      </li>
    @endif
  </ul>
  <!-- Mobile Menu End -->
</nav>
