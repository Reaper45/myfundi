<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Meta title & meta -->
  <title>{{ config('app.name') }} | @yield('title')</title>
  @meta

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:200,600" rel="stylesheet" type="text/css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

  <!-- Styles -->
  @include('partials.mini.theme')
  {{ Html::style(asset('assets/custom/css/master.css'))}}
  <!-- Laravel variables for js -->
  <style>
    .navbar-default .navbar-nav > li > a{
      border-radius: 0 !important;
    }
    .navbar-collapse{
      padding-right: 0px;
      padding-left: 0px;
    }
    h1, h2, h3, h4, h5, h6{
      font-family: 'Raleway', sans-serif !important;
    }
    .navbar-default .navbar-nav > li > a.auth-name{
      /*border-radius: 0px !important;*/
      border: none !important;
      background: transparent !important;
    }
  </style>
  @yield('styles')

  @tojs
</head>
<body>
  <!-- Header Section Start -->
  <div class="header">
    <!-- Start intro section -->
        <section id="intro-bg">
          <div class="logo-menu">
            @include('partials.nav')
          </div>
          <!-- Header Section End -->
          @yield('page-header')
        </section>
  </div>

  <div>
    @yield('content')
  </div>

  @include('partials.footer')
  @include('partials.mini.scripts')

@yield('scripts')
</body>
</html>
