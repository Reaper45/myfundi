<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 9/22/17
 * Time: 3:54 PM
 */
?>

@extends('layouts.patafundi')

@section('styles')
    <style>
        body{
            background: #f5f5f5 !important;
        }
        .footer-Content{
            display: none !important;
        }
        .account-nav li > a{
            text-ftransform: uppercase;
            font-weight: 200;
        }
        .account-nav li .active{
            border-bottom: solid 2px #FF9D02;
        }
        .profile-title{
            margin-top: 10px;
            margin-bottom: 15px;
            background: #fff;
            border-bottom: solid 1px #cccccc;
            padding: 20px 10px;
        }
        .profile-pic{
            width: 100%;
            max-width: 300px;
            margin-bottom: 10px;
        }
        .document{
            background: #fff;
            border-bottom: solid 1px #ccc;
        }
        .label{
            vertical-align: middle;
        }

        .btn-upload{
            border-radius: 0px;
            border: solid 2px #FF9D02;
            padding: 2px 15px;
        }
        .btn-upload:hover{
            border: solid 2px #FF9D02;
            background: #FF9D02;
            color: #fff !important;
        }
        .document{
            padding: 20px;
        }
        .modal-header{
            padding-bottom: 10px;
            border-bottom: solid 1px #ccc !important;
        }
        .modal-footer{
            padding-top: 10px;
            border-top: solid 1px #ccc !important;
        }
        input[type='file']{
            opacity: 1 !important;
            position: initial !important;
        }
        @media screen and (max-width: 990px){
            .col-md-6{
                padding-left: 0px;
                padding-right: 0px;
            }
            .user-content{
                text-align: center;
            }
            .first-name{
                float: left;
                margin-right: 10px;
                width: 50%;
                text-align: right;
            }
            .last-name{
                line-height: 2.4;
                text-align: left;
            }
            .new-profile-pic{
                top: -60px;
            }
        }
    </style>
@endsection

@section('content')
    <nav class="navbar" style="background: #fff;">
        <div class="container">
            <ul class="nav navbar-nav account-nav">
                <li>
                    <a id="profile-tab" href="{{ route('fundi.dashboard') }}">
                        <i class="ti-user" style="margin-right: 8px"></i>
                        Profile
                    </a>
                </li>
                <li>
                    <a id="documents-tab" href="{{ route('fundi.documents') }}">
                        <i class="ti-files" style="margin-right: 8px"></i>
                        Manage Documents
                    </a>
                </li>

            </ul>
        </div>
    </nav>

    @if(!auth()->user()->confirmed )
        <div class="row">
            <div class="col-md-12">
                <p class="alert alert-warning" style="margin-bottom: 0px; margin-top: 5px;">
                    <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                    You haven't confirmed you email address. Please confirm your email first.
                </p>
            </div>
        </div>
    @endif

    <div class="row" style="margin-left: 0px;">
        @include('partials.message')
    </div>
    <div class="row profile-title">
        <div class="container user-content">
            <div class="col-md-2 col-sm-4 col-xs-12 profile-pic-container text-center">
                @if(Storage::disk('profile')->has(auth()->user()->technician->avartar))
                    <img src="{{ route('avatar',['filename'=>auth()->user()->technician->avartar]) }}" alt="" class="img-circle profile-pic">
                @else
                    <img src="{{ asset('assets/img/default.png') }}" alt="" class="img-circle profile-pic">
                @endif
                    <br>
                    <button class="btn btn-sm btn-common new-profile-pic" data-toggle="modal" data-target="#uploadProfilePic">Change &nbsp;<i class="ti-plus"></i></button>
            </div>
            <div class="col-md-10 col-sm-8  col-xs-12">
                <h1 class="title">{{ auth()->user()->first_name }}
                <small class="title">{{ auth()->user()->last_name }}</small></h1>
                <div>{!! auth()->user()->confirmed ? '<span class="label label-success">Verified</span>' : '<span class="label label-warning">Unconfirmed</span>' !!}</div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="uploadProfilePic" tabindex="-1" role="dialog" aria-labelledby="uploadProfilePicLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Upload New Profile Picture</h4>
                </div>
                <div class="modal-body">
                    <form id="uploadProfilePicForm" action="{{ action('TechnicianController@uploadProfilePic') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="image" class="control-label">New Profile Picture:</label>
                            <input type="file" class="form-control" id="image" name="image">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" form="uploadProfilePicForm" class="btn btn-common" style="padding: 8px 20px;">Upload File</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


@endsection