@extends('admin.layouts.admin')

@section('title', 'Categories')

@section('styles')
    @parent()
    <style>
        .modal{
            z-index: 1050;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Categories </h2>

                        <button class="btn btn-sm btn-primary pull-right"  data-toggle="modal" data-target="#new_category">New Cartegory &nbsp;<i class="fa fa-plus"></i></button>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    {{--<th>--}}
                                        {{--<input type="checkbox" id="check-all" class="flat">--}}
                                    {{--</th>--}}
                                    <th class="column-title"># </th>
                                    <th class="column-title">Title </th>
                                    <th class="column-title">Services </th>
                                    <th class="column-title no-link last text-right"><span class="nobr">Action</span>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>
                                @php($count = 1)
                                @foreach($categories as $category)
                                <tr class="even pointer">
                                    {{--<td class="a-center ">--}}
                                        {{--<input type="checkbox" class="flat" name="table_records">--}}
                                    {{--</td>--}}
                                    <td class="a-center">{{ $count }}</td>
                                    <td class=" "> {{ $category->name }} </td>
                                    <td class=" ">{{ $category->services }}</td>
                                    <td class="a-right a-right text-right">
                                        <input type="hidden" value="{{ $category->name }}">
                                        <a class="btn btn-sm btn-success btn-new-service" data-toggle="modal" data-target="#new_service"><i class="fa fa-plus"></i></a>
                                        <input type="hidden" value="{{ $category->id }}">
                                        <a class="btn btn-sm btn-danger btn-delete-category"><i class="fa fa-trash"></i></a>
                                        <a href="{{ route('admin.category.view', ['slug'=>$category->slug]) }}" class="btn btn-sm btn-primary btn-view-category"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                                    @php($count++)
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.categories.new')
    @endsection

@section('scripts')
    @parent()
    <script type="application/javascript">
        var new_category_url = "{{ route('admin.categories.create') }}";
        var new_service_url = "{{ route('admin.service.create') }}";

        $('.btn-new-service').click(function () {
            var name = $(this).prev().val()
            var id = $(this).next().val()

            console.log(name)

            $('#category_id').val(id);
            $('#category').val(name);
        })

        $('.btn-delete-category').click(function ()  {
            var token = "{{ csrf_token()  }}";

            var id = $(this).prev().val()
            var url = "{{ route('admin.category.delete') }}";

            setTimeout(
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {id: id, _token: token},
                    statusCode: {
                        500: function(err){
                            swal('Failled!', err.message, 'error')
                        }
                    },
                    success: function(data){
                        swal('Success!', data.message, 'success')
                        window.location.reload()
                    }
                }),
                2000
            )

        })

        $('#newCategory').submit(function (e) {
            e.preventDefault();
            var formdata = new FormData(this);

            $.ajax({
                url: new_category_url,
                method: 'POST',
                data: formdata,
                cache: false,
                contentType: false,
                processData: false,
                statusCode: {
                    500: function(err){
                        console.log(err.errors)
                        swal('Failled!', 'Error Processing request. Try Again!', 'error')
                    },
                    200: function(data){
                        swal('Success!', data.message, 'success')
                        window.location.reload()
                    }
                }
            })
        })

        $('#newService').submit(function (e) {
            e.preventDefault();
            var formdata = new FormData(this);

            $.ajax({
                url: new_service_url,
                method: 'POST',
                data: formdata,
                cache: false,
                contentType: false,
                processData: false,
                statusCode: {
                    500: function(err){
                        console.log(err.errors)
                        swal('Failled!', 'Error Processing request. Try Again!', 'error')
                    },
                    200: function(data){
                        swal('Success!', data.message, 'success')
                        window.location.reload()
                    }
                }
            })
        })
    </script>
    @endsection()