
{{--New Category modal--}}
<div id="new_category" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Create Category</h4>
            </div>
            {{ Form::open(['route'=>'admin.categories.create', 'method'=> 'post', 'id'=>'newCategory']) }}
            {{ csrf_field() }}
            <div class="modal-body">

                <div class="form-group">
                    {{ Form::label('title', 'Title', ['class'=>'control-label']) }}
                    {{ Form::text('title', '', ['class'=>'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('desc', 'Description', ['class'=>'control-label']) }} <span class="info">&nbsp;(max: 250)</span>
                    <textarea name="desc" class="form-control" maxlength="250"></textarea>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" form="newCategory" class="btn btn-primary">Save</button>
            </div>
    {{ Form::close() }}
        </div>
    </div>
</div>


{{--New service modal--}}
<div class="modal fade" id="new_service" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Create Service</h4>
            </div>
            {{ Form::open(['route'=>'admin.service.create', 'method'=> 'post', 'id'=>'newService']) }}

            <div class="modal-body">
                <input type="hidden" name="category_id" id="category_id" value="">
                <div class="form-group">
                    
                    {{ Form::label('title', 'Category Title', ['class'=>'control-label']) }}
                    <input id="category"  class="form-control" type="text" value="" disabled>
                </div>.
                
                <div class="form-group">
                    {{ Form::label('service', 'Service', ['class'=>'control-label']) }}
                    {{ Form::text('service', null, ['class'=>'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('desc', 'Description', ['class'=>'control-label']) }} <span class="info">&nbsp;(max: 250)</span>
                    <textarea name="desc" class="form-control" maxlength="250"></textarea>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" form="newService" class="btn btn-primary">Save</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>


{{--view service modal--}}
<div class="modal fade" id="view_category" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="category_label"></h4>
            </div>
            <div class="modal-body">
                <h6>Description:</h6>
                <p id="desc"></p>
                <br>
                <h6>Services</h6>
                <table class="table table-striped" id="services_container">
                    <thead>
                    <tr>
                        <td>#</td>
                        <td>Name</td>
                        <td>Description</td>
                        <td>Action</td>
                    </tr>
                    </thead>
                    <tbody id="cat_services">

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>