<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 9/19/17
 * Time: 4:39 PM
 */
?>

@extends('admin.layouts.admin')

@section('title', 'Category')

@section('styles')
    @parent()

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <button class='btn btn-sm btn-primary btn-new-service pull-right' data-toggle='modal' data-target='#new_service'><i class='fa fa-plus'></i>&nbsp;New Service</button>
            <h3 class="title"></h3>
            <table class="table-striped" id="serviceTab" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th></th>
                    <th>Service Name</th>
                    <th>Technicians</th>
                    <th>Jobs</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th></th>
                    <th>Service Name</th>
                    <th>Technicians</th>
                    <th>Jobs</th>
                    <th>Actions</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

{{--New service modal--}}
<div class="modal fade" id="new_service" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Create Service</h4>
            </div>
            {{ Form::open(['route'=>'admin.service.create', 'method'=> 'post', 'id'=>'new_service_form']) }}
            <input type="hidden" name="category_id" value="{{ $category->id }}">

            <div class="modal-body">
                <div class="form-group">

                    {{ Form::label('title', 'Category Title', ['class'=>'control-label']) }}
                    <input id="category"  class="form-control" type="text" value="{{ $category->name }}" disabled>
                </div>.

                <div class="form-group">
                    {{ Form::label('service', 'Service', ['class'=>'control-label']) }}
                    {{ Form::text('service', null, ['class'=>'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('desc', 'Description', ['class'=>'control-label']) }} <span class="info">&nbsp;(max: 250)</span>
                    <textarea name="desc" class="form-control" maxlength="250"></textarea>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" form="new_service_form" class="btn btn-primary">Save</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

{{--Edit service modal--}}
<div class="modal fade" id="edit_service" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit Service</h4>
            </div>
            {{ Form::open(['route'=>'admin.service.edit', 'method'=> 'post', 'id'=>'edit_service_form']) }}

            <div class="modal-body">
                <input type="hidden" name="service_id" id="service_id" value="">
                <div class="form-group">
                    {{ Form::label('service_name', 'Service', ['class'=>'control-label']) }}
                    {{ Form::text('service_name', null, ['class'=>'form-control', 'id'=>'service_name', 'disabled'=>'true']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('service_desc', 'Description', ['class'=>'control-label']) }} <span class="info">&nbsp;(max: 250)</span>
                    <textarea name="service_desc" id="service_desc" class="form-control" maxlength="250"></textarea>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" form="edit_service_form" class="btn btn-primary">Save</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>


@endsection
@section('scripts')
@parent
<!-- Datatables -->
<script>
    var get_service_url = "{{ route('admin.service.get') }}"
    var edit_service_url = "{{ route('admin.service.edit') }}";

    function format ( d ) {
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
            '<tr>'+
            '<td>Description:</td>'+
            '<td>'+d.description+'</td>'+
            '</tr>'+
            '</table>';
    }

    $(document).ready(function() {
        var table = $('#serviceTab').DataTable( {
            "ajax": "{{ route('category.services', ['slug'=>$category->slug]) }}",
            "columns": [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "data":           null,
                    "targets": 1,
                    "defaultContent": ""
                },
                { "data": "name" },
                { "data": "technicians" },
                { "data": "jobs" },
                { "data": "action" }
            ],
            "order": [[1, 'asc']]
        } );

        // Add event listener for opening and closing details
        $('#serviceTab tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        });

        $('#serviceTab tbody').on( 'click', 'button.edit-service', function () {
            var id = $(this).next().val();
            var  modal = $('#edit_service');

            $.get(get_service_url, {id: id}, function(data, status){
                $('#service_id').val(data.service.id);
                $('#service_name').val(data.service.name);
                $('#service_desc').val(data.service.description);

                modal.modal()
            });

            $('#edit_service_form').submit(function (e) {
                e.preventDefault();
                var formdata = new FormData(this);

                setTimeout($.ajax({
                    url: edit_service_url,
                    method: 'POST',
                    data: formdata,
                    cache: false,
                    contentType: false,
                    processData: false,
                    statusCode: {
                        500: function (err) {
                            console.log(err)
                            swal('Failled!', 'Error Processing request. Try Again!', 'error')
                        },
                        200: function (data) {
                            swal('Success!', data.message, 'success')
                            window.location.reload()
                        }
                    }
                }), 3000)
            });
        });

        $('#serviceTab tbody').on( 'click', 'button.delete-service', function () {
            var id = $(this).prev('input').val();
            var del_url = "{{ route('admin.service.delete') }}";
            var token = "{{ csrf_token() }}";

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {
                $.ajax({
                    url: del_url,
                    method: 'POST',
                    data: {id: id, _token: token},
                    statusCode: {
                        500: function () {
                            swal('Failed!','Error Processing your request.','error')
                        }
                    },
                    success: function () {
                        swal('Deleted!', 'Your file has been deleted.','success')
                        window.location.reload()
                    }
                })
            })
        } );
    } );

    var new_service_url = "{{ route('admin.service.create') }}";

    $('#new_service_form').submit(function (e) {
        e.preventDefault();
        var formdata = new FormData(this);

        setTimeout($.ajax({
            url: new_service_url,
            method: 'POST',
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            statusCode: {
                500: function(err){
                    console.log(err)
                    swal('Failled!', 'Error Processing request. Try Again!', 'error')
                },
                200: function(data){
                    swal('Success!', data.message, 'success')
                    window.location.reload()
                }
            }
        }), 3000)
    });

</script>
    @endsection