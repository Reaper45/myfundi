@extends('admin.layouts.admin')

@section('title', 'PataFundi Dashboard')

@section('styles')
    @parent
    <style>
        .tile-stats{
            border-radius: 0px !important;
        }
    </style>
    @endsection

@section('content')
    <div class="row tile_count">
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i>&nbsp;Total Technicians</span>
            <div class="count">{{ $technicians }}</div>
            <span class="count_bottom"><i class="green">{{ $technicians_increase }}% </i> From last Week</span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-male"></i>&nbsp;Male Technicians</span>
            <div class="count">{{ $male_technicians }}</div>
            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>{{ $male_technicians_increase }}% </i> From last Week</span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-female"></i>&nbsp;Female Technicians</span>
            <div class="count green">{{ $female_technicians }}</div>
            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>{{ $female_technicians_increase }}
                    % </i> From last Week</span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i>&nbsp;Total Clients</span>
            <div class="count">{{ $clients }}</div>
            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>{{ $clients_increase }}% </i> From last Week</span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-industry"></i>&nbsp;Total Companies</span>
            <div class="count">{{ $companies }}</div>
            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>{{ $companies_increase }}% </i> From last Week</span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-briefcase"></i>&nbsp;Total Job Posts</span>
            <div class="count">{{ $jobs}}</div>
            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>{{ $jobs_increase }}% </i> From last Week</span>
        </div>
    </div>
    <br/>

    <div class="row">


        <div class="col-md-3 col-sm-4 col-xs-12">
            {{-- Categories Chart --}}
            <div class="x_panel" style="padding: 0px;">
                <div class="x_title" style="padding: 8px">
                    <h2>Categories</h2>
                    <div class="pull-right">
                        Total &nbsp;<span class="label label-primary">{{ $categories}}</span>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <canvas id="category_chart" height="200" width="230"></canvas>
                </div>
            </div>

            {{-- Jobs Chart --}}
            <div class="x_panel" style="padding: 0px;">
                <div class="x_title" style="padding: 8px" >
                    <h2>Jobs</h2>
                    <div class="pull-right">
                        Total &nbsp;<span class="label label-primary">{{ $jobs}}</span>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <canvas id="job_chart" height="200" width="230"></canvas>
                </div>
            </div>
        </div>

        <div class="col-md-9 col-sm-8 col-xs-12">
            <div class="x_content">
                <div class="row">
                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <h3>Job Posts</h3>
                            <div class="icon"><i class="fa fa-briefcase"></i>
                            </div>
                            <div class="count">{{ $jobs_today }}</div>
                            <p>Total Job Posts Today</p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <h3>Technicians</h3>
                            <div class="icon"><i class="fa fa-user"></i>
                            </div>
                            <div class="count">{{ $technicians_today }}</div>
                            <p>New Sign ups Today</p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <h3>Clients</h3>
                            <div class="icon"><i class="fa fa-group"></i>
                            </div>
                            <div class="count">{{ $clients_today }}</div>
                            <p>New Sign ups Today</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" x_panel">
                <div class="row x_title">
                    <h3>PataFundi
                        <small>User Growth</small>
                    </h3>
                </div>
                <div class="x_content">
                    {{--<div class="technicians-map"></div>--}}
                    <div class="chart-container" style="position: relative;">
                        <canvas id="userGrowth" height="130"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class=" x_panel">
                <div class="row x_title">
                    <h3>PataFundi
                        <small>Job Requests in each Category</small>
                    </h3>
                </div>
                <div class="x_content">
                    <div class="chart-container">
                        <canvas id="jobsPerCategory" height="120"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script type="application/javascript">
        var cat_url = "{{ route('admin.charts') }}";
        var weather_url = "http://api.worldweatheronline.com/premium/v1/weather.ashx?key=fe78a9c24a9144169d063430171809&q=48.85,2.35&num_of_days=2&tp=3&format=json";

        var fetchLabelsAndData = function(data) {
            var labels = []
            var values = []
            $.each(data, function (key, value) {
                labels.push(key)
                values.push(value)
            })
            return data = {'labels': labels, 'values': values}
        }

        $.ajax({
            url: cat_url,
            method: 'GET',
            response: 'json',
            statusCode: {
                500: function(){
                    new PNotify({
                        title: 'Failed',
                        text: 'Error occured loading charts!',
                        type: 'error',
                        styling: 'bootstrap3'
                    });

                },
                200: function(data){

                    var cat_ctx = $('#category_chart');
                    var job_ctx = $('#job_chart');
                    var user_ctx = $("#userGrowth");
                    var jobspercat_ctx = $("#jobsPerCategory")

                    // ChartOptions
                    var chartOptions = {
                        legend: false,
                        responsive: false,
                    };

                    var categories = fetchLabelsAndData(data.categories)
                    var cat_labels = categories.labels;
                    var cat_data = categories.values;

                    var jobs = fetchLabelsAndData(data.jobs)
                    var job_labels = jobs.labels;
                    var job_data = jobs.values;

                    var user_growth_labels = [];
                    var user_growth_data = [];

                    var jobs_per_cat = fetchLabelsAndData(data.jobs_per_cat)
                    var jobs_per_cat_labels = jobs_per_cat.labels;
                    var jobs_per_cat_data = jobs_per_cat.values;

                    // Get user growth data
                    $.each(data.user_growth, function (key, value) {
                        user_growth_data.push(value.count)
                        var mon = moment(value.month, 'M').format('MMMM')
                        user_growth_labels.push(mon.slice(0,3))
                    })

                    // Category chart
                    var catChart = new Chart(cat_ctx, {
                        type: 'pie',
                        tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                        data: {
                            labels: cat_labels,
                            datasets: [{
                                data: cat_data,
                                backgroundColor: [
                                    "#BDC3C7", "#E74C3C", "#26B99A", "#3498DB", "#BDC3C7", "#9B59B6", "#E74C3C", "#26B99A", "#3498DB", "#BDC3C7", "#9B59B6", "#E74C3C", "#26B99A", "#3498DB"
                                ],
                                hoverBackgroundColor: [
                                    "#CFD4D8", "#E95E4F", "#36CAAB", "#49A9EA", "#CFD4D8", "#B370CF", "#E95E4F", "#36CAAB", "#49A9EA", "#CFD4D8", "#B370CF", "#E95E4F", "#36CAAB", "#49A9EA"
                                ]
                            }]
                        },
                        options: chartOptions
                    });

                    // Jobs Chart
                    var jobChart = new Chart(job_ctx, {
                        type: 'pie',
                        tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                        data: {
                            labels: job_labels,
                            datasets: [{
                                data: job_data
                                ,
                                backgroundColor: [
                                    "#26B99A", "#3498DB", "#BDC3C7", "#9B59B6", "#E74C3C", "#26B99A", "#3498DB", "#9B59B6", "#BDC3C7", "#E74C3C", "#26B99A", "#3498DB", "#BDC3C7", "#9B59B6", "#E74C3C"
                                ],
                                hoverBackgroundColor: [
                                    "#36CAAB", "#49A9EA", "#CFD4D8", "#B370CF", "#E95E4F", "#36CAAB", "#49A9EA", "#B370CF", "#CFD4D8", "#E95E4F", "#36CAAB", "#49A9EA", "#CFD4D8", "#B370CF", "#E95E4F"
                                ]
                            }]
                        },
                        options: chartOptions
                    });

                    // user growth Chart
                    var userChart = new Chart(user_ctx, {
                        type: 'line',
                        data: {
                            labels: user_growth_labels,
                            datasets: [{
                                data: user_growth_data,
                                label: "Clients",
                                borderColor: "#3e95cd",
                            }]
                        },
                        options: {
                            legend: false,
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        stepSize: 5
                                    }
                                }]
                            }
                        }
                    });

                    // Jobs per Category Bar Chart
                    var jobsPerCat = new Chart(jobspercat_ctx, {
                        type: 'bar',
                        data: {
                            labels: jobs_per_cat_labels,
                            datasets: [{
                                label: 'Jobs',
                                data: jobs_per_cat_data
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        stepSize: 10
                                    }
                                }]
                            }
                        }
                    })
                }
            }
        })


        //

    </script>
    @endsection