<?php
?>
@include('partials.mini.theme')

<div style="padding: 30px 15px;">
    <div class="btn-group">
        <button class="btn btn-sm btn-primary">PDF</button>
        <button class="btn btn-sm btn-primary">Excel</button>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>User Id</th>
            <th>Full Name</th>
            <th>Phone Number</th>
            <th>Email</th>
            <th>Gender</th>
            <th>Id No</th>
            <th>Active</th>
            <th>Joined</th>
        </tr>
        </thead>
        <tbody>
        @php($count = 1)
        @foreach($users as $user)
            <tr>
                <th scope="row">{{ $count++ }}</th>
                <td>{{ $user->id }}</td>
                <td>{{ $user->first_name." ".$user->last_name }}</td>
                <td>{{ $user->phone_number }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->technician->gender }}</td>
                <td>{{ $user->technician->id_no }}</td>
                <td>{{ $user->active ?'active': 'inactive' }}</td>
                <td>{{ $user->created_at }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
