@extends('admin.layouts.admin')

@section('title', 'Technicians Profile', ['name' => $user->first_name.' '.$user->last_name ])
@section('styles')
    @parent()
    <style>
        .dropdown-menu li a{
            padding: 3px 8px;
        }
        .tile-stats{
            width: 24%;
            border-radius: 0;
            margin-right: 5px;
        }
    </style>
@endsection
@section('content')

    <div class="row">

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>User Report <small>Activity report</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li style="float: inherit;"><a href="#">Activate Account</a>
                                    </li>
                                    <li><a href="#">Deactivate Account</a>
                                    </li>
                                    <li><a href="#">Delete Account</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div style="margin-right: 10px; width: 23%;"  class="col-md-3 col-sm-3 col-xs-12 profile_left">

                            <div class="profile_img">

                                <!-- end of image cropping -->
                                <div id="crop-avatar">
                                    <!-- Current avatar -->
                                    @if (Storage::disk('profile')->has($user->technician->avartar))
                                        <img class="img-responsive avatar-view" src="{{ route('avatar', ['filename'=>$user->technician->avartar, '_token'=>csrf_token()]) }}" alt="Avatar" title="Change the avatar">
                                    @else
                                        <img class="img-responsive avatar-view" src="{{ url('assets/img/default.png') }}" alt="Avatar" title="Change the avatar">
                                    @endif

                                </div>
                                <!-- end of image cropping -->

                            </div>
                            <h3>{{ $user->first_name.' '.$user->last_name}}</h3>

                            <ul class="list-unstyled user_data">
                                <li><i class="fa fa-map-marker user-profile-icon"></i>
                                    {{ $user->technician->location_name.', '.$user->technician->country }}
                                </li>

                                <li>
                                    <i class="fa fa-briefcase user-profile-icon"></i>
                                    {{ \App\Service::find($user->user_type)->first()->name }}
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-12">

                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Profile</a>
                                    </li>
                                    <li role="presentation" class=""><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="false">Recent Activity</a>
                                    </li>
                                    <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="true">Projects Worked on</a>
                                    </li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content3" aria-labelledby="profile-tab">
                                        <div class="row">
                                            <div class="col-md-4 tile-stats">
                                                <span>Accepted Jobs</span>
                                                <h2>{{ $accepted_jobs }}</h2>
                                                <span>Total Accepted Jobs</span>
                                            </div>
                                            <div class="col-md-4 tile-stats">
                                                <span>Done</span>
                                                <h2>{{ $done_jobs }}</h2>
                                                <span>Total Jobs Done</span>
                                            </div>
                                            <div class="col-md-4 tile-stats">
                                                <span>Clients</span>
                                                <h2>{{ $user_clients }}</h2>
                                                <span>Total Clients Served</span>
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 15px;">
                                            <h4>Technician Docs</h4>
                                            <hr>
                                            <div class="col-md-4">
                                                <div class="text-center">
                                                    <a target="_blank" href="{{ ($user->technician->certificate == null) ?'#' : asset('storage/certificates/'.$user->technician->certificate)}}"
                                                       class="{{ ($user->technician->certificate == null) ? 'disabled': ''}}"
                                                    ><i class="fa fa-file-pdf-o fa-5x"></i></a>
                                                    <h4 class="title">Certificate
                                                        @if($user->technician->certificate_valid)
                                                            <i style="margin-left: 5px; color: #00A000" class="fa fa-certificate"></i>
                                                        @else
                                                            <i style="margin-left: 5px; color: #db0000"  class="fa fa-close"></i>
                                                        @endif
                                                    </h4>

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="text-center">
                                                    <a target="_blank" href="{{ ($user->technician->good_conduct == null) ?'#' : asset('storage/good_conduct/'.$user->technician->good_conduct)}}"
                                                       class="{{ ($user->technician->good_conduct == null) ? 'disabled': ''}}"><i class="fa fa-file-pdf-o fa-5x"></i></a>
                                                    <h4 class="title">Good Conduct
                                                        @if($user->technician->good_conduct_valid)
                                                            <i style="margin-left: 5px; color: #00A000" class="fa fa-check"></i>
                                                        @else
                                                            <i style="margin-left: 5px; color: #db0000"  class="fa fa-close"></i>
                                                        @endif
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="text-center">
                                                    <a target="_blank" href="{{ ($user->technician->id_card == null) ?'#' : asset('storage/idcards/'.$user->technician->id_card)}}"
                                                       class="{{ ($user->technician->id_card == null) ? 'disabled': ''}}"><i class="fa fa-file-pdf-o fa-5x"></i></a>
                                                    <h4 class="title">National Id Card
                                                        @if($user->technician->id_card_valid)
                                                            <i style="margin-left: 5px; color: #00A000" class="fa fa-check"></i>
                                                        @else
                                                            <i style="margin-left: 5px; color: #db0000"  class="fa fa-close"></i>
                                                        @endif
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="tab_content1" aria-labelledby="home-tab">

                                        <!-- start recent activity -->
                                        <ul class="messages">
                                            <li>
                                                @if (Storage::disk('profile')->has($user->technician->avatar))
                                                    <img class="avatar" src="{{ route('avatar', ['filename'=>$user->technician->avatar, '_token'=>csrf_token()]) }}" alt="Avatar" title="Change the avatar">
                                                @else
                                                    <img class="avatar" src="{{ url('assets/img/avatar.png') }}" alt="Avatar" title="Change the avatar">
                                                @endif
                                                {{-- <img src="images/img.jpg" class="avatar" alt="Avatar"> --}}
                                                <div class="message_date">
                                                    <h3 class="date text-info">24</h3>
                                                    <p class="month">May</p>
                                                </div>
                                                <div class="message_wrapper">
                                                    <h4 class="heading">Desmond Davison</h4>
                                                    <blockquote class="message">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth.</blockquote>
                                                    <br />
                                                    <p class="url">
                                                        <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                        <a href="#"><i class="fa fa-paperclip"></i> User Acceptance Test.doc </a>
                                                    </p>
                                                </div>
                                            </li>
                                        </ul>
                                        <!-- end recent activity -->

                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                                        <!-- start user projects -->
                                        <table class="data table table-striped no-margin">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Project Name</th>
                                                <th>Client Company</th>
                                                <th class="hidden-phone">Hours Spent</th>
                                                <th>Contribution</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>New Company Takeover Review</td>
                                                <td>Deveint Inc</td>
                                                <td class="hidden-phone">18</td>
                                                <td class="vertical-align-mid">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-success" data-transitiongoal="35"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>New Partner Contracts Consultanci</td>
                                                <td>Deveint Inc</td>
                                                <td class="hidden-phone">13</td>
                                                <td class="vertical-align-mid">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger" data-transitiongoal="15"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Partners and Inverstors report</td>
                                                <td>Deveint Inc</td>
                                                <td class="hidden-phone">30</td>
                                                <td class="vertical-align-mid">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-success" data-transitiongoal="45"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>New Company Takeover Review</td>
                                                <td>Deveint Inc</td>
                                                <td class="hidden-phone">28</td>
                                                <td class="vertical-align-mid">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-success" data-transitiongoal="75"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!-- end user projects -->

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- </div> --}}
    <!-- /page content -->
@endsection

@section('scripts')
    @parent
    <script>

    </script>
@endsection