@extends('admin.layouts.admin')

@section('title', 'Technicians Map')

@section('content')
<div style="width: 100%; height: 500px;" id="technicians_map"></div>
@endsection
@section('scripts')
    @parent
    <script type="text/javascript">
        var map;
        var map_id = "technicians_map";
        var coordsurl = "{{ route('admin.technicians.coords') }}";
    </script>
    {{ Html::script(asset('assets/custom/js/plot.js')) }}
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDBYOlNSA5ofbvMRe6I-MHxdio2TgdXQU&callback=initMap"> </script>
@endsection
