{{--


     Functionality Removed.
     Keeping it jst in case

--}}

{{--@extends('admin.layouts.admin')--}}

{{--@section('title', 'Register New Technician')--}}

{{--@section('content')--}}
  {{--<!-- Smart Wizard -->--}}
  {{--<div id="wizard" class="form_wizard wizard_horizontal">--}}
    {{--<ul class="wizard_steps">--}}
      {{--<li>--}}
        {{--<a href="#step-1">--}}
          {{--<span class="step_no">1</span>--}}
          {{--<span class="step_descr">--}}
            {{--Step 1<br />--}}
            {{--<small>{{ __('admin.register.technicians.step_1')}}</small>--}}
          {{--</span>--}}
        {{--</a>--}}
      {{--</li>--}}
      {{--<li>--}}
        {{--<a href="#step-2">--}}
          {{--<span class="step_no">2</span>--}}
          {{--<span class="step_descr">--}}
            {{--Step 2<br />--}}
            {{--<small>{{ __('admin.register.technicians.step_2')}}</small>--}}
          {{--</span>--}}
        {{--</a>--}}
      {{--</li>--}}
      {{--<li>--}}
        {{--<a href="#step-3">--}}
          {{--<span class="step_no">3</span>--}}
          {{--<span class="step_descr">--}}
            {{--Step 3<br />--}}
            {{--<small>{{ __('admin.register.technicians.step_3')}}</small>--}}
          {{--</span>--}}
        {{--</a>--}}
      {{--</li>--}}
      {{--<!-- <li>--}}
        {{--<a href="#step-4">--}}
          {{--<span class="step_no">4</span>--}}
          {{--<span class="step_descr">--}}
            {{--Step 4<br />--}}
            {{--<small>{{ __('admin.register.technicians.step_4')}}</small>--}}
          {{--</span>--}}
        {{--</a>--}}
      {{--</li> -->--}}
    {{--</ul>--}}
    {{--{{ Form::open(['route' => 'post.technicians', 'files' => true,'id'=>'registerTechnicianForm']) }}--}}
    {{--<div id="step-1">--}}
      {{--<div class="col-md-8 col-md-offset-2">--}}
        {{-- <h2 class="StepTitle">{{ __('admin.register.technicians.step_1')}}</h2> --}}
        {{--<div class="form-group row ">--}}
          {{--<div class="col-md-6 {{ $errors->has('first_name') ? ' has-error' : '' }}">--}}
            {{--{{ Form::label('first_name', 'First Name', ['class' => 'control-label', 'for'=>'first_name']) }}--}}
            {{--{{ Form::text('first_name', old('first_name'), ['class' => 'form-control', 'required' => 'true']) }}--}}

            {{--{{ Form::hidden('user_type', 'technician') }}--}}
          {{--</div>--}}
          {{--<div class="col-md-6 {{ $errors->has('last_name') ? ' has-error' : '' }}">--}}
            {{--{{ Form::label('last_name', 'Last Name', ['class' => 'control-label', 'for'=>'last_name']) }}--}}
            {{--{{ Form::text('last_name', old('last_name'), ['class' => 'form-control', 'required' => 'true']) }}--}}

          {{--</div>--}}
        {{--</div>--}}

        {{--<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">--}}
          {{--{{ Form::label('email', 'email', ['class' => 'control-label', 'for'=>'email']) }}--}}
          {{--{{ Form::text('email', old('email'), ['class' => 'form-control', 'required' => 'true']) }}--}}

        {{--</div>--}}
        {{--<div class="form-group {{ $errors->has('phone_number') ? ' has-error' : '' }}">--}}
          {{--{{ Form::label('phone_number', 'Phone Name', ['class' => 'control-label', 'for'=>'phone_number']) }}--}}
          {{--{{ Form::text('phone_number', old('phone_number'), ['class' => 'form-control', 'required' => 'true']) }}--}}

        {{--</div>--}}

        {{--<div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }}">--}}
          {{--{{ Form::label('gender', 'Gender', ['class' => 'control-label', 'style'=>'width: 100%']) }}--}}
          {{--<div id="gender" class="btn-group" data-toggle="buttons">--}}
            {{--<label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">--}}
              {{--{{ Form::radio('gender', 'male', ['id'=>'male']) }}&nbsp; Male--}}
            {{--</label>--}}
            {{--<label class="btn btn-primary"--}}
                   {{--data-toggle-class="btn-primary"--}}
                   {{--data-toggle-passive-class="btn-default"--}}
                   {{--data-date-format="yyyy/mm/dd">--}}
              {{--{{ Form::radio('gender', 'female', ['id'=>'male']) }}&nbsp;Female--}}
            {{--</label>--}}
          {{--</div>--}}
        {{--</div>--}}
        {{--<div class="form-group {{ $errors->has('dob') ? ' has-error' : '' }}">--}}

          {{--{{ Form::label('dob', 'Date Of Birth', ['class' => 'control-label']) }}--}}
          {{--<div class="input-group date">--}}
              {{--<input name="dob" type="text" class='form-control datepicker-here'data-position='top right' value="{{\Carbon\Carbon::now()}}" data-language='en' >--}}
              {{--<div class="input-group-addon">--}}
                  {{--<i class="fa fa-calendar-o"></i>--}}
              {{--</div>--}}
          {{--</div>--}}
        {{--</div>--}}


        {{-- {{ Form::close() }} --}}
      {{--</div>--}}
    {{--</div>--}}
    {{--<div id="step-2" style="display: none;">--}}
      {{--<div class="col-md-8 col-md-offset-2">--}}
        {{--<h2 class="StepTitle">{{ __('admin.register.technicians.step_2')}}</h2>--}}

        {{--<div class="form-group">--}}
          {{--{{ Form::label('skills', 'Select Field Of Specialty', ['class' => 'control-label', 'style'=>'width: 100%']) }}--}}
          {{--<div>--}}
            {{--<select id="skills" name="skills" class="select2 form-control" multiple="multiple" style="width: 100%" autocomplete="off">--}}
              {{--@if($services_group)--}}
                  {{--@foreach($services_group as $category_id => $services)--}}
                  {{--<optgroup label="{{ App\Category::find($category_id)->name }}">--}}
                      {{--@foreach( $services as $service)--}}
                      {{--<option value="{{ $service->id }}"> {{ $service->name }}</option>--}}
                      {{--@endforeach--}}
                  {{--</optgroup>--}}
                  {{--@endforeach--}}
              {{--@endif--}}
            {{--</select>--}}
          {{--</div>--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
          {{--{{ Form::label('location', 'Location', ['class' => 'control-label', 'style'=>'width: 100%']) }}--}}
          {{--<div class="text-center">--}}
            {{--<input id="lon" type="hidden" name="lon" value="">--}}
            {{--<input id="lat" type="hidden" name="lat" value="">--}}
            {{--<div style="width: 100%; height: 500px;" id="picklocation"></div>--}}
            {{----}}
            {{--<div id="message">--}}
            {{--<div id='picklocationform'>--}}
              {{--<input class="form-control pull-left" type='text' name='location_name'  id='name' placeholder='Tag Name' />--}}
              {{--<input class='btn btn-success btn-xs' type='button' value='Save' onclick='close()'/>--}}
            {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
        {{--</div>--}}
        {{--<br/>--}}
        {{--<br/>--}}
      {{--</div>--}}
    {{--</div>--}}
    {{--<div id="step-3" style="display: none;">--}}
      {{--<div class="col-md-8 col-md-offset-2">--}}
        {{--<h2 class="StepTitle">{{ __('admin.register.technicians.step_3')}}</h2>--}}
        {{--<div class="form-group">--}}
            {{--{{ Form::label('id_no', 'ID Card No.', ['class' => 'control-label', 'style'=>'width: 100%']) }}--}}
            {{--{{ Form::number('id_no', old('id_no'), ['class' => 'form-control']) }}--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
          {{--{{ Form::label('avartar', 'Upload Image', ['class' => 'control-label', 'style'=>'width: 100%']) }}--}}
          {{--{{ Form::file('avartar', ['class' => 'form-control']) }}--}}
        {{--</div>--}}
        {{--<div class="form-group row">--}}
          {{--<div class="col-md-8">--}}
            {{--{{ Form::label('certificate', ' Technician Certification', ['class' => 'control-label', 'style'=>'width: 100%']) }}--}}
            {{--{{ Form::file('certificate', ['class' => 'form-control']) }}--}}
          {{--</div>--}}
          {{--<div class="col-md-4 text-right">--}}
            {{--{{ Form::label('certificate_valid', 'Mark as Valid', ['class' => 'control-label', 'style'=>'width: 100%']) }}--}}
            {{--<div id="gender" class="btn-group" data-toggle="buttons">--}}
              {{--<label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">--}}
                {{--{{ Form::radio('certificate_valid', 'true') }}&nbsp; Valid--}}
              {{--</label>--}}
              {{--<label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">--}}
                {{--{{ Form::radio('certificate_valid', 'false') }}&nbsp; Pending--}}
              {{--</label>--}}
            {{--</div>--}}
          {{--</div>--}}
        {{--</div>--}}
        {{--<div class="form-group row">--}}
          {{--<div class="col-md-8">--}}
            {{--{{ Form::label('good_conduct', 'Certificate of Good Conduct', ['class' => 'control-label', 'style'=>'width: 100%']) }}--}}
            {{--{{ Form::file('good_conduct', ['class' => 'form-control']) }}--}}
          {{--</div>--}}
          {{--<div class="col-md-4 text-right">--}}
            {{--{{ Form::label('gc_cert', 'Mark as Valid', ['class' => 'control-label', 'style'=>'width: 100%']) }}--}}
            {{--<div id="gc_cert" class="btn-group" data-toggle="buttons">--}}
              {{--<label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">--}}
                {{--{{ Form::radio('good_conduct_valid', 'true') }}&nbsp; Valid--}}
              {{--</label>--}}
              {{--<label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">--}}
                {{--{{ Form::radio('good_conduct_valid', 'false') }}&nbsp; Pending--}}
              {{--</label>--}}
            {{--</div>--}}
          {{--</div>--}}
        {{--</div>--}}
      {{--</div>--}}
    {{--</div>--}}
   {{----}}
    {{--</div>--}}
    {{--{{ Form::close() }}--}}
  {{--</div>--}}
  {{--<!-- End SmartWizard Content -->--}}

{{--@endsection--}}
{{--@section('styles')--}}
    {{--@parent--}}
    {{--{{ Html::style(asset('assets/admin/css/users/edit.css')) }}--}}
    {{--<style>--}}
      {{--#message{--}}
        {{--display: none;--}}
      {{--}--}}
      {{--#picklocation{--}}
        {{--height: 400px; width: 100%;--}}
      {{--}--}}
    {{--</style>--}}
{{--@endsection--}}

{{--@section('scripts')--}}
    {{--@parent--}}
    {{--<script type="text/javascript">--}}
      {{--var MARKER_COLORS = [ 'blue', 'red', 'green', 'orange', 'pink', 'purple', 'yellow' ];--}}

      {{--var log = document.getElementById('lon');--}}
      {{--var lat = document.getElementById('lat');--}}
      {{--var map;--}}
      {{--var marker;--}}
      {{--var infowindow;--}}
      {{--var messagewindow;--}}

      {{--initMap = function() {--}}
        {{--var mapOptions = {--}}
          {{--center: new google.maps.LatLng(-4.0435, 39.6682),--}}
          {{--zoom: 15,--}}
          {{--zoomControl: true,--}}
          {{--zoomControlOptions: {--}}
              {{--style: google.maps.ZoomControlStyle.SMALL--}}
          {{--},--}}
          {{--mapTypeControl: false,--}}
          {{--streetViewControl: false,--}}
          {{--mapTypeId: google.maps.MapTypeId.ROADMAP,--}}
          {{--styles: [{--}}
              {{--stylers: [--}}
                  {{--{ invert_lightness: true },--}}
                  {{--{ hue: "#0091ff" }--}}
              {{--]--}}
          {{--}]--}}
        {{--};--}}

        {{--infowindow = new google.maps.InfoWindow({--}}
          {{--content: document.getElementById('picklocationform')--}}
        {{--});--}}

        {{--map = new google.maps.Map(document.getElementById('picklocation'), mapOptions);--}}
        {{----}}
        {{--messagewindow = new google.maps.InfoWindow({--}}
          {{--content: document.getElementById('message')--}}
        {{--});--}}

        {{--google.maps.event.addListener(map, 'click', function(event) {--}}
          {{--marker = new google.maps.Marker({--}}
            {{--position: event.latLng,--}}
            {{--map: map--}}
          {{--});--}}

          {{--google.maps.event.addListener(marker, 'click', function(){--}}
            {{--infowindow.open(map, marker);--}}
            {{--var latlng = marker.getPosition();--}}

            {{--lat.value = latlng.lat();--}}
            {{--lon.value = latlng.lng();--}}
          {{--});--}}
        {{--});--}}
        {{----}}
      {{--};--}}

      {{--close = function(){--}}
        {{--$('#picklocationform').style.display = 'none';--}}
      {{--}--}}
    {{--</script>--}}
    {{--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBegRx25srxedzqkSyfs-glxltcyMRRNAQ&callback=initMap"> </script>--}}
    {{--{{ Html::script(asset('assets/admin/js/users/edit.js')) }}--}}
{{--@endsection--}}
