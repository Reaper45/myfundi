@extends('admin.layouts.admin')

@section('title', 'Technicians')

@section('content')
    <div class="row">
      <div class="col-md-3 col-sm-3 col-xs-12 form-group pull-right top_search">
        <div class="input-group">
          <input id="searchInput" type="text" onkeyup="searchTable()" class="form-control" placeholder="Search for...">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
          </span>
        </div>
      </div>
        <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
               width="100%">
            <thead>
            <tr>
                <th>@sortablelink('email', __('views.admin.users.index.table_header_0'),['page' => $users->currentPage()])</th>
                <th>@sortablelink('name',  __('views.admin.users.index.table_header_1'),['page' => $users->currentPage()])</th>
                <th>{{ __('views.admin.users.index.table_header_2') }}</th>
                <th>@sortablelink('active', __('views.admin.users.index.table_header_3'),['page' => $users->currentPage()])</th>
                <th>@sortablelink('confirmed', __('views.admin.users.index.table_header_4'),['page' => $users->currentPage()])</th>
                <th>@sortablelink('created_at', __('views.admin.users.index.table_header_5'),['page' => $users->currentPage()])</th>
                <th>@sortablelink('updated_at', __('views.admin.users.index.table_header_6'),['page' => $users->currentPage()])</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody id="table">
            @foreach($users as $user)
              @if ($user->hasRole('technician') && !$user->hasRole('administrator'))
                <tr>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->first_name }}</td>
                    <td>{{ $user->roles->pluck('name')->implode(',') }}</td>
                    <td>
                        @if($user->active)
                            <span class="label label-primary">{{ __('views.admin.users.index.active') }}</span>
                        @else
                            <span class="label label-danger">{{ __('views.admin.users.index.inactive') }}</span>
                        @endif
                    </td>
                    <td>
                        @if($user->confirmed)
                            <span class="label label-success">{{ __('views.admin.users.index.confirmed') }}</span>
                        @else
                            <span class="label label-warning">{{ __('views.admin.users.index.not_confirmed') }}</span>
                        @endif</td>
                    <td>{{ $user->created_at }}</td>
                    <td>{{ $user->updated_at }}</td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="{{ route('admin.technician.show', [$user->id]) }}" data-toggle="tooltip" data-placement="top" data-title="{{ __('views.admin.users.index.show') }}">
                            <i class="fa fa-eye"></i>
                        </a>

                        @if($user->active)
                            <a id="deactivate" class="btn btn-xs btn-warning" href="{{ route('admin.technician.deactivate', ['id'=>$user->id]) }}" data-toggle="tooltip" data-placement="top" data-title="Deactivate Account">
                                <i class="fa fa-times-rectangle"></i>
                            </a>
                        @else
                            <a id="activate" class="btn btn-xs btn-success" href="{{ route('admin.technician.activate', ['id'=>$user->id]) }}" data-toggle="tooltip" data-placement="top" data-title="Activate Account">
                                <i class="fa fa-check-square"></i>
                            </a>
                        @endif

                        @if(auth()->user()->hasRole('administrator'))
                            <button class="btn btn-xs btn-danger user_destroy"
                                    data-url="{{ route('admin.users.destroy', [$user->id]) }}" data-toggle="tooltip" data-placement="top" data-title="{{ __('views.admin.users.index.delete') }}">
                                <i class="fa fa-trash"></i>
                            </button>
                        @endif
                    </td>
                </tr>
              @endif
            @endforeach
            </tbody>
        </table>
        <div class="pull-right">
            {{ $users->links() }}
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        $('.user_destroy').click(function (e) {
            e.preventDefault()
            var delete_url = $(this).attr('data-url')
            console.log(delete_url)

            setTimeout(
                $.ajax({
                    url: delete_url,
                    method: 'DELETE',
                    data: {_token: "{{ csrf_token() }}"},
                    statusCode: {
                        200: function (data) {
                            swal('Success', 'User Account Deleted Successfully!', 'success')
                            window.location.reload()
                        },
                        500: function (err) {
                            swal('Failed', 'Error Processing Request!', 'error')
                            console.log(err)
                        }
                    }
                }), 2000
            )
        });

        $('#activate').click(function (e) {
            e.preventDefault()
            var activate_url = $(this).attr('href')

            setTimeout(
                $.ajax({
                    url: activate_url,
                    method: 'GET',
                    statusCode: {
                        200: function (data) {
                            swal('Success', 'Account Activated successfully!', 'success')
                            window.location.reload()
                        },
                        500: function (err) {
                            swal('Failed', 'Error Processing Request!', 'error')
                            console.log(err)
                        }
                    }
                }), 2000
            )
        })

        $('#deactivate').click(function (e) {
            e.preventDefault()
            var deactivate_url = $(this).attr('href')

            setTimeout(
                $.ajax({
                    url: deactivate_url,
                    method: 'GET',
                    statusCode: {
                        200: function (data) {
                            swal('Success', 'Account Deactivated successfully!', 'success')
                            window.location.reload();
                        },
                        500: function (err) {
                            swal('Failed', 'Error Processing Request!', 'error')
                            console.log(err)
                        }
                    }
                }), 2000
            )
        })
    </script>
    @endsection