<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 9/19/17
 * Time: 10:38 AM
 */
?>

@extends('admin.layouts.admin')

@section('title', 'Generate Reports')

@section('styles')
    @parent
    <style>
        .select2-container--default .select2-selection--single{
            border-radius: 0px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow{
            top: 5px !important;
        }
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
            color: #fff;
            background-color: #172d44;
            border-radius: 0px !important;
        }
    </style>
    @endsection

@section('content')
    <div class="container">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-bar-chart"></i>&nbsp; Reports<small></small></h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <div class="col-xs-3">
                        <!-- required for floating -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-left">
                            <li class="active"><a href="#jobs" data-toggle="tab">Jobs</a>
                            </li>
                            <li><a href="#technicians" data-toggle="tab">Technicians</a>
                            </li>
                            <li><a href="#companies" data-toggle="tab">Companies</a>
                            </li>
                            <li><a href="#clients" data-toggle="tab">Clients</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-xs-9">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="jobs">
                                <p class="lead">Jobs Report</p>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="">Filter by status</label>
                                        <select name="" id="filterJobsByStatus" class="form-control js-basic-single">
                                            <option value="all">All</option>
                                            <option value="pending">Pending</option>
                                            <option value="accepted">Accepted</option>
                                            <option value="cancelled">Canceled</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Filter by category</label>
                                        <select name="job_category" id="filterJobsByCat" class="form-control js-basic-single">
                                            <option value="all" selected>All</option>
                                            @foreach($categories as $key => $category)
                                                <optgroup label="{{ $key }}">
                                                @foreach($category as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="control-label" for="from">From: </label>
                                        <input class="form-control from" type="text" id="from" name="job_from" data-language='en' >
                                        <label for="to">To: </label>
                                        <input class="form-control to" type="text" id="to" name="job_to" data-language='en' >
                                    </div>
                                </div>

                                <div class="text-right" style="padding-top: 20px;">
                                    <button id="exportjobreport" class="btn btn-success" type="button">Export</button>
                                </div>
                            </div>

                            <div class="tab-pane" id="technicians">
                                <p class="lead">Technicians Report</p>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="">Filter by category</label>
                                        <select name="" id="filterTechByCat" class="form-control">
                                            <option value="all">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Filter By Technician</label>
                                        <select name="" id="filterTechByIndividuals" class="form-control">
                                            <option value="all">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="control-label" for="from">From: </label>
                                        <input class="form-control from" type="text" id="from" name="tech_from" data-language='en' >
                                        <label for="to">To: </label>
                                        <input class="form-control to" type="text" id="to" name="tech_to" data-language='en' >
                                    </div>
                                </div>
                                <div class="text-right" style="padding-top: 20px;">
                                    <button id="exportechreport" class="btn btn-success" type="button">Export</button>
                                </div>
                            </div>

                            <div class="tab-pane" id="companies">
                                <p class="lead">Companies Report</p>
                            </div>
                            <div class="tab-pane" id="clients">
                                <p class="lead">Clients Report</p>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>
    @endsection

@section('scripts')
    @parent
    <script>
        $( function() {
            var dateFormat = "yy-mm-dd",
                from = $( ".from" ).datepicker({
                        defaultDate: "+1w",
                        changeMonth: true,
                        numberOfMonths: 3
                    }).on( "change", function() {
                        to.datepicker( "option", "minDate", getDate( this ) );
                    }),
                to = $( ".to" ).datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 3
                }).on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

            function getDate( element ) {
                var date;
                try {
                    date = $.datepicker.parseDate( dateFormat, element.value );
                    console.log(date)
                } catch( error ) {
                    date = null;
                }

                return date;
            }
            } );
        $(document).ready(function() {
            $('.js-basic-single').select2();
        });

        $('#exportjobreport').click(function () {
            var status = $('#filterJobsByStatus').val()
            var category_id = $('#filterJobsByCat').val()
            var from = $( "input[name='job_from']" ).val()
            var to = $( "input[name='job_to']" ).val()

            var report_url = "{{ route('admin.jobs.print') }}"

            window.open(
                report_url+"?status="+status+"&category_id="+category_id+"&from="+from+"&to="+to,
                '_blank'
            );

        })

        $('#exportechreport').click(function () {
            var category = $('#filterTechByCat').val()
            var technician_id = $('#filterTechByIndividuals').val()
            var from = $( "input[name='tech_from']" ).val()
            var to = $( "input[name='tech_to']" ).val()

            var report_url = "{{ route('admin.technicians.print') }}"

            console.log('hit');
            window.open(
                report_url+"?category="+category+"&technician_id="+technician_id+"&from="+from+"&to="+to,
                '_blank'
            );

        })
    </script>
    @endsection