@extends('admin.layouts.admin')

@section('title', 'All Comapnies')

@section('content')
<div class="row">
  @foreach($companies as $company)
  <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
      <div class="well profile_view">
        <div class="col-sm-12">
          <h4 class="brief"><i>{{$company->name}}</i> <span class="fa fa-check-circle info"></span></h4>
          <div class="left col-xs-7">
            <h2></h2>
            <p><strong>Categories: </strong></p>
            @foreach($company->categories as $category)
              <span class="label label-default">{{$category->name}}</span>
            @endforeach
             
            <ul class="list-unstyled">
              <li><i class="fa fa-map-marker"></i>&nbsp;&nbsp;{{ $company->location_name }} </li><br>
              <li><i class="fa fa-phone"></i>&nbsp;&nbsp;{{$company->user->phone_number}} </li><br>
            </ul>
          </div>
          <div class="right col-xs-5 text-center">
            @if($company->logo && Storage::disk('company')->has($company->logo))
              <img class="img-responsive avatar-view" src="{{ route('avatar', ['filename'=>$company->logo, '_token'=>csrf_token()]) }}" alt="Avatar" title="Change the avatar">
            @else
             <img class="img-responsive avatar-view" src="{{ url('assets/img/avatar.png') }}" alt="Avatar" title="Change the avatar">
            @endif
          </div>
        </div>
        <div class="col-xs-12 bottom text-center">
          <div class="col-xs-12 col-sm-6 emphasis">
            <p class="ratings">
              <a>4.0</a>
              <a href="#"><span class="fa fa-star"></span></a>
              <a href="#"><span class="fa fa-star"></span></a>
              <a href="#"><span class="fa fa-star"></span></a>
              <a href="#"><span class="fa fa-star"></span></a>
              <a href="#"><span class="fa fa-star-o"></span></a>
            </p>
          </div>
          <div class="col-xs-12 col-sm-6 emphasis">
            <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">
              </i> <i class="fa fa-comments-o"></i> </button>
            <button type="button" class="btn btn-primary btn-xs">
              <i class="fa fa-user"> </i> View Profile
            </button>
          </div>
        </div>
      </div>
    </div>
    @endforeach
</div>

@endsection