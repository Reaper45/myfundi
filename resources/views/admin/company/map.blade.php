@extends('admin.layouts.admin')

@section('title', 'Companies Map')

@section('content')
<div style="width: 100%; height: 500px;" id="companies_map"></div>
@endsection
@section('scripts')
    @parent
    <script type="text/javascript">
        var map;
        var map_id = "companies_map";
        var coordsurl = "{{ route('admin.companies.coords') }}";		
    </script>
    {{ Html::script(asset('assets/custom/js/plot.js')) }}
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDBYOlNSA5ofbvMRe6I-MHxdio2TgdXQU&callback=initMap"> </script>
@endsection
