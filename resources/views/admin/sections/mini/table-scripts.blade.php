{{ Html::script(asset('assets/datatables/jszip/dist/jszip.min.js')) }}
{{ Html::script(asset('assets/datatables/pdfmake/build/pdfmake.min.js')) }}
{{ Html::script(asset('assets/datatables/pdfmake/build/vfs_fonts.js')) }}

<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>