<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>
      {{-- <ul class="nav navbar-nav">
        <li class="nav-item">
          <a class="fa-2x">@yield('title')</a>
        </li>
      </ul> --}}
      <ul class="nav navbar-nav navbar-right">

        <li class="nav-item">
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
          aria-expanded="false">
          <img src="{{ auth()->user()->avatar }}" alt="">{{ auth()->user()->first_name }}
          <span class=" fa fa-angle-down"></span>
        </a>
        <ul class="dropdown-menu dropdown-usermenu pull-right">
          <li>
            <a href="{{ route('logout') }}">
              <i class="fa fa-sign-out pull-right"></i> {{ __('views.backend.section.header.menu_0') }}
            </a>
          </li>
        </ul>
      </li>
      <li class="nav-item">
        <a href="#"><i class="fa fa-envelope-o"></i></a>
      </li>
    </ul>
  </nav>
</div>
</div>
