<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 9/20/17
 * Time: 3:30 PM
 */
?>
@include('partials.mini.theme')
<div style="padding: 30px 15px;">
    <div class="btn-group">
        <button class="btn btn-sm btn-primary" onclick="">PDF</button>
        <button class="btn btn-sm btn-primary">Excel</button>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>User Id</th>
            <th>Full Name</th>
            <th>Date Created</th>
            <th>Job Status</th>
            <th>Accepted by</th>
            <th>Accepted at</th>
            <th>Cancelled by</th>
            <th>Cancelled at</th>
        </tr>
        </thead>
        <tbody>
        @php($count = 1)
            @foreach($jobs as $job)
            <tr>
                <th scope="row">{{ $count++ }}</th>
                <td>{{ $job->user_id }}</td>
                <td>{{ $job->fullname }}</td>
                <td>{{ $job->created_at }}</td>
                <td>{{ $job->status }}</td>
                <td>{{ $job->accepted_by?: '-' }}</td>
                <td>{{ $job->accepted_at?: '-' }}</td>
                <td>{{ $job->cancelled_by?: '-' }}</td>
                <td>{{ $job->cancelled_at?: '-' }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
