@extends('admin.layouts.admin')

@section('title', 'Jobs')

@section('content')

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Al Jobs</h2>

      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>Client</th>
            <th>Category</th>
            <th>Location</th>
            <th>Assigned</th>
            <th>Start Date</th>
            <th>Completed</th>
          </tr>
        </thead>


        <tbody>
        @foreach($jobs as $job)
          <tr>
            <td>{{ $job->client }}</td>
            <td>{{ $job->service }}</td>
            <td>{{ $job->location_name ?: '-' }}</td>
            <td>{!! $job->assigned_to ?: '<span class="label label-warning">unassigned</span>' !!}</td>
            <td>{{ $job->start_date ?: '-'  }}</td>
            <td>{!! $job->completed ? '<span class="label label-success">complete</span>' : '<span class="label label-warning">incomplete</span>' !!}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection
@section('styles')
    @parent
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">    
@endsection
@section('scripts')
    @parent
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    @include('admin.sections.mini.table-scripts')
@endsection
