@extends('layouts.app')

@section('body_class','nav-md')

@section('page')
    <div class="container body">
        <div class="main_container">
            @section('header')
                @include('admin.sections.navigation')
                @include('admin.sections.header')
            @show

            @yield('left-sidebar')

            <div class="right_col" role="main" style="min-height: 100vh;">
                <div class="page-title">
                    <div class="title_left">
                        <h1 class="h3">@yield('title')</h1>
                    </div>
                    @if(Breadcrumbs::exists())
                        <div class="title_right">
                            <div class="pull-right">
                                {!! Breadcrumbs::render() !!}
                            </div>
                        </div>
                    @endif
                </div>
                @include('partials.message')
                @yield('content')
            </div>

            <footer>
                @include('admin.sections.footer')
            </footer>
        </div>
    </div>
@stop

@section('styles')
    {{ Html::style(asset('assets/app/css/app.css')) }}
    {{ Html::style(asset('assets/admin/datepicker/css/datepicker.min.css')) }}
    {{ Html::style(asset('assets/admin/custom/css/master.css')) }}
    {{ Html::style(asset('assets/admin/build/css/custom.css')) }}
    {{ Html::style(asset('assets/admin/css/admin.css')) }}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.10/sweetalert2.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/b-1.4.2/datatables.min.css"/>

    <style>

        td.details-control {
            background: url("{{ asset('assets/img/details_close.png') }}") no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control {
            background: url("{{ asset('assets/img/details_open.png') }}") no-repeat center center;
        }
        thead {
            background: rgba(52, 73, 94, 0.94);
            color: #ECF0F1;
        }
        .nav-md .container.body .col-md-3.left_col{
            z-index: 100 !important;
        }
        .dataTables_filter label input{
            box-shadow: inset 0 1px 0px rgba(0, 0, 0, 0.075);
            border-radius: 25px 25px 25px 25px;
            padding-left: 20px;
            border: 1px solid rgba(221, 226, 232, 0.49);
            height: 30px;
            font-weight: 100;
        }

    </style>
@endsection

@section('scripts')
    {{ Html::script(asset('assets/app/js/app.js')) }}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    @include('admin.sections.mini.table-scripts')
    <script>
        $(document).ready(function() {
            $('#wizard').smartWizard();

            $('#wizard_verticle').smartWizard({
                transitionEffect: 'slide'
            });

            $('.buttonNext').addClass('btn btn-success');
            $('.buttonPrevious').addClass('btn btn-primary');
            $('.buttonFinish').addClass('btn btn-default');

        });

        function searchTable() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("searchInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("table");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    // console.log(td[j].innerHTML.toUpperCase());
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";

                    }
                }
            }
        }

    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.js"></script>
    {{ Html::script(asset('assets/custom/js/googlemaps.js')) }}
    {{ Html::script(asset('assets/admin/datepicker/js/datepicker.min.js')) }}
    {{ Html::script(asset('assets/admin/datepicker/js/datepicker.en.js')) }}
    {{ Html::script(asset('assets/admin/build/js/jquery.smartWizard.js')) }}
    {{ Html::script(asset('assets/admin/js/admin.js')) }}
    {{ Html::script(asset('assets/admin/custom/js/master.js')) }}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.10/sweetalert2.js"></script>

@endsection
