<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 9/22/17
 * Time: 2:06 PM
 */
?>


@extends('layouts.patafundi')

@section('title')
    Sign Up - Successful
@endsection

@section('styles')
    @parent
    <style>
        .logo-menu{
            display: none !important;
        }
        footer{
            display: none !important;
        }
        .job-list{
            width: 100%;
        }
        .fa-exclamation-circle{
            color: #FF9D02;
        }
    </style>
@endsection

@section('page-header')
    <!-- Page Header Start -->
    <div class="page-header" style="background: url(assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Become a Fundi</h2>
                        <ol class="breadcrumb">
                            <li><a href="/"><i class="ti-home"></i>&nbsp;Home</a></li>
                            <li class="current">Sign Up - Successful </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3" style="padding-top: 15px;">
                <div class="card job-list">
                    <h2 class="title headline">Congrats! You're all done. <i class="fa fa-exclamation-circle pull-right" aria-hidden="true"></i></h2>
                    <div class="push-bottom">
                        <p>Thank you for registering with PataFundi. You are almost ready. Update your profile and pick your first job. </p>
                        <p>Please check your email to activate confirm email address and learn more about next steps!</p>
                    </div>

                    <a href="{{ action('TechnicianController@sendLink',['uuid'=>$uuid]) }}" class="btn btn-common push-bottom" style="border-radius: 0px; width: 100%;">Get Text For Patafundi Fundi's App</a>
                    <div class="text-center">
                        <u><a href="{{ route('fundi.dashboard') }}" style="border-radius: 0px; width: 100%;">GO TO YOUR DASHBOARD </a></u>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('scripts')

@endsection

