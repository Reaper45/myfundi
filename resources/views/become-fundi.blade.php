<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 9/22/17
 * Time: 5:04 AM
 */
?>

@extends('layouts.patafundi')

@section('title')
    Sign up - Become a Fundi
@endsection

@section('styles')
    <style>
        .form-control{
            padding: 10px 10px !important;
            height: 45px !important;
        }
        .lead{
            font-family: 'Raleway', sans-serif !important;
        }
        .selected{
            color: #FF9D02;
            background: #fff;
            border: solid 1px #FF9D02;
        }
        .btn-common{
            border: solid 1px transparent;
        }
    </style>
    @endsection

@section('page-header')
    <!-- Page Header Start -->
    <div class="page-header" style="height: 100vh; min-height:650px; background: url(assets/img/banner1.jpg);">
        <div class="container">
            <div class="row" style="margin-left: 0px; margin-right: 0px;">
                <div class="col-md-7 hidden-xs hidden-sm" style="padding: 30px;">
                    <h1 class="lead push-bottom" style="color: #fff; font-size: 5.5rem !important; line-height:1.14;" >PataFundi is looking for you.</h1>
                    <p class="lead">Become a patafundi technician and earn great money as an independent contractor. Get jobs for your company and personally assign them to your technicians.</p>
                </div>
                <div class="col-md-5" style="padding-right: 0px;">
                    @include('auth.register')
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->
@endsection
@section('content')

    @endsection

@section('scripts')
    @parent
    <script>
        $('.gender').click(function () {
            $('.gender').removeClass('selected')
            $(this).addClass('selected')
        })
    </script>
    @endsection
