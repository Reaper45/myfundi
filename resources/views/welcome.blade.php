@extends('layouts.patafundi')

@section('title')
    Welcome
@endsection

@section('styles')
    @parent
    <style>
        .search-container{
            padding: 0px;
        }
        .app_img{
            position: relative;
            width: 350px;
        }
        .title{
            font-weight: 500 !important;
        }
        h1.title{
            color: #fff;
            font-size: 54px;
            line-height: 54px;
        }
        .carousel-inner .item img{
            border: solid 1px #FF9D02;
        }
        .search-container .btn{
            width: initial;
        }
        @media screen and (max-width: 1207px){
            .left-content{
                text-align: center;
            }
        }
        @media screen and (min-width: 1207px){
            .left-content{
                padding-left: 80px;
            }
        }
    </style>
    @endsection

@section('page-header')
    <div class="search-container">
        <div class="container" style="width: 100%;">
            <div class="row" style="max-height: 450px; padding-top: 50px; background: url({{ url("assets/theme/img/bg/bg-intro-1.jpg") }});">
                <div class="col-md-8 col-sm-12 left-content" style="padding-top: 50px;">
                    <h1 class="title">
                        Need a Fundi. Just Tap PataFundi.
                        <br>
                        {{--<small style="color: #f5f5f5;">Easiest way to get you professional artisans to fix your home, appliances etc.</small>--}}
                    </h1>
                    <br>
                    <br>
                    <br>
                    <a href="#" class="btn btn-common" style="margin-bottom: 30px;">Get PataFundi App &nbsp; <i class="ti-arrow-right"></i></a>

                </div>
                <div class="col-md-4 pull-right hidden-sm hidden-xs" style="padding-top: 25px;">

                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img class="app_img" src="{{ asset('assets/img/app1.jpg') }}" alt="">
                            </div>
                            <div class="item">
                                <img class="app_img" src="{{ asset('assets/img/app2.jpg') }}" alt="">
                            </div>
                            <div class="item">
                                <img class="app_img" src="{{ asset('assets/img/app3.jpg') }}" alt="">
                            </div>
                            <div class="item">
                                <img class="app_img" src="{{ asset('assets/img/app4.jpg') }}" alt="">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <!-- Find Job Section Start -->
    <section id="service-main" class="section">
        <!-- Container Starts -->
        <div class="container">
            <h1 class="section-title text-center">
                Smart & Easy Way to hire Technicians, Fundis or Artisans.
            </h1>
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="service-item">
                        <div class="icon-wrapper">
                            <i class="ti-rocket">
                            </i>
                        </div>
                        <h2 class="title">
                            Easy to Use
                        </h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis eligendi consequuntur assumenda perferendis natus dolorem facere mollitia eius.
                        </p>
                    </div>
                    <!-- Service-Block-1 Item Ends -->
                </div>

                <div class="col-sm-6 col-md-4">
                    <!-- Service-Block-1 Item Starts -->
                    <div class="service-item">
                        <div class="icon-wrapper">
                            <i class="ti-crown">
                            </i>
                        </div>
                        <h2 class="title">
                            Certified Technicians
                        </h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis eligendi consequuntur assumenda perferendis natus dolorem facere mollitia eius.
                        </p>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="service-item">
                        <div class="icon-wrapper">
                            <i class="ti-stats-up">
                            </i>
                        </div>
                        <h2 class="title">
                            Affordable
                        </h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis eligendi consequuntur assumenda perferendis natus dolorem facere mollitia eius.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Category Section Start -->
    <section class="category section" style="padding: 100px 0px;">
        <div class="container">
            <h2 class="section-title">Features Categories</h2>
            <div class="row">
                <div class="col-md-12">
                    @foreach($category_jobs as $category)
                    <div class="col-md-3 col-sm-3 col-xs-12 f-category">
                        <a>
                            <div class="icon">
                                <i class="ti-home"></i>
                            </div>
                            <h3>{{ $category['name'] }}</h3>
                            <p>{{ $category['jobs'] }}&nbsp; jobs</p>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- Category Section End -->

    <!-- Start Purchase Section -->
    <section class="section purchase" data-stellar-background-ratio="0.5" style="padding: 200px 0px">
        <div class="container">
            <div class="row">
                <div class="section-content text-center">
                    <h1 class="title-text">
                        Want to Become a PataFundi Fundi?
                    </h1>
                    <p>Join thousand of Technicians and earn what you deserve!</p>
                    <a href="{{ route('become.fundi') }}" class="btn btn-common">Get Started Now</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Purchase Section -->

    <!-- Clients Section -->
    <section class="clients section">
        <div class="container">
            <h2 class="section-title">
                Clients & Partners
            </h2>
            <div class="row">
                <div id="clients-scroller">
                    <div class="items">
                        <img src="{{ asset('assets/theme/img/clients/img1.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Client Section End -->

    <!-- Testimonial Section Start -->
    <section id="testimonial" class="section">
        <div class="container">
            <div class="row">
                <div class="touch-slider" class="owl-carousel owl-theme">
                    <div class="item active text-center">
                        <img class="img-member" src="{{ asset('assets/theme/img/testimonial/img2.jpg') }}" alt="">
                        <div class="client-info">
                            <h2 class="client-name">John Doe <span>( Plumber)</span></h2>
                        </div>
                        <p><i class="fa fa-quote-left quote-left"></i> The team that was assigned to our project... were extremely professional <i class="fa fa-quote-right quote-right"></i><br> throughout the project and assured that the owner expectations were met and <br> often exceeded. </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonial Section End -->

    <!-- Counter Section Start -->
    <section id="counter">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="counting">
                        <div class="icon">
                            <i class="ti-briefcase"></i>
                        </div>
                        <div class="desc">
                            <h2>Jobs</h2>
                            <h1 class="counter">{{ $total_jobs }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="counting">
                        <div class="icon">
                            <i class="ti-write"></i>
                        </div>
                        <div class="desc">
                            <h2>Registered Fundis</h2>
                            <h1 class="counter">{{ $total_fundis }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="counting">
                        <div class="icon">
                            <i class="ti-user"></i>
                        </div>
                        <div class="desc">
                            <h2>Clients</h2>
                            <h1 class="counter">{{ $total_clients }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="counting">
                        <div class="icon">
                            <i class="ti-heart"></i>
                        </div>
                        <div class="desc">
                            <h2>Companies</h2>
                            <h1 class="counter">{{ $total_companies }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Counter Section End -->

    <!-- Infobox Section Start -->
    <section class="infobox section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="info-text">
                        <h2>You need PataFundi services?</h2>
                        <p>Just go to <a href="#">Google Play</a> to download PataFundi</p>
                    </div>
                    <a href="#" class="btn btn-border">Google Play &nbsp;<i class="ti-android"></i></a>
                </div>
            </div>
        </div>
    </section>
    <!-- Infobox Section End -->

@endsection
