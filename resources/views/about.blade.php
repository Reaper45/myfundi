@extends('layouts.patafundi')

@section('title')
  About
@endsection


@section('styles')
  @parent
  <style>
    .title{
      font-weight: 500 !important;
    }lect service
  </style>
@endsection

@section('page-header')
  <!-- Page Header Start -->
      <div class="page-header" style="background: url(assets/img/banner1.jpg);">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="breadcrumb-wrapper">
                <h2 class="product-title">About Us</h2>
                <ol class="breadcrumb">
                  <li><a href="#"><i class="ti-home"></i> Home</a></li>
                  <li class="current">About Us</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Page Header End -->

@endsection

@section('content')

  <!-- Testimonial Section Start -->
  <section id="service-main" class="section">
    <!-- Container Starts -->
    <div class="container">
      <h1 class="section-title text-center">
        Smart & Easy Way to hire Technicians, Fundis or Artisans.
      </h1>
      <div class="row">
        <div class="col-sm-6 col-md-4">
          <div class="service-item">
            <div class="icon-wrapper">
              <i class="ti-rocket">
              </i>
            </div>
            <h2 class="title">
              Easy to Use
            </h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis eligendi consequuntur assumenda perferendis natus dolorem facere mollitia eius.
            </p>
          </div>
          <!-- Service-Block-1 Item Ends -->
        </div>

        <div class="col-sm-6 col-md-4">
          <!-- Service-Block-1 Item Starts -->
          <div class="service-item">
            <div class="icon-wrapper">
              <i class="ti-crown">
              </i>
            </div>
            <h2 class="title">
              Certified Technicians
            </h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis eligendi consequuntur assumenda perferendis natus dolorem facere mollitia eius.
            </p>
          </div>
        </div>

        <div class="col-sm-6 col-md-4">
          <div class="service-item">
            <div class="icon-wrapper">
              <i class="ti-stats-up">
              </i>
            </div>
            <h2 class="title">
              Affordable
            </h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis eligendi consequuntur assumenda perferendis natus dolorem facere mollitia eius.
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="testimonial" class="section">
    <div class="container">
      <div class="row">
        <div class="touch-slider" class="owl-carousel owl-theme">
          <div class="item active text-center">
            <img class="img-member" src="{{ asset('assets/theme/img/testimonial/img2.jpg') }}" alt="">
            <div class="client-info">
              <h2 class="client-name">John Doe <span>( Plumber)</span></h2>
            </div>
            <p><i class="fa fa-quote-left quote-left"></i> The team that was assigned to our project... were extremely professional <i class="fa fa-quote-right quote-right"></i><br> throughout the project and assured that the owner expectations were met and <br> often exceeded. </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Testimonial Section End -->

  <!-- Clients Section -->
  <section class="clients section">
    <div class="container">
      <h2 class="section-title">
        Clients & Partners
      </h2>
      <div class="row">
        <div id="clients-scroller">
          <div class="items">
            <img src="{{ url('assets/theme/img/clients/img1.png') }}" alt="">
          </div>
          <div class="items">
            <img src="{{ url('assets/theme/img/clients/img2.png') }}" alt="">
          </div>
          <div class="items">
            <img src="{{ url('assets/theme/img/clients/img3.png') }}" alt="">
          </div>
          <div class="items">
            <img src="{{ url('assets/theme/img/clients/img4.png') }}" alt="">
          </div>
          <div class="items">
            <img src="{{ url('assets/theme/img/clients/img5.png') }}" alt="">
          </div>
          <div class="items">
            <img src="{{ url('assets/theme/img/clients/img6.png') }}a" alt="">
          </div>
          <div class="items">
            <img src="{{ url('assets/theme/img/clients/img6.png') }}" alt="">
          </div>
          <div class="items">
            <img src="{{ url('assets/theme/img/clients/img6.png') }}" alt="">
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Client Section End -->

@endsection
