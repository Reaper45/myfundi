<?php
/**
 * Created by PhpStorm.
 * User: reaper45
 * Date: 9/25/17
 * Time: 5:12 AM
 */
?>


@extends('layouts.patafundi')

@section('title')
    Sign Up - Almost Done
@endsection

@section('styles')
    @parent
    <style>
        .form-control{
            padding: 10px 10px !important;
            height: 45px !important;
        }
        .logo-menu{
            display: none !important;
        }
        footer{
            display: none !important;
        }
        .job-list{
            width: 100%;
        }
        .job-list:hover {
            cursor: pointer;
            color: #FF9D02;
        }
        .job-list.active{
            border: 1px solid #FF9D02;
        }
        .job-list.active .title{
            color: #FF9D02 !important;
        }
        .hide{
            display: none;
        }
        a.btn-common:hover .ti-arrow-right{
            transition: margin 200ms;
            margin-right: -10px;
        }
        .ti-info{
            color: #FF9D02;
        }
        .ti-search{
            top: 15px;
            left: auto !important;
            right: 12px !important;
        }

    </style>
@endsection

@section('page-header')
    <!-- Page Header Start -->
    <div class="page-header" style="background: url(assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Become a Fundi</h2>
                        <ol class="breadcrumb">
                            <li><a href="/"><i class="ti-home"></i>&nbsp;Home</a></li>
                            <li class="current">Sign Up - Select Service</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->
@endsection
@section('content')
    <div class="container">
        <div class="row" style="margin-top: 30px">
            <section class="all-categories section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <h2 class="section-title">Browse All Categories</h2>
                        </div>
                        <div class="col-md-4 col-sm-12 col-md-offset-2">
                            <div class="form-group">
                                <input id="searchInput" onkeyup="searchServices()" class="form-control" type="text" placeholder="Search services...">
                                <i class="ti-search"></i>
                            </div>
                        </div>

                    </div>
                    @foreach($services as $key => $services_groups)
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="cat-title">{{ App\Category::find($key)->name }}
                                <span>({{ count($services_groups) }} Services)</span>
                            </h3>
                        </div>
                        @foreach($services_groups->chunk(4) as $services_group)
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <ul>
                                    @foreach($services_group as $key => $item)
                                    <li  class="lists">
                                        <a class="service-link" href="#">{{ $item->name }}</a>
                                        <input name="service_id" type="hidden" value="{{ $item->id }}">
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach

                    </div>
                        @endforeach
                </div>
                <form id="serviceForm" method="GET" action="{{ action('TechnicianController@create',['uid'=>$uid]) }}">
                    {{ csrf_field() }}

                </form>
            </section>
        </div>
    </div>
    @endsection

@section('scripts')
    @parent
    <script>
        $('.service-link').click(function (event) {
            event.preventDefault();

            var service = $(this).next();
            var form = $('#serviceForm');

            form.append(service);
            form.submit();

            console.log(form)
        })

        function searchServices() {
            var input, filter, ul, li, a, i;

            input = $("#searchInput");
            filter = input.val().toUpperCase();
            li = $(".lists");
//            li = ul.getElementsByTagName("li");

            for (i = 0; i < li.length; i++) {
                a = li[i].getElementsByTagName("a")[0];
                if (a) {
                     console.log(a);
                    if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        li[i].style.display = "";
                    } else {
                        li[i].style.display = "none";

                    }
                }
            }
        }
    </script>
    @endsection