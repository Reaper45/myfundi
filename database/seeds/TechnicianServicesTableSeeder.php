<?php

use Illuminate\Database\Seeder;
use App\Service;
use App\Models\Auth\User\User;
use Illuminate\Support\Facades\DB;
use Database\traits\TruncateTable;
use Database\traits\DisableForeignKeys;

class TechnicianServicesTableSeeder extends Seeder
{
  use TruncateTable, DisableForeignKeys;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->disableForeignKeys();
      $this->truncate('technician_services');

      $technician_services = [
        'technician@patafundi.com'=>['plumbing','welding']
      ];

      foreach ($technician_services as $email => $user_service) {
        $user = App\Models\Auth\User\User::whereEmail($email)->first();

        if (!$user || !$user->hasRole('technician')) continue;

        $user_service = !is_array($user_service) ? [$user_service] : $user_service;

        $services = App\Service::whereIn('slug', $user_service)->get();

        $user->service()->attach($services);
      }

      $this->enableForeignKeys();
    }
}
