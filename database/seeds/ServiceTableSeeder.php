<?php

use Illuminate\Database\Seeder;
use App\Service;
use App\Category;
use Database\traits\TruncateTable;
use Database\traits\DisableForeignKeys;

class ServiceTableSeeder extends Seeder
{
  use TruncateTable, DisableForeignKeys;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->disableForeignKeys();
      $this->truncate('services');

      $cat1 = Category::where('slug',str_slug('Building Maintenance'))->get()->first()->id;
      $cat2 = Category::where('slug',str_slug('Appliance Services'))->get()->first()->id;

      $services = [
        [
          'name'=>'Plumber',
          'slug'=>str_slug('Plumber'),
          'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
           Suspendisse interdum, velit nec blandit gravida, felis nulla dapibus
           odio, ac efficitur velit magna in purus.',
          'category_id'=> $cat1
        ],
        [
          'name'=>'Painter',
          'slug'=>str_slug('Painter'),
          'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
           Suspendisse interdum, velit nec blandit gravida, felis nulla dapibus
           odio, ac efficitur velit magna in purus.',
          'category_id'=> $cat1
        ],
        [
          'name'=>'Tilling',
          'slug'=>str_slug('Tilling'),
          'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
           Suspendisse interdum, velit nec blandit gravida, felis nulla dapibus
           odio, ac efficitur velit magna in purus.',
          'category_id'=> $cat1
        ],
        [
          'name'=>'Electrician',
          'slug'=>str_slug('Electrician'),
          'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
           Suspendisse interdum, velit nec blandit gravida, felis nulla dapibus
           odio, ac efficitur velit magna in purus.',
          'category_id'=>$cat2
        ],
        [
          'name'=>'Washing Machine',
          'slug'=>str_slug('Washing Machine'),
          'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
           Suspendisse interdum, velit nec blandit gravida, felis nulla dapibus
           odio, ac efficitur velit magna in purus.',
          'category_id'=> $cat2
        ],
        [
          'name'=>'Air Conditioning',
          'slug'=>str_slug('Air Conditioning'),
          'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
           Suspendisse interdum, velit nec blandit gravida, felis nulla dapibus
           odio, ac efficitur velit magna in purus.',
          'category_id'=> $cat2
        ],
        [
          'name'=>'Welding',
          'slug'=>str_slug('Welding'),
          'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
           Suspendisse interdum, velit nec blandit gravida, felis nulla dapibus
           odio, ac efficitur velit magna in purus.',
          'category_id'=> $cat1
        ]
      ];

      foreach ($services as $service) {
        Service::create($service);
      }

      $this->enableForeignKeys();
    }
}
