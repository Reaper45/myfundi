<?php

use Illuminate\Database\Seeder;
use App\Company;
use App\Models\Auth\User\User;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $comapany = new Company;

        $comapany->name = 'Digitart';
        $comapany->slug = str_slug($comapany->name);
        $comapany->lat = -4.048629;
        $comapany->lon = 39.704943;
        $comapany->location_name = 'Nyali';
        $comapany->description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
           Suspendisse interdum, velit nec blandit gravida, felis nulla dapibus
           odio, ac efficitur velit magna in purus.';

        $user = User::whereEmail('company@patafundi.com')->first();

        $user->company()->save($comapany);
    }
}
