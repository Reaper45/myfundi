<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Company;
use App\Models\Auth\User\User;
use Database\traits\TruncateTable;
use Database\traits\DisableForeignKeys;

class CompanyCategoriesTableSeeder extends Seeder
{
    use TruncateTable, DisableForeignKeys;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate('technician_services');
        
        $categories = [
        	'company@patafundi.com'=>'Building Maintenance',
        	'company@patafundi.com'=>'Appliance Services'
        ];

        foreach ($categories as $email => $cat) {
       		$company = User::whereEmail($email)->first()->company;
       		$category = Category::where('slug', str_slug($cat))->first();

       		$company->categories()->attach($category);
        }
        $this->enableForeignKeys();
    }
}
