<?php

use Illuminate\Database\Seeder;
use App\Models\Auth\User\User;

class TechniciansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'technician@patafundi.com')->first();
        $technician = new App\Technician;

        if ($technician) {
			$technician->gender = 'male';
			$technician->dob = Carbon\Carbon::today();
			$technician->lat = env('google.location.lat');
			$technician->lon = env('google.location.lon');
			$technician->id_card_no = 00000000;
			$technician->location_name = 'Mombasa';

            $user->technician()->save($technician);
            $user->service()->attach(1)
        }
    }
}
