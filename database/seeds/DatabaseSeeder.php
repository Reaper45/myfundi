<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RolesSeeder::class);
         $this->call(UsersSeeder::class);
         $this->call(UsersRolesSeeder::class);
         $this->call(CategoryTableSeeder::class);
         $this->call(ServiceTableSeeder::class);
         $this->call(TechnicianServicesTableSeeder::class);
         $this->call(TechniciansTableSeeder::class);
         $this->call(JobsTableSeeder::class);
         $this->call(CompaniesTableSeeder::class);
         $this->call(CompanyCategoriesTableSeeder::class);
    }
}