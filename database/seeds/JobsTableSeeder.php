<?php

use Illuminate\Database\Seeder;
use App\Job;
use App\Models\Auth\User\User;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $job = new Job;
        $user = User::whereEmail('customer@patafundi.com')->first();

        $job->service_id = 2;
        $job->description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse interdum, velit nec blandit gravida, felis nulla dapibus odio, ac efficitur velit magna in purus.';
        $job->location_name = 'Kisauni';
        $job->lat = -4.048629;
        $job->lon = 39.704943;
        $job->created_by = $user->id;
        // $job->completed = false;
        // $job-> = User::whereEmail('technician@patafundi.com')->first()->id;

        $user->jobs()->save($job);
    }
}
