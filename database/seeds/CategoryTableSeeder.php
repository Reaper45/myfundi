<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $cat1 = 'Building and Construction';
        $cat2 = 'Appliance Maintenance';
        $cat3 = 'Home Services';
        $cat4 = 'Business to Business';

        $categories = [
            [
                'name'=>$cat1,
                'slug'=>str_slug($cat1),
                'description'=>''
            ],
            [
                'name'=>$cat2,
                'slug'=>str_slug($cat2),
                'description'=>''
            ],
            [
                'name'=>$cat3,
                'slug'=>str_slug($cat3),
                'description'=>''
            ],
            [
                'name'=>$cat4,
                'slug'=>str_slug($cat4),
                'description'=>''
            ]
        ];

        foreach ($categories as $category) {
            Category::create($category);
        }
    }
}
