<?php

use Database\traits\TruncateTable;
use Database\traits\DisableForeignKeys;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class UsersSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate('users');

        $users = [
            [
              'first_name' => 'PataFundi',
              'last_name' => 'Admin',
              'phone_number' => '0700000000',
              'email' => 'admin@patafundi.com',
              'description'=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse interdum, velit nec blandit gravida, felis nulla dapibus odio, ac efficitur velit magna in purus.',
              'password' => bcrypt('admin'),
              'user_type' => null,
              'active' => true,
              'confirmation_code' => Uuid::uuid4(),
              'confirmed' => true,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now(),
            ],
            [
              'first_name' => 'PataFundi',
              'last_name' => 'Technician',
              'phone_number' => '0700000001',
              'email' => 'technician@patafundi.com',
              'description'=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse interdum, velit nec blandit gravida, felis nulla dapibus odio, ac efficitur velit magna in purus.',
              'password' => bcrypt('technician'),
              'user_type' => 0,
              'active' => true,
              'confirmation_code' => Uuid::uuid4(),
              'confirmed' => true,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now(),
            ],
            [
              'first_name' => 'PataFundi',
              'last_name' => 'Customer',
              'phone_number' => '0700000002',
              'email' => 'customer@patafundi.com',
              'description'=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse interdum, velit nec blandit gravida, felis nulla dapibus odio, ac efficitur velit magna in purus.',
              'password' => bcrypt('customer'),
              'user_type' => 1,
              'active' => true,
              'confirmation_code' => Uuid::uuid4(),
              'confirmed' => true,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now(),
            ],
            [
              'first_name' => 'PataFundi',
              'last_name' => 'Company',
              'phone_number' => '0700000003',
              'email' => 'company@patafundi.com',
              'description'=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse interdum, velit nec blandit gravida, felis nulla dapibus odio, ac efficitur velit magna in purus.',
              'password' => bcrypt('company'),
              'user_type' => null,
              'active' => true,
              'confirmation_code' => Uuid::uuid4(),
              'confirmed' => true,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now(),
            ],
        ];

        DB::table('users')->insert($users);

        $this->enableForeignKeys();
    }
}
