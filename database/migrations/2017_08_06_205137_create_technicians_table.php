<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechniciansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technicians', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('id_card_no')->nullable();
            $table->string('gender')->nullable();
            $table->string('dob')->nullable();
            $table->double('lat')->nullable();
            $table->double('lon')->nullable();
            $table->string('location_name')->nullable();
            $table->string('avartar')->nullable();
            $table->string('certificate')->nullable();
            $table->string('certificate_valid')->default(false);
            $table->string('good_conduct')->nullable();
            $table->string('good_conduct_valid')->default(false);
            $table->timestamps();

            // Indexes
            $table->foreign('user_id', 't_foreign_user')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('technicians', function (Blueprint $table) {
            $table->dropForeign('t_foreign_user');
        });
        Schema::dropIfExists('technicians');
    }
}
