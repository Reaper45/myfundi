<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->text('description');
            $table->float('lat');
            $table->float('lon');
            $table->string('location_name')->nullable();

            $table->enum('status', ['pending',  'cancelled', 'accepted'])->default('pending');
            $table->datetime('cancelled_at')->nullable();
            $table->datetime('accepted_at')->nullable();

            $table->integer('created_by');
            $table->integer('cancelled_by')->nullable();
            $table->integer('accepted_by')->nullable();

            $table->boolean('completed')->default(false);
            $table->timestamps();
            $table->softDeletes();

            // Indexes
            $table->foreign('user_id', 'j_foreign_user')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
            $table->foreign('service_id', 'j_foreign_service')
                  ->references('id')
                  ->on('services')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->dropForeign('j_foreign_user');
            $table->dropForeign('j_foreign_service');
        });
        Schema::dropIfExists('jobs');
    }
}
