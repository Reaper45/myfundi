<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('job_id')->unsigned();
            $table->text('content');
            $table->timestamps();

            // Indexes
            $table->foreign('user_id', 'ct_foreign_user')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');

            $table->foreign('job_id', 'ct_foreign_job')
                  ->references('id')
                  ->on('jobs')
                  ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function(Blueprint $table){
            $table->dropForeign('ct_foreign_user');
            $table->dropForeign('ct_foreign_job');
        });

        Schema::dropIfExists('comments');
    }
}
