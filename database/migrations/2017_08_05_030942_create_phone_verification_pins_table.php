<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhoneVerificationPinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phone_verification_pins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone_number', 100);
            $table->integer('pin');
            $table->integer('user_type')->nullable();
            $table->integer('timestamp');

            // $table->unique('phone_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phone_verification_pins');
    }
}
