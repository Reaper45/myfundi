<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name', 100);
            $table->string('slug');
            $table->string('license')->nullable();
            $table->string('license_number')->nullable();
            $table->string('logo')->nullable();
            $table->float('lat');
            $table->float('lon');
            $table->string('location_name');
            $table->text('description');
            $table->boolean('verified')->default(false);
            $table->timestamps();

            $table->foreign('user_id', 'c_foreign_user')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');

            $table->unique(['user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function(Blueprint $table){
          $table->dropForeign('c_foreign_user');
        });
        Schema::dropIfExists('companies');
    }
}
