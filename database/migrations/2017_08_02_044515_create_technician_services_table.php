<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechnicianServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technician_services', function (Blueprint $table) {
          $table->integer('user_id')->unsigned();
          $table->integer('service_id')->unsigned();

          $table->foreign('user_id', 'ts_foreign_user')
              ->references('id')
              ->on('users')
              ->onDelete('cascade');

          $table->foreign('service_id', 'ts_foreign_service')
              ->references('id')
              ->on('services')
              ->onDelete('cascade');

          $table->unique(['user_id', 'service_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('technician_services', function (Blueprint $table) {
            $table->dropForeign('ts_foreign_service');
            $table->dropForeign('ts_foreign_user');
        });

        Schema::dropIfExists('technician_services');
    }
}
