<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyServiceAndCategoryColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table){
            $table->text('description')->nullable()->change();
        });

        Schema::table('categories', function (Blueprint $table){
            $table->text('description')->nullable()->change();
        });

        Schema::table('users', function (Blueprint $table){
            $table->dropColumn('last_seen');
        });

        Schema::table('technicians', function (Blueprint $table){
            $table->string('id_card')->nullable();
            $table->string('id_card_valid')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
