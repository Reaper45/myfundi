<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCompanyEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_employees', function (Blueprint $table){
          $table->integer('user_id')->unsigned();
          $table->integer('company_id')->unsigned();

          $table->foreign('user_id', 'ce_foreign_user')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

          $table->foreign('company_id', 'ce_foreign_company')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');

          $table->unique(['user_id', 'company_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('company_employees', function (Blueprint $table) {
          $table->dropForeign('ce_foreign_user');
          $table->dropForeign('ce_foreign_company');
      });
      Schema::dropIfExists('company_employees');
    }
}
