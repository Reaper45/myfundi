<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_categories', function (Blueprint $table) {
            $table->integer('category_id')->unsigned();
            $table->integer('company_id')->unsigned();

            // Indexes
            $table->foreign('category_id', 'cc_category_id')
                  ->references('id')
                  ->on('categories')
                  ->onDelete('cascade');

            $table->foreign('company_id', 'cc_company_id')
                  ->references('id')
                  ->on('companies')
                  ->onDelete('cascade');

            // $table->unique(['category_id', 'company_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_categories', function(Blueprint $table){
            $table->dropForeign('cc_category_id');
            $table->dropForeign('cc_company_id');
        });
        Schema::dropIfExists('company_categories');
    }
}
