
<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a class="site_title">
        <span><?php echo e(config('app.name')); ?></span>
      </a>
    </div>

    <div class="clearfix"></div>
<br/>

<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    
    <ul class="nav side-menu">
      <li>
        <a href="<?php echo e(route('admin.dashboard')); ?>">
          <i class="fa fa-home" aria-hidden="true"></i>
          Dashboard
        </a>

      </li>
    </ul>
    <ul class="nav side-menu">
      <li>
        <a>
          <i class="fa fa-users" aria-hidden="true"></i>
          Technicains
          <span class="fa fa-chevron-down"></span>
        </a>
        <ul class="nav child_menu">
          <li>
            <a href="<?php echo e(route('admin.technicians')); ?>">
              All Technicains
            </a>
          </li>
          
            
              
            
          
          <li>
            <a href="<?php echo e(route('admin.technicians.map')); ?>">
              Map
            </a>
          </li>
        </ul>
      </li>
      <li>
        <a>
          <i class="fa fa-user-circle-o" aria-hidden="true"></i>
          Clients
          <span class="fa fa-chevron-down"></span>
        </a>
        <ul class="nav child_menu">
          <li>
            <a href="<?php echo e(route('admin.clients')); ?>">
              All Clients
            </a>
          </li>

        </ul>
      </li>
      <li>
        <a>
          <i class="fa fa-industry" aria-hidden="true"></i>
          Companies
          <span class="fa fa-chevron-down"></span>
        </a>
        <ul class="nav child_menu">
          <li>
            <a href="<?php echo e(route('admin.companies')); ?>">
              All Companies
            </a>
          </li>
          <li>
            <a href="<?php echo e(route('admin.companies')); ?>">
              New Company
            </a>
          </li>
          <li>
            <a href="<?php echo e(route('admin.companies.map')); ?>">
            Map
            </a>
          </li>
        </ul>
      </li>
      <li>
        <a href="<?php echo e(route('admin.jobs')); ?>">
          <i class="fa fa-briefcase" aria-hidden="true"></i>
          Jobs
          
        </a>

      </li>
      <li>
        <a  href="<?php echo e(route('admin.categories')); ?>">
          <i class="fa fa-sitemap" aria-hidden="true"></i>
          Categories
          
        </a>

      </li>
      <li>
        <a href="<?php echo e(route('admin.reports')); ?>">
          <i class="fa fa-bar-chart" aria-hidden="true"></i>
          Reports
        </a>
      </li>
    </ul>
  </div>
  <div class="menu_section">
    <h3><?php echo e(__('views.backend.section.navigation.sub_header_2')); ?></h3>

    <ul class="nav side-menu">
      <li>
        <a>
          <i class="fa fa-list"></i>
          <?php echo e(__('views.backend.section.navigation.menu_2_1')); ?>

          <span class="fa fa-chevron-down"></span>
        </a>
        <ul class="nav child_menu">
          <li>
            <a href="<?php echo e(route('log-viewer::dashboard')); ?>" target="_blank">
              <?php echo e(__('views.backend.section.navigation.menu_2_2')); ?>

            </a>
          </li>
          <li>
            <a href="<?php echo e(route('log-viewer::logs.list')); ?>" target="_blank">
              <?php echo e(__('views.backend.section.navigation.menu_2_3')); ?>

            </a>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</div>
<!-- /sidebar menu -->
<!-- /menu footer buttons -->
<div class="sidebar-footer hidden-small">
  <a data-toggle="tooltip" data-placement="top" title="Settings">
    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
  </a>
  <a data-toggle="tooltip" data-placement="top" title="FullScreen">
    <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
  </a>
  <a data-toggle="tooltip" data-placement="top" title="Lock">
    <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
  </a>
  <a href="<?php echo e(route('logout')); ?>" data-toggle="tooltip" data-placement="top" title="Logout">
    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
  </a>
</div>

<!-- /menu footer buttons -->
</div>
</div>
