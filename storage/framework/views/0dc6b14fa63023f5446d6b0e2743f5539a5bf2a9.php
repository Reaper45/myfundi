<?php if(Session::has('success')): ?>
  <div class="alert-container">
    <li class="alert alert-success alert-dismissible message" role="alert" style="list-style: none;">
        <strong><i style="font-size: 1.5em; padding-right: 5px;"  class="fa fa-check-circle" aria-hidden="true"></i>Done!</strong>
        <?php echo e(Session::get('success')); ?>

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button></li>
          </div>
<?php endif; ?>


<?php if(Session::has('info')): ?>
  <div class="alert-container">
    <li class="alert alert-info alert-dismissible message" role="alert" style="list-style: none;">
      <strong>
        <i style="font-size: 1.5em; padding-right: 5px;"  class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
        Heads Up!
      </strong>
      <?php echo e(Session::get('info')); ?>

      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </li>
  </div>
<?php endif; ?>


<?php if(Session::has('errors')): ?>
  <div class="alert-container">
    <?php $__currentLoopData = Session::get('errors'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <li class="alert alert-error alert-dismissible message" role="alert" style="list-style: none;">
      <strong>
        <i style="font-size: 1.5em; padding-right: 5px;"  class="fa fa-minus-circle fa-2x" aria-hidden="true"></i>
        Heads Up!
      </strong>
      <?php echo e($error); ?>

      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </li>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </div>
<?php endif; ?>


<?php if($errors->count() > 0): ?>
  <div class="alert-container">
    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li class="alert alert-danger alert-dismissible message" role="alert" style="list-style: none;">
        <strong><i style="font-size: 1.5em; padding-right: 5px;" class="fa fa-minus-circle" aria-hidden="true"></i>Failed!</strong>
        <?php echo e($error); ?>

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </div>
    <?php endif; ?>
