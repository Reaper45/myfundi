<?php

use DaveJamesMiller\Breadcrumbs\Generator;
use App\Models\Auth\User\User;

Breadcrumbs::register('admin.users', function (Generator $breadcrumbs) {
    $breadcrumbs->push(__('views.admin.dashboard.title'), route('admin.dashboard'));
    $breadcrumbs->push(__('views.admin.users.index.title'));
});

Breadcrumbs::register('admin.dashboard', function (Generator $breadcrumbs) {
    $breadcrumbs->push(__('views.admin.dashboard.title'), route('admin.dashboard'));
});

Breadcrumbs::register('admin.users.show', function (Generator $breadcrumbs, User $user) {
    $breadcrumbs->push(__('views.admin.dashboard.title'), route('admin.dashboard'));
    $breadcrumbs->push('Technicians', route('admin.technicians'));
    $breadcrumbs->push(__('views.admin.users.show.title', ['name' => $user->first_name]));
});


Breadcrumbs::register('admin.technician.edit', function (Generator $breadcrumbs, User $user) {
    $breadcrumbs->push(__('views.admin.dashboard.title'), route('admin.dashboard'));
    $breadcrumbs->push('Technicians', route('admin.technicians'));
    $breadcrumbs->push(__('views.admin.users.edit.title', ['name' => $user->name]));
});


Breadcrumbs::register('admin.clients', function (Generator $breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
    $breadcrumbs->push('Clients', route('admin.clients'));
});

/**
* @Technicians
*/

// All
Breadcrumbs::register('admin.technicians', function(Generator $breadcrumbs){
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
    $breadcrumbs->push('Technicians', route('admin.technicians'));
});
// New 
Breadcrumbs::register('admin.technicians.form', function (Generator $breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
    $breadcrumbs->push('Technician', route('admin.technicians.new'));
    $breadcrumbs->push('Register');
});
// Maps
Breadcrumbs::register('admin.technicians.map', function(Generator $breadcrumbs){
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
    $breadcrumbs->push('Technicians', route('admin.technicians'));
    $breadcrumbs->push('Map');
});

/**
* @Companies
*/

// All
Breadcrumbs::register('admin.companies', function(Generator $breadcrumbs){
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
    $breadcrumbs->push('Companies', route('admin.companies'));
});
// New
Breadcrumbs::register('admin.companies.new', function(Generator $breadcrumbs){
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
    $breadcrumbs->push('Companies', route('admin.companies'));
    $breadcrumbs->push('Create');
});
// Edit
Breadcrumbs::register('admin.companies.edit', function(Generator $breadcrumbs){
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
    $breadcrumbs->push('Companies', route('admin.companies'));
    $breadcrumbs->push('Edit');
});
// Preview
Breadcrumbs::register('admin.companies.show', function(Generator $breadcrumbs){
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
    $breadcrumbs->push('Companies', route('admin.companies'));
    $breadcrumbs->push('Preview');
});
// Map
Breadcrumbs::register('admin.companies.maps', function(Generator $breadcrumbs){
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
    $breadcrumbs->push('Companies', route('admin.companies'));
    $breadcrumbs->push('Map');
});

// Reports
Breadcrumbs::register('admin.reports', function (Generator $breadcrumbs){
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
    $breadcrumbs->push('Reports');
});

// Categories
Breadcrumbs::register('admin.categories', function(Generator $breadcrumbs){
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
    $breadcrumbs->push('Categories');
});

//
Breadcrumbs::register('admin.category.view', function(Generator $breadcrumbs, $category){
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
    $breadcrumbs->push('Categories', route('admin.categories'));
    $breadcrumbs->push(\App\Category::where('slug', $category)->first()->name);
});