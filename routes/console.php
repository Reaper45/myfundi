<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Cache;
use App\Jobs\SendSMS;
use App\Jobs\SendPushNotification;
use App\Http\Controllers\ApiController;
use App\Device;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
        $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('notify', function(){
    $devices = Device::pluck('token')->toArray();
    // $devices = json_encode(array("dF0QaDUaveE:APA91bFk8jTEl5vwv5-XKyx95XlZmhskCOc-Gx5oZz6bdR9YV9D4ykKs-rcmxez6DSaZAc4j0Xmecvwd_i0iirgFXOOdak5JvCjaeSaVI4RiEkQt4-MEbAyPEKTiYLyO0gVBciGKC1k7"));

    $job = App\Job::select('id', 'description', 'status', 'lat', 'lon')->where('id', 10)->first();

    $notification = array(
            'to'=> $devices,
            'title'=>'Pata Fundi',
            'body'=>'New Job Post',
            'data'=>['data_payload'=>array($job)],
    );
    $check_job = (new App\Jobs\CheckJobState($job->id))->onConnection("database")->delay(\Carbon\Carbon::now()->addSeconds(30));
    dispatch($check_job);

    $push_notification = (new SendPushNotification($notification))->onConnection("database");
    dispatch($push_notification);
});

//Artisan::command('ee', function () {
//    App\Http\Controllers\JobController::
//});
