<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@index')->name('home');

Route::get('/about', 'HomeController@about')->name('about');

/**
 * Auth routes
 */
Route::group(['namespace' => 'Auth'], function () {

    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');

    Route::get('logout', 'LoginController@logout')->name('logout');

//    Route::post('register', 'RegisterController@register')->name('post.technicians');

    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');

    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset');

    // Confirmation Routes...
    if (config('auth.users.confirm_email')) {
        Route::get('confirm/{user_by_code}', 'ConfirmController@confirm')->name('confirm');
        Route::get('confirm/resend/{user_by_email}', 'ConfirmController@sendEmail')->name('confirm.send');
    }

    // Social Authentication Routes...
    Route::get('social/redirect/{provider}', 'SocialLoginController@redirect')->name('social.redirect');
    Route::get('social/login/{provider}', 'SocialLoginController@login')->name('social.login');
});

/**
 * Admin routes
 */
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware'=>'admin'], function () {

    Route::group(['namespace'=>'Admin'], function(){

        // Dashboard
        Route::get('/', 'DashboardController@dashboard')->name('dashboard');

        // Charts
        Route::get('charts/', 'DashboardController@charts')->name('charts');

        // New Subscription plan
        Route::post('subscriptions/create/', 'DashboardController@createSubscription')->name('subscription.new');

        // Subscription plans
        Route::get('subscriptions/', 'DashboardController@subscriptions')->name('subscriptions');
        
        // Users
        Route::get('users', 'AdminController@index')->name('users');

    });

    /**
    * @Technicians
    */

    // All Technicians
    Route::get('technicians', 'TechnicianController@technicians')->name('technicians');

    // Technicians Map
    Route::get('technicians/map', 'TechnicianController@map')->name('technicians.map');

    // Get technician Certificates
    Route::get('technician/certificate/{filename}', 'TechnicianController@certificate')->name('technicians.certificate');

    // Get technician Good Conduct
    Route::get('technician/{user_id}/good-conduct/', 'TechnicianController@goodConduct')->name('technicians.good-conduct');

    // Get technician Good Conduct
    Route::get('technician/{user_id}/id-card/', 'TechnicianController@nationalIdCard')->name('technicians.id-card');

    // Technician Details
    Route::get('technician/{user}', 'TechnicianController@show')->name('technician.show');

    // Edit Technician Details
    Route::get('technician/{user}/edit', 'TechnicianController@edit')->name('technician.edit');

    // Delete User
    Route::delete('technician/{user}', 'TechnicianController@destroy')->name('users.destroy');

    // Get Technicians Coordinates
    Route::get('technicians/coordinates', 'TechnicianController@getCoordinates')->name('technicians.coords');

    // Activate Account
    Route::get('technician/activate/{id}', 'TechnicianController@activate')->name('technician.activate');

    // Deactivate Account
    Route::get('technician/deactivate/{id}', 'TechnicianController@deactivate')->name('technician.deactivate');

    /**
    * @Clients
    */

    // All Clients
    Route::get('clients/', 'ClientController@clients')->name('clients');

    // Activate Account
    Route::get('clients/activate/{id}', 'ClientController@activate')->name('clients.activate');

    // Deactivate Account
    Route::get('clients/deactivate/{id}', 'ClientController@deactivate')->name('clients.deactivate');

    /**
    * @Jobs
    */

    // All Jobs
    Route::get('/jobs', 'JobController@getAll')->name('jobs');
    
    // Complete Jobs
    Route::get('/jobs/complete', 'JobController@getAll')->name('jobs.complete');

    // Canceled Jobs
    Route::get('/jobs/canceled', 'JobController@cancelled')->name('jobs.canceled');

    // Unasigned Jobs
    Route::get('/jobs/unasigned', 'JobController@unasigned')->name('jobs.unasigned');

    // Assigned Jobs
    Route::get('/jobs/assigned', 'JobController@assigned')->name('jobs.assigned');


    /**
    * @Companies
    */

    // All Companies
    Route::get('companies', 'CompanyController@getAll')->name('companies');

    // New Company
    Route::get('companies/new', 'CompanyController@create')->name('companies.new');
    
    // Edit Company
    Route::post('companies/edit', 'CompanyController@edit')->name('companies.edit');

    // Preview Company
    Route::get('companies/show', 'CompanyController@preview')->name('companies.show');

    // Company map
    Route::get('companies/map', 'CompanyController@map')->name('companies.map');

    // Get Company Coordinates
    Route::get('companies/coordinates', 'CompanyController@getCoordinates')->name('companies.coords');


    /**
    * @Categories
    */

    // Get all catergories
    Route::get('categories', 'CategoryController@index')->name('categories');

    // Create new category
    Route::post('categories/new', 'CategoryController@create')->name('categories.create');

    // Delete category
    Route::post('category/delete', 'CategoryController@delete')->name('category.delete');

    // Get category view
    Route::get('categories/{slug}','CategoryController@services')->name('category.view');

    /**
    * @Services
    */

    // Create new category
    Route::post('service/new', 'ServiceController@create')->name('service.create');

    // Get service
    Route::get('service/get', 'ServiceController@get')->name('service.get');

    // Edit Service
    Route::post('service/edit', 'ServiceController@edit')->name('service.edit');

    // Delete Service
    Route::post('service/delete', 'ServiceController@delete')->name('service.delete');


    // Route::get('permissions', 'PermissionController@index')->name('permissions');
    // Route::get('permissions/{user}/repeat', 'PermissionController@repeat')->name('permissions.repeat');


    // Reports
    Route::get('/reports', 'ReportsController@index')->name('reports');

    Route::get('/reports/filter/technicians', 'ReportsController@technicians')->name('filterby.tech');

    Route::get('reports/jobs/print/', 'ReportsController@printJobReports')->name('jobs.print');

    Route::get('reports/technicians/print/', 'ReportsController@printTechReports')->name('technicians.print');
});

// Technician Avatar
Route::get('technician/avatar/{filename}', [
  'uses'=> 'TechnicianController@avatar',
  'as'=>'avatar',
]);

// Fundi Profile / dashboard
Route::group(['prefix'=>'fundi'], function (){

    //
    Route::match(['get', 'post'], 'signup/', 'TechnicianController@register')->name('become.fundi');

    //
    Route::get('signup/process/{uid}', 'TechnicianController@selectState')->name('fundi.state');

    //
    Route::get('signup/process/{uid}/finish/', 'TechnicianController@create');

    //
    Route::get('signup/successful/{uid}', 'TechnicianController@signupSuccess')->name('fundi.success');

    //
    Route::get('send-app-link/{uuid}', 'TechnicianController@sendLink');

    //
    Route::group(['middleware'=>'auth:web'], function (){

        //
        Route::get('dashboard/', 'TechnicianController@fundiDashboard')->name('fundi.dashboard');

        //
        Route::get('dashboard/documents/', 'TechnicianController@fundiDocuments')->name('fundi.documents');

        //
        Route::get('dashboard/settings/', 'TechnicianController@settings')->name('fundi.settings');

        //
        Route::post('update/', 'TechnicianController@update')->name('fundi.update');

        //
        Route::post('upload/certificate', 'TechnicianController@uploadCertificate');

        //
        Route::post('upload/certificate-of-good-conduct', 'TechnicianController@uploadCertificateOfGoodConduct');

        //
        Route::post('upload/national-id-card', 'TechnicianController@uploadNationaIdCard');

        //
        Route::post('upload/profile-pic', 'TechnicianController@uploadProfilePic');

    });

});

// View category services
Route::get('category/service/{slug}','ServiceController@fetchServices')->name('category.services');


Route::get('/fundi/test/{uid}', 'TechnicianController@selectService');