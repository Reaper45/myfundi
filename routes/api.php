<?php

use Illuminate\Http\Request;
use Illuminate\Foundation\Testing\Concerns\MocksApplicationServices;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'v1'], function ()
{
    // Verify Number via SMS
    Route::post('verify_number', 'ApiController@verifyNumber');

    // COnfirm verification
    Route::post('verify_pin', 'ApiController@verifyPin');

    // Check phone Number
    Route::post('check_phone_number', 'ApiController@checkPhoneNumber');

    // Register User
    Route::post('create_account', 'ApiController@register');

    // Login User
    Route::post('/app_auth', 'ApiController@login');

    // Get Users
    Route::get('/users', 'ApiController@getAllUsers');

    // Get user by username
    Route::get('/users/{id}', 'ApiController@getUser');

    // Forgot password
    Route::post('/forgot_password', 'ApiController@forgotPassword');

    // Get Services
    Route::get('/get_services', 'ApiController@getServices');

    // Get Providers
    Route::get('/get_providers/{service_id}', 'ApiController@getProviders');

    // Route get user details
    Route::get('/get_user_details/{user_id}', 'ApiController@getUserDetails');

    //Get Messages
    Route::get('get_messages/', 'ApiController@getMessages');

    // Get Ratings
    Route::get('get_ratings/{user_id}', 'ApiController@getRatings');

    // Rate User
    Route::post('rate/', 'ApiController@rateFundi');

    // Update Location
    Route::put('update_location', 'ApiController@updateLocation');

    // Post Jobs
    Route::post('create_job', 'JobController@create');

    // Add Device
    Route::post('add_device/', 'DeviceController@addDevice');

    // Get job details
    Route::get('get_job_details/{job_id}', 'JobController@jobDetails');

    // Get Recent jobs
    Route::get('get_jobs/{user_id}', 'JobController@recentJobs');

    // Cancel jobs
    Route::post('cancel_job/', 'JobController@cancelJob');

    // Accept jobs
    Route::post('accept_job/', 'JobController@acceptJob');

    // Available jobs
    Route::get('/get_available_jobs/{user_id}', 'JobController@availableJobs');

    // Near technicians
    Route::get('nearest_fundi/', 'JobController@nearTechnicians');

});

// Near technicians
Route::get('test', 'JobController@nearTechnicians');

Route::get('test_all', function()
{
    return response(App\Models\Auth\User\User::all());
});
